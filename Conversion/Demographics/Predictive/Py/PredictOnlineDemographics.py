from h2o.estimators.random_forest import H2ORandomForestEstimator #checar que modelo es el mejor 
import h2o 
import urllib
import json 
import requests
import time 
import datetime
import pandas as pd
import numpy as np
import mysql.connector as mariadb
inicio = datetime.datetime.now()
archivo = open('/home/antonio/fbfakeevents/Conversion/Demographics/Predictive/Tokens/tokensOnline.txt', 'r') #cuidar que la primer linea sea el token y la segunda el id de la app 
lineas = archivo.read().splitlines()
passKineduDB = lineas[0]
archivo.close()
mariadb_connection = mariadb.connect(host = 'dbmaster.c6ji2pa9hmrh.us-west-2.rds.amazonaws.com', user='root',
                                     password= passKineduDB, database='kinedu_app', port=3306)
cursor = mariadb_connection.cursor()
# la consulta
string = '''
SELECT fb.user_id, locale as language,
IF(dem.mp_region is null , 'Undefined', dem.mp_region) as mp_region,
IF( dem.mp_country is null, 'Undefined', dem.mp_country) as country,
CASE
  WHEN dem.mp_city IS NULL THEN 'Undefined'
  WHEN dem.mp_city =''     THEN 'Undefined'
  WHEN dem.mp_city IS NOT NULL THEN dem.mp_city
END AS city,
dem.signup_provider,
IF( dem.mp_timezone IS NULL, 'Undefined', dem.mp_timezone) AS timezone,
CASE
 WHEN dem.gender_user ='unknown' THEN 'Undefined'
 WHEN dem.gender_user IS NULL THEN 'Undefined'
 WHEN dem.gender_user IS NOT NULL THEN dem.gender_user
END AS user_gender, /* por alguna razon en back end los usuarios que hacen signup con google no siempre tienen genero */
 (SUBSTR(email, INSTR(email, '@') + 1, LENGTH(email) - (INSTR(email, '@') + 1) - LENGTH(SUBSTRING_INDEX(email,'.',-1)) + 4)) as email_domain,
case
when dem.mp_os = 'iOS'  then 'iOS'
WHEN UPPER(dem.mp_os) = 'ANDROID' THEN 'ANDROID'
ELSE 'Undefined' END AS os, /* evaluar esta variable con mayor detalle */
CASE
WHEN dem.adjust_network IN ('Facebook', 'SMARTLY - Facebook', 'Facebook Installs') THEN 'FACEBOOK'
WHEN dem.adjust_network  IN ( 'Instagram', 'SMARTLY - Instagram') THEN 'INSTAGRAM'
ELSE 'Undefined'
END AS adjust_network,
dem.weeks_before_birth,
 (DATEDIFF( fb.created_at, dem.birthday_baby ) / 30.4)  as baby_age_at_signup_meses,
 (DATEDIFF ( NOW(), dem.birthday_baby) / 30.4)  as  edad_bebe_actual_meses,
 CASE
  WHEN dem.gender_baby='0' THEN 'Undefined'
  ELSE dem.gender_baby END AS baby_gender
FROM
( 
             SELECT *
               FROM fbpredictives
               WHERE prediction is NULL)  as fb
               INNER JOIN ( SELECT * FROM `demographics`
                            WHERE  (SUBSTR(email, INSTR(email, '@') + 1, LENGTH(email) - (INSTR(email, '@') + 1) - LENGTH(SUBSTRING_INDEX(email,'.',-1)) + 4))  IS NOT NULL )
as dem 
ON dem.user_id = fb.user_id
'''
cursor.execute(string)
raw_data  = pd.DataFrame(cursor.fetchall())
raw_data.columns = cursor.column_names
raw_data.baby_age_at_signup_meses = raw_data.baby_age_at_signup_meses.astype(np.float64)
raw_data.edad_bebe_actual_meses = raw_data.edad_bebe_actual_meses.astype(np.float64)
raw_data.weeks_before_birth = raw_data.weeks_before_birth.astype(np.float64)
# no hizo falta imputar el lenguaje porque desde back el default es ingles
# imputacion de datos
# la variable 'suscribed_to_emails' no puede ser nula por politica de 'backend' 
# la variable adjust_network no fue necesaria de imputar
###
# acoplamiento con los datos de entrenamiento 
raw_data['weeks_before_birth'] = (raw_data['weeks_before_birth'] -  38) / (3)
raw_data['baby_age_at_signup_meses'] = (raw_data['baby_age_at_signup_meses'] - 6.7)/ (8.5)
raw_data['edad_bebe_actual_meses'] = (raw_data['edad_bebe_actual_meses'] - 10.4)/ (8.8)
# carga de modelo 
h2o.init()
modelopath ='/home/antonio/fbfakeevents/Conversion/Demographics/Predictive/Model/BR/Grid_GBM_py_3_sid_8ced_model_python_1572582693426_61_model_4'
modelo = h2o.load_model(modelopath)
raw_data = h2o.H2OFrame(raw_data)
raw_data['prediccion'] = modelo.predict(raw_data)['predict']
archivo = open('/home/antonio/Predictive/Tokens/tokensOnline.txt', 'r') #cuidar que la primer linea sea el token y la segunda el id de la app 
lineas = archivo.read().splitlines()
token = lineas[1] # Kinedu APP  token
path = lineas[2] # Kinedu APP id
archivo.close()
parametros = dict()
parametros['_eventName'] = "fb_mobile_initiated_checkout"
count = 0

for i in range(raw_data.shape[0]):
    print(i)
    parametros['kinedu_user_id'] = int(raw_data[i, 'user_id']) # caso en se envia el evento de facebook 
    query ='' # inicializamos el query al string vacio
    if raw_data[i, 'prediccion'] == 'Premium':
        parametros_json = json.dumps(parametros)
        # actualizamos en la base de kinedu 
        query = "UPDATE fbpredictives SET  date_predicted = NOW(),  model_id = "
        query = query + "'" + modelopath + "'" ", prediction = 'Premium' " +  ', params ='
        query = query + "'" +urllib.parse.quote(parametros_json) + "', processed= 1 WHERE user_id =" + str(int(raw_data[i, 'user_id'])) 
        #print(query)
        cursor.execute(query) #insert en la DB de produccion 
        mariadb_connection.commit()
        count +=1
    else:
        parametros_json = json.dumps(parametros)
        query = "UPDATE fbpredictives SET  date_predicted = NOW(),  model_id = "
        query = query + "'" + modelopath + "'" ", prediction = 'Freemium' " +  ', params ='
        query = query + "'" +urllib.parse.quote(parametros_json) +"', processed=1  WHERE user_id =" + str(int(raw_data[i, 'user_id'])) 
        #print(query)
        cursor.execute(query) #insert en la DB de produccion 
        mariadb_connection.commit()
fin = datetime.datetime.now()
a = fin - inicio 
print(a.total_seconds())
print('Se procesaron ' + str(int(raw_data.shape[0])) + ' de los cuales ' + str(count) + ' detonaron evento en facebook') 
print(datetime.datetime.now())
