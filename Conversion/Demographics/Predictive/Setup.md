# README #

#
### What is this repository for? ###

Descripción de proceso para el set up de las VM del equipo de Analytics, de manera general, algunos pasos (como el set up de jupyter notebook o de R se pueden omititr para maquinas de otros proyectos).

* Version 0.1

### Set up´s del ambiente ###

* Configuration:
- VM: Ubuntu 18 (LTS) -versión sin python2- , 8vCPU, 30 GB RAM, 50GB disco no SSD. Es importante darle acceso a las API´s y tráfico HTTP y HTTPS (requisito para conectarse por medio de las API´s). En la zona sur de Carolina para mejorar la velocidad de la red.
- Se fija la ip (si se requiere como R server o Jupyter notebook)  y el firewall como [aqui](https://towardsdatascience.com/running-jupyter-notebook-in-google-cloud-platform-in-15-min-61e16da34d52)  
- Se instalo Anaconda como root.

```
sudo su
wget http://repo.continuum.io/archive/Anaconda3-4.0.0-Linux-x86_64.sh
sudo su
bash Anaconda3-4.0.0-Linux-x86_64.sh
exit
exit
```

Es importante hacerlo como root *porque las instancias de VM no permiten que los usuarios no root usen el cron* y  __aceptar el ajuste de path global__ en la instalación de Anaconda.

En otra terminal se crea el archivo de config. de jupyter __todas las instalaciones deben de ser como root para que esten disponibles en todo el OS__ para agregar lineas *cuidando que el puerto sea el mismo que el que se indico en el wirefall* y se guardan los cambios.


```
sudo su
jupyter notebook --generate-config
nano /root/.jupyter/jupyter_notebook_config.py
c = get_config()
c.NotebookApp.ip = '*'
c.NotebookApp.open_browser = False
c.NotebookApp.port = 5000
```

En este punto la terminal indica que el notebook ya es accesible en ` http://[ip_de_la_maquina]:5000`  y se puede abrir en cualquier browser.


### API´s configuration

Para poder consultar el DW (KineduDataWarehouse) y sus conjuntos de datos, se requiere de seleccionar un lenguaje (python en nuestro caso) y prepararlo *todo como root*.

Instalamos con Anaconda las librerias de __google-cloud-*__

```
conda install -c conda-forge google-cloud-bigquery
conda update -n base conda
pip install --upgrade pip
pip install --upgrade google-cloud-bigquery
anaconda search -t conda google-cloud-bigquery-storage[fastavro,pandas]
apt update
apt upgrade
```


Luego hay que crear un service account como [aqui](https://cloud.google.com/bigquery/docs/reference/libraries) en el proyecto que contiene los datasets. En nuestro caso se llama __PredictiveServiceAccount__ y requiere ser *owner*; despues de crearlo se genera un .json que sirve para guardar las llaves 'KineduDataWarehouse-0e5e74497ad3.json'. Creamos una carpeta en la VM para guardar el proyecto.

Exportamos una variable de entorno que es temporal __hay que exportarla cada vez que se habra un shell de la VM__ (que con el crontab exportamos cada vez que se ejecuta un script en bash)

De aqui en adelante los scrits 'Predictive_train.py' y 'Predictive_predict.py' (en la carpeta /home/antonio/Predictive/) se encargan de la estimación de sí un usuario __convierte__ o no. Para que funcionen se requiere instalar las librerias y construir los ejecutables BASH

```
conda install -c conda-forge category_encoders
chmod 777 Predictive_train.sh
chmod 777 Predictive_predict.sh
```



### Cron

Para que funcione el servicio de jupyter notebook cada vez que se reinicie (o prenda) la maquina se agrega al crontab la siguiente linea (solo el root tiene cron en las VM de GLP)

```
crontab -e #creacion de archivo crontab
@reboot  /root/anaconda3/bin/jupyter-notebook  --no-browser --port=5000     # esta es la linea que se agrega
service cron restart   # se reinicia el servicio
service cron status
reboot
```

Para entrenar el modelo y decidir que usuarios seran propensos a convertir ejecutacon con el cron los archivos .py

```
sudo apt-get install postfix #instalar mailer
crontab -e
0 */3 * * * /bin/bash   /home/antonio/Predictive/Bash/Predictive_train.sh > /home/antonio/Predictive/Bash/train_log.txt
0 */1 * * * * /bin/bash  /home/antonio/Predictive/Bash/Predictive_predict.sh > /home/antonio/Predictive/Bash/predict_log.txt
service cron restart
service cron status   
```

### R, RStudio server y Shiny-server

Así los [hice](https://medium.com/@pabecer/crear-una-maquina-virtual-vm-con-rstudio-server-en-google-cloud-engine-gce-33d6ca4e9665)

Funciona solo falta agregar el shiny-server.


### Set up para interactuar con la app de Fabebook

Se puede hacer esta interación con el SDK de facebook o como solo requerimos de hacer un `post` lo hicimos en Python.

Instalamos las librerias:

/home/antonio/Predictive/Model/DRF_model_python_1563328706463_1

```
sudo su
conda update -n base -c defaults conda
conda install -c conda-forge urllib3
conda install -c anaconda requests
conda update --all
conda install -c anaconda h2o
pip install "colorama>=0.3.8"
pip install requests
pip install tabulate
pip install future
pip uninstall h2o
pip install -f http://h2o-release.s3.amazonaws.com/h2o/latest_stable_Py.html h2o
sudo apt install default-jre    
sudo apt update
sudo apt upgrade
```

model_path = h2o.save_model(model=grid, path="/home/antonio/Predictive/Model", force=True)
