CloseApp, RWdismiss, RW_Paywall_dismiss, Cancel_FT,  -- no aparece

SELECT    u.id as user_id,
CASE
  WHEN ev.name='SkillsCardNotPersonalized' THEN UPPER('SkillsCardNotPersonalize')
  WHEN ev.name='S_Skills_Milestones' THEN UPPER('S_SkillsMilestones')
  WHEN ev.name ='SkillsCardNotPersonalized' THEN UPPER('SkillsCardNotPersonalize')
  ELSE UPPER(ev.name)
END as name,

IF( ue.premium_conversion_date IS NULL, 'Freemium', 'Premium') AS did_convert

FROM `celtic-music-240111.mixpanel.event` as ev

INNER JOIN mixpanel.people as p
      on ev.distinct_id = p.distinct_id

INNER JOIN (SELECT *
             FROM aws_kinedu_app.users
                 WHERE _fivetran_deleted=false) as  u
on u.email = p.email
/* hay informacion duplicada por usuario en la tabla user_extra_data POR EL MOMENTO TOMO EL REGISTRO MAS VIEJO */
INNER JOIN ( SELECT * from `celtic-music-240111.aws_kinedu_app.user_extra_data` WHERE id IN (SELECT min(id)
                                                                                             FROM `celtic-music-240111.aws_kinedu_app.user_extra_data`
                                                                                             GROUP BY user_id ) ) ue on u.id = ue.user_id

WHERE ev.name in ('Download', 'cancel_promo_yearly', 'cancel_promo_monthly', 'OpenApp', 'LogOut', 'logOut', 'S_DAPNewSkills', 'ClickInappCta', 'Survey', 'ClickSurveyAnswer',
        'ClickSurveyDismiss', 'S_SPHome', 'S_SPLogin', 'LogIn', 'SPForgotPass', 'S_SPSignup', 'SignUp', 'S_OBUserRole',
        'OBCreateUser', 'S_OBBabyName', 'CreateBaby', 'OBCreateTwins', 'S_OBPremature', 'S_OBWeightLength', 'OBNotification', 'S_IAIntro', 'IAStraightToActivities', 'S_Paywall',
        'Paywall_Dismiss', 'IAStartAssessment', 'IASkipAssessment', 'MasterSkill', 'IAFinishSkill', 'S_IAHome', 'S_HealthInterest', 'IAFinishAssessment', 'S_IAReminderSet',
        'S_IAReminderContinue', 'IAReminder', 'S_DAPHome', 'DAPChangeActivity', 'S_DAPPastPlansPremium', 'DAPActivityCompleted', 'InviteFriends', 'DAPFullScreenActivity',
        'S_DAPMaterials', 'ActivityDescription', 'MilestonesUpdate', 'Share', 'Favorite', 'ArticleView', 'SlideshowView', 'PPPaymentFail', 'PPPaymentStarted', 'PPPaymentSuccess',
        'PPPaymentProvider', 'NPSSubmit', 'S_NPSScore', 'S_RWDashboard', 'RWRedeemSuccess', 'RWRedeemStarted', 'RWInvite', 'S_SkillsHome', 'S_SkillsWeightLength', 'SkillsMilestoneLearnMore',
        'S_SkillsMilestones', 'SkillsReviewIA', 'SkillsDetail', 'UpdateLength', 'UpdateWeight', 'S_MilestonesHome', 'SkillsCardNotPersonalize', 'SkillsCardNotPersonalized',
        'SkillsViewAllSkills', 'ManageSubscription', 'S_MySubscription', 'ReasonsToCancel', 'FeaturesToTry', 'S_Reason_Activities', 'S_Reason_Activities', 'S_Reason_Budget', 'S_Reason_Age',
        'S_Reason_NoTime', 'S_FeaturesYouLose', 'CancelSubscription', 'S_MenuFamilyHome', 'S_MenuSetReminders', 'MenuHelp', 'MenuMemberStats', 'MenuFamilyInvite', 'MenuChangeBirthday',
        'MenuSkipVideo', 'MenuTellMyFriends', 'S_MenuEditBaby', 'S_MenuUserProfile', 'MenuFamilyDefault', 'DailyReminders', 'S_MenuFamilyProfile', 'TAPCallToAction', 'S_PremiumProcess',
        'CATSkillDetail', 'S_CATHome_P', 'S_CATHome_F', 'S_CATSkill', 'S_CATFavorite_F', 'S_CATFavorite_P', 'CATSearch', 'S_CATViewSkill', 'ActivityView', 'FirstActivityView',
        'ClickInDAP', 'S_PromoCode', 'PromoCodeRedeem', 'DeleteAccount', 'S_ProgressHome', 'S_ProgressSkillHome', 'S_InviteReceived', 'InviteReceived', 'S_DSVidas', 'RateActivityDAP',
        'Feedback_RateActivity', 'Dismiss_Feedback_RateActivity', 'S_MilestonesHome', 'S_SkillsMilestones', 'S_Skills_Milestones', 'SkillsCardNotPersonalize', 'SkillsCardNotPersonalized',
        'SkillsMilestoneLearnMore', 'GetStarted', 'S_RW_Paywall', 'ExpiredSubscription', 'Refund', 'TurnOff_Sub', 'AB_TEST', 'S_RWDashboard', 'S_RW_Redeem', 'S_Paywall_Soft',
        'Paywall_Dismiss', 'OverTime', 'ComparedAge', 'RestorePurchase')
AND DATE_DIFF(date(ev.time), CURRENT_DATE(), DAY ) <= 7 /* Usamos los eventos más recientes*/
AND u.created_at >= '2019-01-01'
order by u.id
limit 1000000
