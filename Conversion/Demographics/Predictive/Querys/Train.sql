select distinct
u.locale as language,
IF(u.mp_region is null , 'Undefined', u.mp_region) as mp_region,
IF(u.mp_country is null, 'Undefined', u.mp_country) as country,
CASE
  WHEN u.mp_city IS NULL THEN 'Undefined'
  WHEN u.mp_city =''     THEN 'Undefined'
  WHEN u.mp_city IS NOT NULL THEN u.mp_city
END AS city,
u.signup_provider,
 /* u.suscribed_to_emails, esta columna es constante */
IF( u.mp_timezone IS NULL, 'Undefined', u.mp_timezone) AS timezone,
/*DATE_DIFF(CURRENT_DATE(), date(u.birthday), month) as user_age,*/
CASE
 WHEN u.gender ='unknown' THEN 'Undefined'
 WHEN u.gender IS NULL THEN 'Undefined'
 WHEN u.gender IS NOT NULL THEN u.gender
END AS user_gender, /* por alguna razon en back end los usuarios que hacen signup con google no siempre tienen genero */
CASE
  WHEN  u.email IS NULL THEN 'Undefined'
  WHEN u.email IS NOT NULL THEN REGEXP_EXTRACT(u.email, r'@(.+)')
END  AS email_domain, /* se encontro un usuario con mail vacio */
case
when u.mp_os = 'iOS' or u.mp_os = 'iOS 8' or u.mp_os = 'iOS' or u.mp_os = 'iPhone' or u.mp_os = 'iPad' or UPPER(u.mp_os)= 'MAC' then 'iOS'
WHEN UPPER(u.mp_os) = 'ANDROID' THEN 'ANDROID'
ELSE 'Undefined' END AS os, /* evaluar esta variable con mayor detalle */
CASE
WHEN u.adjust_network IN ('Facebook', 'SMARTLY - Facebook', 'Facebook Installs') THEN 'FACEBOOK'
WHEN u.adjust_network  IN ( 'Instagram', 'SMARTLY - Instagram') THEN 'INSTAGRAM'
ELSE 'OTRO'
END AS adjust_network,
b.weeks_before_birth,
DATE_DIFF(date(u.created_at), date(b.birthday), month) as baby_age_at_signup_meses,
DATE_DIFF( current_date() , date(b.birthday), month) as edad_bebe_actual_meses,
CASE
  WHEN b.gender='0' THEN 'Undefined'
  ELSE b.gender END AS baby_gender,
if(ue.premium_conversion_date is null, 'Freemium', 'Premium') as did_convert
from
`celtic-music-240111.aws_kinedu_app.users` u
/* hay informacion duplicada por usuario en la tabla user_extra_data POR EL MOMENTO TOMO EL REGISTRO MAS VIEJO */
INNER JOIN ( SELECT * from `celtic-music-240111.aws_kinedu_app.user_extra_data` WHERE id IN (SELECT min(id)
FROM `celtic-music-240111.aws_kinedu_app.user_extra_data`
GROUP BY user_id ) ) ue on u.id = ue.user_id
/* ASIGNAMOS UN BEBE SOLAMENTE A CADA USUARIO ya que por reglas de negocio un usuario freemium solo puede tener un bebe*/
INNER join ( SELECT * FROM `celtic-music-240111.aws_kinedu_app.babies` where id in
                 (SELECT min(id) as id  FROM `celtic-music-240111.aws_kinedu_app.babies`
                  GROUP BY author_id ) ) as b on u.id = b.author_id
INNER join `celtic-music-240111.TESTS.user_IP_location` loc on u.id = loc.user_id
where
(ue.premium_conversion_date >= u.created_at or ue.premium_conversion_date is null)
and (ue.premium_conversion_date >= b.birthday or ue.premium_conversion_date is null)
and u.created_at >= '2019-01-01' /* Sam nos dijo que solo le interesa la población del 2019 */
and b.id is not null /* users con baby creado*/
and u.email not like '%test.com' /* filtramos los usuarios de test*/
and u.email not like '%kinedu.com'
and u.email not like '%deleted.com' /* usuarios borrados de prueba y por solicitud*/
and (u.adjust_network IN ('Facebook', 'SMARTLY - Facebook', 'Instagram', 'SMARTLY - Instagram', 'Smartlinks',
'SmartLinks', 'smartlinks', 'Facebook Installs')) /* JUSTO TOMAMOS LOS USUARIOS QUE LLEGAN POR FACEBOOK */
and b.weeks_before_birth<=44 /* lo consulte con Maria y Asael y me dijeron que 44 es una buena cota superior*/
and DATE_DIFF( current_date() , date(b.birthday), month) <= 48 /* se valido con Maria esta cota */
AND u.mp_os IS NOT NULL /* hay inconsistencias y los usuarios creados por Aldea no tienen OS */
AND u.mp_os NOT IN ('', 'Unknown') /* desde back end no se tiene el dato  */
AND u._fivetran_deleted=false
AND u.signup_provider !='' /* en back end cuando un usuario con signup_provider pide borrar sus datos se setea este campo a '' */
AND REGEXP_EXTRACT(u.email, r'@(.+)')  IS NOT NULL
