create or replace table TESTS.inputacionDemographics as (


  SELECT distinct u.id as user_id, p.country_code

  FROM `celtic-music-240111.mixpanel.people` as p
  inner join aws_kinedu_app.demographics as u
  on u.email = p.email

  where u.id <= 4683885
  and p.country_code is not null
  order by u.id
)



##################### imoutacion de la region
create or replace table TESTS.inputacionRegionDemographics as (

  SELECT distinct u.id as user_id, p.region

  FROM `celtic-music-240111.mixpanel.people` as p
  inner join aws_kinedu_app.demographics as u
  on u.email = p.email

  where u.id <= 4683885
  and p.region is not null
  order by p.region

 )


####################  imputacion de la ciudad
create or replace table TESTS.inputacionCityDemographics as (


  SELECT distinct u.id as user_id, p.city

  FROM `celtic-music-240111.mixpanel.people` as p
  inner join aws_kinedu_app.demographics as u
  on u.email = p.email

  where u.id <= 4683885
  and p.city is not null
  order by p.city
)
############################## imputacion de sistema operativo


create or replace table TESTS.inputacionOsDemographics as (

  SELECT distinct u.id as user_id, p.os

  FROM `celtic-music-240111.mixpanel.people` as p
  inner join aws_kinedu_app.demographics as u
  on u.email = p.email

  where u.id <= 4683885
  and p.os is not null
  order by p.os  )

###################################


create or replace table TESTS.inputacionTimezoneDemographics as (

  SELECT distinct u.id as user_id, p.timezone

  FROM `celtic-music-240111.mixpanel.people` as p
  inner join aws_kinedu_app.demographics as u
  on u.email = p.email

  where u.id <= 4683885
  and p.timezone is not null
  order by p.timezone )














##########################
SELECT L.*, u.created_at , timestamp_diff(u.created_at , L.userStart, SECOND) as t
FROM `celtic-music-240111.TESTS.Leanplum` as L
inner join aws_kinedu_app.users as u
on u.id=L.userId
order by t
