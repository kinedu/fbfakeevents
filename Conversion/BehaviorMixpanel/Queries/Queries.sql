Create or replace table TESTS.fb_events_behavior_aos as (

SELECT u.id as user_id,  ev.name, min(screen_height) as min_height, max(screen_height) as max_height, min(screen_width) as min_width, max(screen_width) as max_width, 
u.mp_country ,
/* p.ae_total_app_sessions, p.ae_total_app_session_length, */
if (p.android_app_version is null, 'NULL', android_app_version) as android_app_version, 
if( p.android_app_version_code is null, 'NULL', cast( android_app_version_code as string) ) as android_app_version_code, 
if( android_brand is null, 'NULL', android_brand) as android_brand, 
if(android_lib_version is null, 'NULL', android_lib_version)  as android_lib_version, 
if(android_manufacturer is null, 'NULL', android_manufacturer) as android_manufacturer, 
if(android_os_version is null, 'NULL', android_os_version) as android_os_version, 1 as contador,

if( ued.premium_conversion_date is not null, 'Premium', 'Freemium') as y ,
 baby_length, baby_weight, 
if(initial_assessment_completed is  null, 'NULL', initial_assessment_completed) as initial_assessment_completed,


if (predict_grade is null, 'NULL',predict_grade) as predict_grade, 
if(predict_grade_pay_provider is null, 'NULL', predict_grade_pay_provider) as predict_grade_pay_provider, 
if( predict_grade_pay_success is null, 'NULL', predict_grade_pay_success) as predict_grade_pay_success, 
timezone, if(user_relationship is null, 'NULL', user_relationship) as user_relationship,
if(user_role is null, 'NULL', user_role) as user_role, 

if( predict_grade_pay_succes is null, 'NULL' , predict_grade_pay_succes ) as predict_grade_pay_succes, 
if(ued.trial_start is not null, 1, 0) as FT_start, 
if(ued.trial_converted is not null , 1, 0) as FT_conversion  

FROM `celtic-music-240111.mixpanel.event` as ev
left join mixpanel.people as p 
on p.distinct_id = ev.distinct_id
inner join aws_kinedu_app.users as u 
on u.email = p.user_email
inner join aws_kinedu_app.user_extra_data as ued
on u.id = ued.user_id

where timestamp_MILLIS(ev.mp_processing_time_ms) >= '2019-01-01' and  timestamp_diff( timestamp_MILLIS(ev.mp_processing_time_ms) , u.created_at, day) <= 7 /* META PARAMETRO PARA JUGAR */
and u.mp_os='Android'
and android_app_version in ('1.15.5', '1.17.0', '1.17.1', '1.18.0', '1.18.1', '1.18.2', '1.18.3') /* versiones a partir de las que se implemento DAP Vidas para actualizarlo se puede consultar https://airtable.com/tbl1DxMyKXP0NlWMO/viwCaBGqqb5XG2Cq4?blocks=hide */
group by  ev.name, 
mp_processing_time_ms, 
/*ae_total_app_sessions,  ae_total_app_session_length, */ android_app_version_code, p.android_app_version, android_brand, android_lib_version, android_manufacturer, 
android_os_version, u.id, ued.premium_conversion_date, initial_assessment_completed, ios_app_release,
 ios_device_model, predict_grade,

baby_gender, baby_length, baby_weight, predict_grade_pay_provider, predict_grade_pay_provider, 
predict_grade_pay_success,timezone, user_relationship, user_role, predict_grade_pay_succes, mp_country,
ued.trial_start, ued.trial_converted, p.android_app_version

 )



########################################

Create or replace table TESTS.fb_events_behavior_ios as  (

    # vamos por el query para crear la estructura de datos para el modelo de ios
SELECT u.id as user_id,  ev.name, min(screen_height) as min_height, max(screen_height) as max_height, min(screen_width) as min_width, max(screen_width) as max_width, 

# p.ae_total_app_sessions, p.ae_total_app_session_length, Corde detecto que estas no sirven muchos nulos 50%
u.mp_country,
if( ued.premium_conversion_date is not null, 'Premium', 'Freemium') as y ,
 baby_length, baby_weight, 
if(initial_assessment_completed is  null, 'NULL', initial_assessment_completed) as initial_assessment_completed,
ios_app_release, 
if(ios_device_model is null , 'NULL', ios_device_model) as ios_device_model, 
if (predict_grade is null, 'NULL',predict_grade) as predict_grade, 
if(predict_grade_pay_provider is null, 'NULL', predict_grade_pay_provider) as predict_grade_pay_provider, 
if( predict_grade_pay_success is null, 'NULL', predict_grade_pay_success) as predict_grade_pay_success, 
timezone, if(user_relationship is null, 'NULL', user_relationship) as user_relationship,
if(user_role is null, 'NULL', user_role) as user_role, 1 as contador,

if( predict_grade_pay_succes is null, 'NULL' , predict_grade_pay_succes ) as predict_grade_pay_succes, 
if(ued.trial_start is not null, 1, 0) as FT_start, 
if(ued.trial_converted is not null , 1, 0) as FT_conversion  



FROM `celtic-music-240111.mixpanel.event` as ev
left join mixpanel.people as p 
on p.distinct_id = ev.distinct_id
inner join aws_kinedu_app.users as u 
on u.email = p.user_email
inner join aws_kinedu_app.user_extra_data as ued
on u.id = ued.user_id

where timestamp_MILLIS(ev.mp_processing_time_ms) >= '2019-01-01' and  timestamp_diff( timestamp_MILLIS(ev.mp_processing_time_ms) , u.created_at, day) <= 7
and u.mp_os='iOS'/* ESTE ES UNO DE LOS METAPARAMETROS A CONSIDERAR */    
and p.ios_app_release in ('5.29.0', '5.29.1', '5.29.2')  /* versiones a partir de las que se implemento DAP Vidas para actualizarlo se puede consultar https://airtable.com/tbl1DxMyKXP0NlWMO/viwCaBGqqb5XG2Cq4?blocks=hide */
group by  ev.name, 
mp_processing_time_ms, 
/* ae_total_app_sessions,  ae_total_app_session_length, */  u.id, ued.premium_conversion_date, u.mp_country, initial_assessment_completed, ios_app_release,
 ios_device_model, predict_grade,

baby_gender, baby_length, baby_weight, predict_grade_pay_provider, predict_grade_pay_provider, 
predict_grade_pay_success,timezone, user_relationship, user_role, predict_grade_pay_succes,
ued.trial_start, ued.trial_converted
)














 
 
 
 