#!/usr/bin/env python
# coding: utf-8

# In[1]:


from h2o.estimators.random_forest import H2ORandomForestEstimator #checar que modelo es el mejor
import h2o
import urllib
import json
import requests
import time
import datetime
import pandas as pd
import numpy as np
import mysql.connector as mariadb
inicio = datetime.datetime.now()
archivo = open('/home/fbfakeevents/Conversion/Demographics/Predictive/Tokens/tokensOnline.txt', 'r') #cuidar que la primer linea sea el token y la segunda el id de la app
lineas = archivo.read().splitlines()
passKineduDB = lineas[0]
archivo.close()


# In[5]:


from google.cloud import bigquery
from google.oauth2 import service_account

credentials = service_account.Credentials.from_service_account_file(
  '/home/fbfakeevents/Conversion/BehaviorMixpanel/Credentials/KineduDataWarehouse.json',
    scopes=["https://www.googleapis.com/auth/cloud-platform"],
)

client = bigquery.Client(
    credentials=credentials,
    project=credentials.project_id,
)


# In[6]:


def query_BQ(query_string):
    query_job = client.query(query_string)
    results = query_job.result().to_dataframe()  # Waits for job to complete and cast to pandas dataframe
    return(results)


# In[7]:



query = '''
SELECT distinct user_id, MIN(min_height) as min_height ,MIN(max_height) as max_height, MIN(min_width) as min_width, MIN(max_width) as max_width, min(android_app_version) as android_app_version,min(android_lib_version) as android_lib_version, initial_assessment_completed, timezone,y,
sum(case when name = "LogIn" then contador else 0 end) as LogIn,
sum(case when name = "OpenApp" then contador else 0 end) as OpenApp,
sum(case when name = "IAFinishSkill" then contador else 0 end) as IAFinishSkill,
sum(case when name = "ActivityDescription" then contador else 0 end) as ActivityDescription,
sum(case when name = "S_SPHome" then contador else 0 end) as S_SPHome,
sum(case when name = "LeanplumVarLoadAttempt" then contador else 0 end) as LeanplumVarLoadAttempt,
sum(case when name = "S_OBBabyName" then contador else 0 end) as S_OBBabyName,
sum(case when name = "S_OBUserRole" then contador else 0 end) as S_OBUserRole,
sum(case when name = "S_IAHome" then contador else 0 end) as S_IAHome,
sum(case when name = "AB_TEST" then contador else 0 end) as AB_TEST,
sum(case when name = "MasterSkill" then contador else 0 end) as MasterSkill,
sum(case when name = "IAStraightToActivities" then contador else 0 end) as IAStraightToActivities,
sum(case when name = "S_DAPMaterials" then contador else 0 end) as S_DAPMaterials,
sum(case when name = "$ae_session" then contador else 0 end) as ae_session,
sum(case when name = "MilestonesUpdate" then contador else 0 end) as MilestonesUpdate,
sum(case when name = "S_ProgressHome" then contador else 0 end) as S_ProgressHome,
sum(case when name = "FirstActivityView" then contador else 0 end) as FirstActivityView,
sum(case when name = "S_DAPHome" then contador else 0 end) as S_DAPHome,
sum(case when name = "CreateBaby" then contador else 0 end) as CreateBaby,
sum(case when name = "S_MenuFamilyProfile" then contador else 0 end) as S_MenuFamilyProfile,
sum(case when name = "ActivityView" then contador else 0 end) as ActivityView,
sum(case when name = "OBCreateUser" then contador else 0 end) as OBCreateUser,
sum(case when name = "S_Paywall_Soft" then contador else 0 end) as S_Paywall_Soft,
sum(case when name = "S_MenuFamilyHome" then contador else 0 end) as S_MenuFamilyHome,
sum(case when name = "SignUp" then contador else 0 end) as SignUp,
sum(case when name = "S_CATHome_F" then contador else 0 end) as S_CATHome_F,
sum(case when name = "S_MilestonesHome" then contador else 0 end) as S_MilestonesHome,
sum(case when name = "S_HealthInterest" then contador else 0 end) as S_HealthInterest,
sum(case when name = "$ae_first_open" then contador else 0 end) as ae_first_open,
sum(case when name = "S_IAIntro" then contador else 0 end) as S_IAIntro,
sum(case when name = "S_PremiumProcess" then contador else 0 end) as S_PremiumProcess,
sum(case when name = "S_SkillsMilestones" then contador else 0 end) as S_SkillsMilestones,
sum(case when name = "S_CATHome_P" then contador else 0 end) as S_CATHome_P,
sum(case when name = "TAPCallToAction" then contador else 0 end) as TAPCallToAction,
sum(case when name = "IAStartAssessment" then contador else 0 end) as IAStartAssessment,
sum(case when name = "IAFinishAssessment" then contador else 0 end) as IAFinishAssessment,
sum(case when name = "S_IAReminderSet" then contador else 0 end) as S_IAReminderSet,
sum(case when name = "IASkipAssessment" then contador else 0 end) as IASkipAssessment,
sum(case when name = "SkillsViewAllSkills" then contador else 0 end) as SkillsViewAllSkills,
sum(case when name = "FreeTrialStart" then contador else 0 end) as FreeTrialStart,
sum(case when name = "S_SkillsHome" then contador else 0 end) as S_SkillsHome,
sum(case when name = "S_SPLogin" then contador else 0 end) as S_SPLogin,
sum(case when name = "PPPaymentStarted" then contador else 0 end) as PPPaymentStarted,
sum(case when name = "S_DSVidas" then contador else 0 end) as S_DSVidas,
sum(case when name = "Paywall_Dismiss" then contador else 0 end) as Paywall_Dismiss,
sum(case when name = "S_SPSignUp" then contador else 0 end) as S_SPSignUp,
sum(case when name = "ArticleView" then contador else 0 end) as ArticleView,
sum(case when name = "S_DAPPastPlansPremium" then contador else 0 end) as S_DAPPastPlansPremium,
sum(case when name = "LifeSpent" then contador else 0 end) as LifeSpent,
sum(case when name = "PPPaymentProvider" then contador else 0 end) as PPPaymentProvider,
sum(case when name = "ClickInDAP" then contador else 0 end) as ClickInDAP,
sum(case when name = "$ae_crashed" then contador else 0 end) as ae_crashed,
sum(case when name = "Share" then contador else 0 end) as Share,
sum(case when name = "RestorePurchase" then contador else 0 end) as RestorePurchase,
sum(case when name = "SkillsDetail" then contador else 0 end) as SkillsDetail,
sum(case when name = "DAPActivityCompleted" then contador else 0 end) as DAPActivityCompleted,
sum(case when name = "OBCreateTwins" then contador else 0 end) as OBCreateTwins,
sum(case when name = "S_OBPremature" then contador else 0 end) as S_OBPremature,
sum(case when name = "S_IAReminderContinue" then contador else 0 end) as S_IAReminderContinue,
sum(case when name = "S_MenuEditBaby" then contador else 0 end) as S_MenuEditBaby,
sum(case when name = "S_PromoCode" then contador else 0 end) as S_PromoCode,
sum(case when name = "SkillsReviewIA" then contador else 0 end) as SkillsReviewIA,
sum(case when name = "S_ProgressSkillHome" then contador else 0 end) as S_ProgressSkillHome,
sum(case when name = "CATSkillDetail" then contador else 0 end) as CATSkillDetail,
sum(case when name = "S_DAPNewSkills" then contador else 0 end) as S_DAPNewSkills,
sum(case when name = "MenuMemberStats" then contador else 0 end) as MenuMemberStats,
sum(case when name = "IAReminder" then contador else 0 end) as IAReminder,
sum(case when name = "S_MenuUserProfile" then contador else 0 end) as S_MenuUserProfile,
sum(case when name = "$ae_updated" then contador else 0 end) as ae_updated,
sum(case when name = "OverTime" then contador else 0 end) as OverTime,
sum(case when name = "S_CATViewSkill" then contador else 0 end) as S_CATViewSkill,
sum(case when name = "MenuChangeBirthday" then contador else 0 end) as MenuChangeBirthday,
sum(case when name = "Feedback_RateActivity" then contador else 0 end) as Feedback_RateActivity,
sum(case when name = "SkillsCardNotPersonalize" then contador else 0 end) as SkillsCardNotPersonalize,
sum(case when name = "MenuFamilyHome" then contador else 0 end) as MenuFamilyHome,
sum(case when name = "MenuHelp" then contador else 0 end) as MenuHelp,
sum(case when name = "MenuFamilyDefault" then contador else 0 end) as MenuFamilyDefault,
sum(case when name = "S_NPSScore" then contador else 0 end) as S_NPSScore,
sum(case when name = "S_UpdateMilestones" then contador else 0 end) as S_UpdateMilestones,
sum(case when name = "S_InviteAccepted" then contador else 0 end) as S_InviteAccepted,
sum(case when name = "RateActivityDAP" then contador else 0 end) as RateActivityDAP,
sum(case when name = "DAPChangeActivity" then contador else 0 end) as DAPChangeActivity,
sum(case when name = "S_Demo_CreateBaby" then contador else 0 end) as S_Demo_CreateBaby,
sum(case when name = "NPSSubmit" then contador else 0 end) as NPSSubmit,
sum(case when name = "MenuFamilyInvite" then contador else 0 end) as MenuFamilyInvite,
sum(case when name = "OBNotification" then contador else 0 end) as OBNotification,
sum(case when name = "SlideshowView" then contador else 0 end) as SlideshowView,
sum(case when name = "PromoCodeRedeem" then contador else 0 end) as PromoCodeRedeem,
sum(case when name = "ComparedAge" then contador else 0 end) as ComparedAge,
sum(case when name = "DAPPastPlansPremium" then contador else 0 end) as DAPPastPlansPremium,
sum(case when name = "HealthInterest" then contador else 0 end) as HealthInterest,
sum(case when name = "S_Paywall" then contador else 0 end) as S_Paywall,
sum(case when name = "PPPaymentFail" then contador else 0 end) as PPPaymentFail,
sum(case when name = "Dismiss_Feedback_RateActivity" then contador else 0 end) as Dismiss_Feedback_RateActivity,
sum(case when name = "SPForgotPass" then contador else 0 end) as SPForgotPass,
sum(case when name = "ClickSurveyAnswer" then contador else 0 end) as ClickSurveyAnswer,
sum(case when name = "GetStartedBTN" then contador else 0 end) as GetStartedBTN,
sum(case when name = "CancelledAutorenew" then contador else 0 end) as CancelledAutorenew
from (SELECT u.id as user_id,ev.name, min(screen_height) as min_height, max(screen_height) as max_height, min(screen_width) as min_width, max(screen_width) as max_width,
u.mp_country ,
/* p.ae_total_app_sessions, p.ae_total_app_session_length, */
if (p.android_app_version is null, 'NULL', android_app_version) as android_app_version,
if( p.android_app_version_code is null, 'NULL', cast( android_app_version_code as string) ) as android_app_version_code,
if( android_brand is null, 'NULL', android_brand) as android_brand,
if(android_lib_version is null, 'NULL', android_lib_version)  as android_lib_version,
if(android_manufacturer is null, 'NULL', android_manufacturer) as android_manufacturer,
if(android_os_version is null, 'NULL', android_os_version) as android_os_version, 1 as contador,
if( ued.premium_conversion_date is not null, 'Premium', 'Freemium') as y ,
 baby_length, baby_weight,
if(initial_assessment_completed is  null, 'NULL', initial_assessment_completed) as initial_assessment_completed,
if (predict_grade is null, 'NULL',predict_grade) as predict_grade,
if(predict_grade_pay_provider is null, 'NULL', predict_grade_pay_provider) as predict_grade_pay_provider,
if( predict_grade_pay_success is null, 'NULL', predict_grade_pay_success) as predict_grade_pay_success,
timezone, if(user_relationship is null, 'NULL', user_relationship) as user_relationship,
if(user_role is null, 'NULL', user_role) as user_role,
if( predict_grade_pay_succes is null, 'NULL' , predict_grade_pay_succes ) as predict_grade_pay_succes,
if(ued.trial_start is not null, 1, 0) as FT_start,
if(ued.trial_converted is not null , 1, 0) as FT_conversion
FROM `celtic-music-240111.mixpanel.event` as ev
left join mixpanel.people as p
on p.distinct_id = ev.distinct_id
inner join aws_kinedu_app.users as u
on u.email = p.user_email
inner join aws_kinedu_app.user_extra_data as ued
on u.id = ued.user_id
where timestamp_MILLIS(ev.mp_processing_time_ms) >= '2019-01-01' and  timestamp_diff( timestamp_MILLIS(ev.mp_processing_time_ms) , u.created_at, day) <= 7 /* META PARAMETRO PARA JUGAR */
and u.mp_os='Android'
and ued.user_id>5735947
/* versiones a partir de las que se implemento DAP Vidas para actualizarlo se puede consultar https://airtable.com/tbl1DxMyKXP0NlWMO/viwCaBGqqb5XG2Cq4?blocks=hide */
group by  ev.name,
mp_processing_time_ms,
/*ae_total_app_sessions,  ae_total_app_session_length, */ android_app_version_code, p.android_app_version, android_brand, android_lib_version, android_manufacturer,
android_os_version, u.id, ued.premium_conversion_date, initial_assessment_completed, ios_app_release,
 ios_device_model, predict_grade,
baby_gender, baby_length, baby_weight, predict_grade_pay_provider, predict_grade_pay_provider,
predict_grade_pay_success,timezone, user_relationship, user_role, predict_grade_pay_succes, mp_country,
ued.trial_start, ued.trial_converted, p.android_app_version
order by user_id desc
)
where mp_country not in ('BR','US')
group by user_id, initial_assessment_completed, timezone, y

'''


# In[8]:


import datetime
start = datetime.datetime.now()
raw_data = query_BQ(query)
end = datetime.datetime.now()
print(end - start)


# In[6]:


mariadb_connection = mariadb.connect(host = 'dbmaster.c6ji2pa9hmrh.us-west-2.rds.amazonaws.com', user='root',
                                     password= passKineduDB, database='kinedu_app', port=3306)
cursor = mariadb_connection.cursor()
# la consulta
string = '''
SELECT user_id, model_id,prediction, sended_at
FROM fbpredictives
WHERE
user_id>6415107
and ((model_id is null) or (model_id= 'Demographics') or (model_id= 'Behavior' and prediction='Freemium'))
;
'''


#rw_data[rw_data['prediction'] =='Premium']
#modelo behavior y premium ya no
#WHERE prediction = 'Freemium'

#or (model_id= 'Behaivor' and prediction='Freemium') or (model_id= 'Demographics' and prediction='Freemium') or (model_id= 'Demographics' and prediction='Premium') ;

cursor.execute(string)
rw_data  = pd.DataFrame(cursor.fetchall())
rw_data.columns = cursor.column_names


# In[7]:


l1= raw_data['user_id'].values.tolist()
l2=rw_data['user_id'].tolist()

def list_duplicates(seq):
  seen = set()
  seen_add = seen.add
  # adds all elements it doesn't know yet to seen and all other to seen_twice
  seen_twice = set( x for x in seq if x in seen or seen_add(x) )
  # turn the set into a list (as requested)
  return list( seen_twice )


# In[8]:


l= l1+l2
users=list_duplicates(l) # yi


# In[9]:


premium_before= rw_data[rw_data['prediction'] =='Premium']
premium_before = premium_before[premium_before['user_id'].isin(users)]
type(rw_data)
premium_before.reset_index(drop=True)
usbefPrem = premium_before['user_id'].tolist()
print("Usuarios premium Demographics" " " + str(len(usbefPrem)))


# In[10]:


raw_data=raw_data[raw_data['user_id'].isin(users)]


# In[10]:


# acoplamiento con los datos de entrenamiento
raw_data['CreateBaby']=(raw_data['CreateBaby']-0.913560769577168)/0.305964002386073
raw_data['IAStartAssessment']=(raw_data['IAStartAssessment']-0.76256774173512)/0.435836344260557
raw_data['S_OBPremature']=(raw_data['S_OBPremature']-0.119275144023975)/0.340358843715348
raw_data['S_OBBabyName']=(raw_data['S_OBBabyName']-1.21388530877809)/0.707898485340045
raw_data['S_IAIntro']=(raw_data['S_IAIntro']-1.71664466839547)/1.35233512762037
raw_data['ae_session']=(raw_data['ae_session']-2.64610941163683)/3.46169936086768
raw_data['IAFinishAssessment']=(raw_data['IAFinishAssessment']-0.226410347986769)/0.44159569646272
raw_data['S_DSVidas']=(raw_data['S_DSVidas']-0.218546871845835)/0.628005826488294
raw_data['S_HealthInterest']=(raw_data['S_HealthInterest']-0.236606158480721)/0.435751162980256
raw_data['IAStraightToActivities']=(raw_data['IAStraightToActivities']-0.132607649187099)/0.350082161308
raw_data['S_MenuFamilyProfile']=(raw_data['S_MenuFamilyProfile']-0.0951070668799203)/0.368631245823008
raw_data['S_SkillsHome']=(raw_data['S_SkillsHome']-0.359542850044255)/1.16726912251683
raw_data['S_ProgressHome']=(raw_data['S_ProgressHome']-0.238752154536561)/0.788319581012848
raw_data['S_PremiumProcess']=(raw_data['S_PremiumProcess']-0.272370688985853)/1.07331184481726
raw_data['S_MenuUserProfile']=(raw_data['S_MenuUserProfile']-0.100684793242131)/0.462759357799653
raw_data['IAReminder']=(raw_data['IAReminder']-0.0558921722386993)/0.259260246426719
raw_data['S_MenuEditBaby']=(raw_data['S_MenuEditBaby']-0.0820571748008509)/0.397346918196382
raw_data['S_IAReminderSet']=(raw_data['S_IAReminderSet']-0.0798304321495675)/0.385851691775285
raw_data['PPPaymentStarted']=(raw_data['PPPaymentStarted']-0.235509868165654)/0.701020836712576
raw_data['PPPaymentProvider']=(raw_data['PPPaymentProvider']-0.223916520442864)/0.700750629448948
raw_data['IASkipAssessment']=(raw_data['IASkipAssessment']-0.358502461218342)/0.763103755168127
raw_data['S_IAReminderContinue']=(raw_data['S_IAReminderContinue']-0.0620258078541592)/0.309647143669676
raw_data['S_DAPMaterials']=(raw_data['S_DAPMaterials']-0.317250267861302)/0.659252213934702
raw_data['ae_updated']=(raw_data['ae_updated']-0.01070202953462)/0.110070467254885
raw_data['MasterSkill']=(raw_data['MasterSkill']-0.164471498004627)/0.661062987298816
raw_data['ActivityDescription']=(raw_data['ActivityDescription']-0.159011785897296)/0.742772102958712
raw_data['DAPActivityCompleted']=(raw_data['DAPActivityCompleted']-0.0931349865681144)/0.463678462946408
raw_data['MilestonesUpdate']=(raw_data['MilestonesUpdate']-0.272010434944641)/1.2987043398112
raw_data['ClickInDAP']=(raw_data['ClickInDAP']-0.0578145623379245)/0.317268662059904
raw_data['ArticleView']=(raw_data['ArticleView']-0.139753722883895)/0.695366170295609
raw_data['S_MilestonesHome']=(raw_data['S_MilestonesHome']-0.380962437304926)/1.26911443136728
raw_data['S_CATHome_F']=(raw_data['S_CATHome_F']-0.406531157316107)/0.916624760125631
raw_data['SkillsViewAllSkills']=(raw_data['SkillsViewAllSkills']-0.0968275904905356)/0.621067563495371
raw_data['SkillsReviewIA']=(raw_data['SkillsReviewIA']-0.0215438127921241)/0.169688455264274
raw_data['S_MenuFamilyHome']=(raw_data['S_MenuFamilyHome']-0.470948306650723)/1.0251788086486
raw_data['S_SkillsMilestones']=(raw_data['S_SkillsMilestones']-0.150890541778599)/1.06297895552272
raw_data['Paywall_Dismiss']=(raw_data['Paywall_Dismiss']-0.338051833102998)/0.485475799255515

# In[11]:


#Filtrar Variables que se ocupan en el modelo
variables= ['user_id',
'CreateBaby',
 'IAStartAssessment',
 'S_OBPremature',
 'S_OBBabyName',
 'S_IAIntro',
 'ae_session',
 'IAFinishAssessment',
 'S_DSVidas',
 'S_HealthInterest',
 'IAStraightToActivities',
 'S_MenuFamilyProfile',
 'S_SkillsHome',
 'S_ProgressHome',
 'S_PremiumProcess',
 'S_MenuUserProfile',
 'IAReminder',
 'S_MenuEditBaby',
 'S_IAReminderSet',
 'PPPaymentStarted',
 'PPPaymentProvider',
 'IASkipAssessment',
 'S_IAReminderContinue',
 'S_DAPMaterials',
 'ae_updated',
 'MasterSkill',
 'ActivityDescription',
 'DAPActivityCompleted',
 'MilestonesUpdate',
 'ClickInDAP',
 'ArticleView',
 'S_MilestonesHome',
 'S_CATHome_F',
 'SkillsViewAllSkills',
 'SkillsReviewIA',
 'S_MenuFamilyHome',
 'S_SkillsMilestones',
 'Paywall_Dismiss',
 'timezone']

Outputs= raw_data[variables]


# In[12]:


# carga de modelo
h2o.init()
modelopath = '/home/fbfakeevents/Conversion/BehaviorMixpanel/Models/aOS/RW/Grid_DRF_py_7_sid_97a6_model_python_1589381688408_8537_model_10' # 10 ó 3

modelo = h2o.load_model(modelopath)
Outputs = h2o.H2OFrame(Outputs)
Outputs['prediccion'] = modelo.predict(Outputs)['predict']
archivo = open('/home/fbfakeevents/Conversion/Demographics/Predictive/Tokens/tokensOnline.txt', 'r') #cuidar que la primer linea sea el token y la segunda el id de la app
lineas = archivo.read().splitlines()
token = lineas[1] # Kinedu APP  token
path = lineas[2] # Kinedu APP id
archivo.close()


# In[13]:


Outputs['prediccion'].table()


# In[14]:


ma= Outputs['prediccion'] == 'Premium'
Outputs = Outputs[ma, :]


# In[ ]:


#NO correr


# In[15]:


count = 0


# In[ ]:


for i in range(Outputs.shape[0]):
    print(i)
    query ='' # inicializamos el query al string vacio
    if Outputs[i, 'prediccion'] == 'Premium':
        # actualizamos en la base de kinedu
        query = "UPDATE fbpredictives SET  date_predicted = NOW(),  model_id = 'Behavior' , prediction = 'Premium' , sended = 0, processed= 1 WHERE user_id =" + str(int(Outputs[i, 'user_id']))
        #print(query)
        cursor.execute(query) #insert en la DB de produccion
        mariadb_connection.commit()
        count +=1
 #print(query)



# In[ ]:


print("Registros Premium Actualizados" " " + str(count))


# In[ ]:
