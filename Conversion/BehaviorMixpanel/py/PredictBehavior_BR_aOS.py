#!/usr/bin/env python
# coding: utf-8

# In[40]:


from h2o.estimators.random_forest import H2ORandomForestEstimator #checar que modelo es el mejor
import h2o
import urllib
import json
import requests
import time
import datetime
import pandas as pd
import numpy as np
import mysql.connector as mariadb
inicio = datetime.datetime.now()
archivo = open('/home/fbfakeevents/Conversion/BehaviorMixpanel/Predictive/Tokens/tokensOnline.txt', 'r') #cuidar que la primer linea lineas = archivo.read().splitlines()
lineas = archivo.read().splitlines()
passKineduDB = lineas[0]
archivo.close()

from google.cloud import bigquery
from google.oauth2 import service_account

credentials = service_account.Credentials.from_service_account_file(
  '/home/fbfakeevents/Conversion/BehaviorMixpanel/Predictive/Tokens/Credentials/KineduDataWarehouse.json',
    scopes=["https://www.googleapis.com/auth/cloud-platform"],
)

client = bigquery.Client(
    credentials=credentials,
    project=credentials.project_id,
)

# In[3]:


def query_BQ(query_string):
    query_job = client.query(query_string)
    results = query_job.result().to_dataframe()  # Waits for job to complete and cast to pandas dataframe
    return(results)


# In[43]:

query = '''
SELECT distinct user_id,
max(days_in_app) as days_in_app,
y,
sum(case when video = "Completed" then 1 else 0 end) as Video_Completed,
sum(case when name ="S_DAPNewSkills" then 1 else 0 end) as S_DAPNewSkills,
sum(case when name ="AB_TEST" then 1 else 0 end) as AB_TEST,
sum(case when name ="LogIn" then 1 else 0 end) as LogIn,
sum(case when name ="SignUp" then 1 else 0 end) as SignUp,
sum(case when name ="OBCreateUser" then 1 else 0 end) as OBCreateUser,
sum(case when name ="S_OBBabyName" then 1 else 0 end) as S_OBBabyName,
sum(case when name ="S_Paywall_Soft" then 1 else 0 end) as S_Paywall_Soft,
sum(case when name ="S_IA_Answer_Skill" then 1 else 0 end) as S_IA_Answer_Skill,
sum(case when name ="DAPChangeActivity" then 1 else 0 end) as DAPChangeActivity,
sum(case when name ="S_DAPPastPlansPremium" then 1 else 0 end) as S_DAPPastPlansPremium,
sum(case when name ="DAPActivityCompleted" then 1 else 0 end) as DAPActivityCompleted,
sum(case when name ="S_DAPMaterials" then 1 else 0 end) as S_DAPMaterials,
sum(case when name ="MilestonesUpdate" then 1 else 0 end) as MilestonesUpdate,
sum(case when name ="ArticleView" then 1 else 0 end) as ArticleView,
sum(case when name ="SlideshowView" then 1 else 0 end) as SlideshowView,
sum(case when name ="ActivityView" then 1 else 0 end) as ActivityView,
sum(case when name ="ClickInDAP" then 1 else 0 end) as ClickInDAP,
sum(case when name ="RateActivityDAP" then 1 else 0 end) as RateActivityDAP,
sum(case when name ="Feedback_RateActivity" then 1 else 0 end) as Feedback_RateActivity,
sum(case when name ="Dismiss_Feedback_RateActivity" then 1 else 0 end) as Dismiss_Feedback_RateActivity,
sum(case when name ="WhatchAd" then 1 else 0 end) as WhatchAd,
sum(case when name ="VideoPlayerActivity" then 1 else 0 end) as VideoPlayerActivity,
sum(case when name ="NPSSubmit" then 1 else 0 end) as NPSSubmit,
sum(case when name ="S_NPSScore" then 1 else 0 end) as S_NPSScore,
sum(case when name ="S_ProgressHome" then 1 else 0 end) as S_ProgressHome,
sum(case when name ="S_MenuFamilyHome" then 1 else 0 end) as S_MenuFamilyHome,
sum(case when name ="MenuHelp" then 1 else 0 end) as MenuHelp,
sum(case when name ="MenuMemberStats" then 1 else 0 end) as MenuMemberStats,
sum(case when name ="MenuFamilyInvite" then 1 else 0 end) as MenuFamilyInvite,
sum(case when name ="MenuChangeBirthday" then 1 else 0 end) as MenuChangeBirthday,
sum(case when name ="S_MenuEditBaby" then 1 else 0 end) as S_MenuEditBaby,
sum(case when name ="S_MenuUserProfile" then 1 else 0 end) as S_MenuUserProfile,
sum(case when name ="MenuFamilyDefault" then 1 else 0 end) as MenuFamilyDefault,
sum(case when name ="S_MenuFamilyProfile" then 1 else 0 end) as S_MenuFamilyProfile,
sum(case when name ="S_PremiumProcess" then 1 else 0 end) as S_PremiumProcess,
sum(case when name ="RestorePurchase" then 1 else 0 end) as RestorePurchase,
sum(case when name ="S_CATHome_P" then 1 else 0 end) as S_CATHome_P,
sum(case when name ="S_CATHome_F" then 1 else 0 end) as S_CATHome_F,
sum(case when name ="S_CATViewSkill" then 1 else 0 end) as S_CATViewSkill,
sum(case when name ="S_CollectionView" then 1 else 0 end) as S_CollectionView,
sum(case when name ="ViewAllCollections" then 1 else 0 end) as ViewAllCollections,
sum(case when name ="S_MilestonesHome" then 1 else 0 end) as S_MilestonesHome,
sum(case when name ="SkillsViewAllSkills" then 1 else 0 end) as SkillsViewAllSkills,
sum(case when name ="S_PromoCode" then 1 else 0 end) as S_PromoCode,
sum(case when name ="PromoCodeRedeem" then 1 else 0 end) as PromoCodeRedeem,
sum(case when name ="S_FeedHome" then 1 else 0 end) as S_FeedHome,
sum(case when name ="shown" then 1 else 0 end) as shown,
sum(case when name ="clicked" then 1 else 0 end) as clicked,
sum(case when name ="dismissed" then 1 else 0 end) as dismissed
from
(
SELECT u.id as user_id,
date_diff(current_date('UTC'),date(b.birthday), month) as baby_age,
#timestamp_diff(timestamp(current_date('UTC')),b.birthday, day) as baby_ageD,
timestamp_diff(ued.trial_start ,u.created_at, day) as days_to_take_FT,/*Use only for analysis*/
timestamp_diff(timestamp(current_date('UTC')) ,u.created_at, day) as days_in_app,/*Use only for analysis*/
timestamp_diff(ued.premium_conversion_date,u.created_at, day) as days_to_convert,/*Use only for analysis*/
ev.name, ev.screen_height,ev.screen_width,ev.properties,
u.mp_country,
u.adjust_network,
IF(ev.name= "VideoPlayerActivity" and JSON_EXTRACT(ev.properties, "$.video_completed")="true", "Completed", "Not Completed") as Video,
if (ev.device is null, 'NULL', ev.device) as device,
/*0.01915nulls */
if (p.android_app_version is null, 'NULL', android_app_version) as android_app_version, /*all related to android will be categoric 0.001 nulls*/
if( p.android_app_version_code is null, 'NULL', cast( android_app_version_code as string) ) as android_app_version_code,
if( android_brand is null, 'NULL', android_brand) as android_brand,
if(android_lib_version is null, 'NULL', android_lib_version)  as android_lib_version,
if(android_manufacturer is null, 'NULL', android_manufacturer) as android_manufacturer,
if(android_os_version is null, 'NULL', android_os_version) as android_os_version,
if(ued.premium_conversion_date is not null, 'Premium', 'Freemium') as y,
if(p.initial_assessment_completed is  null or initial_assessment_completed="NO", 0, 1) as initial_assessment_completed,
p.timezone,
if(p.user_relationship is null, 'NULL', user_relationship) as user_relationship,
if(p.user_role is null, 'NULL', user_role) as user_role,
if(ued.trial_start is not null, 1, 0) as FT_start,
if(ued.trial_converted is not null , 1, 0) as FT_conversion
FROM `celtic-music-240111.mixpanel.event` as ev
left join mixpanel.people as p
on p.distinct_id = ev.distinct_id
inner join aws_kinedu_app.users as u
on u.email = p.user_email
inner join aws_kinedu_app.user_extra_data as ued
on u.id = ued.user_id
left join `celtic-music-240111.aws_kinedu_app.babies` as b
on  u.id =b.author_id
where DATE(ev.time) >= '2020-01-01' and  timestamp_diff(timestamp(DATE(ev.time)) , u.created_at, day) <= 3 /*only the first day events*/
and ev.name in ('user_id','MenuFamilyInvite', 'S_NPSScore', 'S_PromoCode', 'NPSSubmit', 'MenuHelp', 'Video_Completed', 'Dismiss_Feedback_RateActivity', 'MenuMemberStats', 'VideoPlayerActivity', 'LogIn', 'ViewAllCollections', 'RestorePurchase', 'clicked', 'S_DAPPastPlansPremium', 'DAPActivityCompleted', 'WhatchAd', 'SkillsViewAllSkills', 'S_CollectionView', 'S_MenuFamilyProfile', 'Feedback_RateActivity', 'ArticleView', 'S_CATViewSkill', 'RateActivityDAP', 'S_FeedHome', 'S_ProgressHome', 'DAPChangeActivity', 'PromoCodeRedeem', 'dismissed', 'OBCreateUser', 'SignUp', 'S_MenuUserProfile', 'ClickInDAP', 'MilestonesUpdate', 'S_CATHome_P', 'shown', 'MenuFamilyDefault', 'S_DAPMaterials', 'S_PremiumProcess', 'S_MenuEditBaby', 'S_DAPNewSkills', 'S_IA_Answer_Skill', 'S_MenuFamilyHome', 'ActivityView', 'SlideshowView', 'MenuChangeBirthday', 'S_MilestonesHome', 'S_CATHome_F', 'AB_TEST', 'S_Paywall_Soft', 'S_OBBabyName')
and u.mp_os='Android'
)
where mp_country ='BR'
and user_id>6416030
and days_to_take_FT is null
group by user_id,y, mp_country
'''

# In[44]:


import datetime
start = datetime.datetime.now()
raw_data = query_BQ(query)
end = datetime.datetime.now()
print(end - start)


# In[45]:


mariadb_connection = mariadb.connect(host = 'dbmaster.c6ji2pa9hmrh.us-west-2.rds.amazonaws.com', user='root',
                                     password= passKineduDB, database='kinedu_app', port=3306)
cursor = mariadb_connection.cursor()
# la consulta
string = '''
SELECT user_id, model_id,prediction, sended_at
FROM fbpredictives
WHERE
user_id>6415107
and ((model_id is null) or (model_id= 'Demographics') or (model_id= 'Behavior' and prediction='Freemium'))
;
'''

cursor.execute(string)
rw_data  = pd.DataFrame(cursor.fetchall())
rw_data.columns = cursor.column_names
# In[46]:


l1= raw_data['user_id'].values.tolist()
l2=rw_data['user_id'].tolist()

def list_duplicates(seq):
  seen = set()
  seen_add = seen.add
  seen_twice = set( x for x in seq if x in seen or seen_add(x) )
  return list( seen_twice )


l= l1+l2
users=list_duplicates(l) # yi

premium_before= rw_data[rw_data['prediction'] =='Premium']
premium_before = premium_before[premium_before['user_id'].isin(users)]
premium_before.reset_index(drop=True)
usbefPrem = premium_before['user_id'].tolist()
print("Usuarios premium Demographics" " " + str(len(usbefPrem)))

raw_data=raw_data.loc[raw_data['days_in_app']<=14]

raw_data=raw_data[raw_data['user_id'].isin(users)]
#FT= raw_data[['user_id','y','days_in_app']]
try:
    raw_data['MenuFamilyInvite']=(raw_data['MenuFamilyInvite']-0.15819318979086)/0.485729099566233
    raw_data['S_NPSScore']=(raw_data['S_NPSScore']-0.0362228688301697)/0.194624507256012
    raw_data['S_PromoCode']=(raw_data['S_PromoCode']-0.124677756670813)/0.465403317061361
    raw_data['NPSSubmit']=(raw_data['NPSSubmit']-0.025527267850097)/0.160586036879171
    raw_data['MenuHelp']=(raw_data['MenuHelp']-0.0546282549534803)/0.315106396638744
    raw_data['Video_Completed']=(raw_data['Video_Completed']-0.785519288446881)/2.05706876996319
    raw_data['Dismiss_Feedback_RateActivity']=(raw_data['Dismiss_Feedback_RateActivity']-0.0336714629866362)/0.178016150003328
    raw_data['MenuMemberStats']=(raw_data['MenuMemberStats']-0.212331185470363)/0.893039885731745
    raw_data['VideoPlayerActivity']=(raw_data['VideoPlayerActivity']-6.2458306727124)/17.6597743660675
    raw_data['LogIn']=(raw_data['LogIn']-1.03545457852303)/0.793921450573819
    raw_data['ViewAllCollections']=(raw_data['ViewAllCollections']-0.0611316523120667)/0.296136746271477
    raw_data['RestorePurchase']=(raw_data['RestorePurchase']-0.0729998905724105)/0.391815475968648
    raw_data['clicked']=(raw_data['clicked']-0.269465190269937)/0.736953579609685
    raw_data['S_DAPPastPlansPremium']=(raw_data['S_DAPPastPlansPremium']-0.391128193202083)/1.47295010565812
    raw_data['DAPActivityCompleted']=(raw_data['DAPActivityCompleted']-1.08676405016699)/2.73358520624685
    raw_data['WhatchAd']=(raw_data['WhatchAd']-0.0853934897371063)/0.413754529414925
    raw_data['SkillsViewAllSkills']=(raw_data['SkillsViewAllSkills']-0.603669156347916)/2.42052898720144
    raw_data['S_CollectionView']=(raw_data['S_CollectionView']-0.827233321237175)/3.2113887089677
    raw_data['S_MenuFamilyProfile']=(raw_data['S_MenuFamilyProfile']-0.723016671119943)/1.84410585285292
    raw_data['Feedback_RateActivity']=(raw_data['Feedback_RateActivity']-0.367736914077895)/2.02429538488977
    raw_data['ArticleView']=(raw_data['ArticleView']-0.934458595082465)/2.47917047900499
    raw_data['S_CATViewSkill']=(raw_data['S_CATViewSkill']-0.020789926200349)/0.183010530332308
    raw_data['RateActivityDAP']=(raw_data['RateActivityDAP']-0.0936491683521729)/0.825493307520763
    raw_data['S_FeedHome']=(raw_data['S_FeedHome']-0.638698597186742)/1.79424716757002
    raw_data['S_ProgressHome']=(raw_data['S_ProgressHome']-1.4150120877882)/4.0840724903357
    raw_data['DAPChangeActivity']=(raw_data['DAPChangeActivity']-0.241638282663933)/2.11395990957473
    raw_data['PromoCodeRedeem']=(raw_data['PromoCodeRedeem']-0.01787539123019)/0.192242659319528
    raw_data['dismissed']=(raw_data['dismissed']-0.197791401850216)/0.71441909879387
    raw_data['OBCreateUser']=(raw_data['OBCreateUser']-1.09286466261296)/0.422644638079732
    raw_data['SignUp']=(raw_data['SignUp']-1.09453302677076)/0.424607805980375
    raw_data['S_MenuUserProfile']=(raw_data['S_MenuUserProfile']-0.746255536800709)/2.04141988149784
    raw_data['ClickInDAP']=(raw_data['ClickInDAP']-0.0271565197478786)/0.239074047868453
    raw_data['MilestonesUpdate']=(raw_data['MilestonesUpdate']-1.44314283777377)/4.0789111247453
    raw_data['S_CATHome_P']=(raw_data['S_CATHome_P']-1.02862229092385)/10.9301249524424
    raw_data['shown']=(raw_data['shown']-8.41541877616085)/25.4050666676803
    raw_data['MenuFamilyDefault']=(raw_data['MenuFamilyDefault']-0.00840528626640335)/0.153230580801708
    raw_data['S_DAPMaterials']=(raw_data['S_DAPMaterials']-0.963702835359169)/1.81499529239659
    raw_data['S_PremiumProcess']=(raw_data['S_PremiumProcess']-6.80086939198422)/13.4285127296873
    raw_data['S_MenuEditBaby']=(raw_data['S_MenuEditBaby']-0.459598249188981)/2.13838226572662
    raw_data['S_DAPNewSkills']=(raw_data['S_DAPNewSkills']-0.042867726862626)/0.469864096076985
    raw_data['S_IA_Answer_Skill']=(raw_data['S_IA_Answer_Skill']-3.1862198776133)/6.9086854349702
    raw_data['S_MenuFamilyHome']=(raw_data['S_MenuFamilyHome']-3.28859954595704)/10.7490650082246
    raw_data['ActivityView']=(raw_data['ActivityView']-5.8059823281396)/14.1751338002528
    raw_data['SlideshowView']=(raw_data['SlideshowView']-0.00654632374487769)/0.112368679619656
    raw_data['MenuChangeBirthday']=(raw_data['MenuChangeBirthday']-0.0702585992720367)/0.84226094455056
    raw_data['S_MilestonesHome']=(raw_data['S_MilestonesHome']-2.3780718907315)/11.7296720716292
    raw_data['S_CATHome_F']=(raw_data['S_CATHome_F']-0.589390131233677)/1.90390081493203
    raw_data['AB_TEST']=(raw_data['AB_TEST']-1.38578578128716)/3.73508761481024
    raw_data['S_Paywall_Soft']=(raw_data['S_Paywall_Soft']-0.983731358246574)/1.40756498591206
    raw_data['S_OBBabyName']=(raw_data['S_OBBabyName']-1.46081786865711)/2.62290838760461
except:
    pass



#Filtrar Variables que se ocupan en el modelo
#Filtrar Variables que se ocupan en el modelo
variables=['user_id','MenuFamilyInvite', 'S_NPSScore', 'S_PromoCode', 'NPSSubmit', 'MenuHelp', 'Video_Completed', 'Dismiss_Feedback_RateActivity', 'MenuMemberStats', 'VideoPlayerActivity', 'LogIn', 'ViewAllCollections', 'RestorePurchase', 'clicked', 'S_DAPPastPlansPremium', 'DAPActivityCompleted', 'WhatchAd', 'SkillsViewAllSkills', 'S_CollectionView', 'S_MenuFamilyProfile', 'Feedback_RateActivity', 'ArticleView', 'S_CATViewSkill', 'RateActivityDAP', 'S_FeedHome', 'S_ProgressHome', 'DAPChangeActivity', 'PromoCodeRedeem', 'dismissed', 'OBCreateUser', 'SignUp', 'S_MenuUserProfile', 'ClickInDAP', 'MilestonesUpdate', 'S_CATHome_P', 'shown', 'MenuFamilyDefault', 'S_DAPMaterials', 'S_PremiumProcess', 'S_MenuEditBaby', 'S_DAPNewSkills', 'S_IA_Answer_Skill', 'S_MenuFamilyHome', 'ActivityView', 'SlideshowView', 'MenuChangeBirthday', 'S_MilestonesHome', 'S_CATHome_F', 'AB_TEST', 'S_Paywall_Soft', 'S_OBBabyName']
Outputs= raw_data[variables]

# carga de modelo
h2o.init()
modelopath = '/home/fbfakeevents/Conversion/BehaviorMixpanel/Models/aOS/BR/Grid_XGBoost_py_3_sid_ad3f_model_python_1599782079367_7552_model_10' # 10 ó 3
modelo = h2o.load_model(modelopath)
Outputs = h2o.H2OFrame(Outputs)
Outputs['prediccion'] = modelo.predict(Outputs)['predict']
Outputs['p_premium']= modelo.predict(Outputs)['Premium']
archivo = open('/home/fbfakeevents/Conversion/BehaviorMixpanel/Predictive/Tokens/tokensOnline.txt', 'r') #cuidar que la primer linea sea el token y la segunda el id de la app
lineas = archivo.read().splitlines()
token = lineas[1] # Kinedu APP  token
path = lineas[2] # Kinedu APP id
archivo.close()

# In[62]:
Outputs= Outputs.as_data_frame()
Outputs=Outputs.sort_values(by=['p_premium'],ascending=False).reset_index(drop= True)
Outputs= Outputs.iloc[:50]

Outputs=Outputs[Outputs['prediccion']=="Premium"]
Outputs=Outputs.reset_index(drop=True)


count = 0

for i in range(Outputs.shape[0]):
    print(i)
    query ='' # inicializamos el query al string vacio
#if Outputs[i, 'prediccionf'] == 'Premium':
    # actualizamos en la base de kinedu
    query = "UPDATE fbpredictives SET  date_predicted = NOW(),  model_id = 'Behavior' , prediction = 'Premium' , sended = 0, processed= 1 WHERE user_id =" + str(int(Outputs.loc[i, 'user_id']))
    #print(query)
    cursor.execute(query) #insert en la DB de produccion
    mariadb_connection.commit()
    count +=1

print("Registros Premium Actualizados" " " + str(count))


# In[ ]:
