
from h2o.estimators.random_forest import H2ORandomForestEstimator #checar que modelo es el mejor
import h2o
import urllib
import json
import requests
import time
import datetime
import pandas as pd
import numpy as np
import mysql.connector as mariadb
inicio = datetime.datetime.now()
archivo = open('/home/fbfakeevents/Conversion/Demographics/Predictive/Tokens/tokensOnline.txt', 'r') #cuidar que la primer linea sea el token y la segunda el id de la app
lineas = archivo.read().splitlines()
passKineduDB = lineas[0]
archivo.close()


# In[2]:


from google.cloud import bigquery
from google.oauth2 import service_account

credentials = service_account.Credentials.from_service_account_file(
  '/home/fbfakeevents/Conversion/BehaviorMixpanel/Credentials/KineduDataWarehouse.json',
    scopes=["https://www.googleapis.com/auth/cloud-platform"],
)

client = bigquery.Client(
    credentials=credentials,
    project=credentials.project_id,
)
def query_BQ(query_string):
    query_job = client.query(query_string)
    results = query_job.result().to_dataframe()  # Waits for job to complete and cast to pandas dataframe
    return(results)


# In[3]:


query = '''
SELECT
user_id,
max(mp_country) as mp_country,
max(initial_assessment_completed) as initial_assessment_completed,
max(ios_app_release) as ios_app_release,
max(ios_device_model) as ios_device_model,
max(timezone) as timezone,
max(FT_start) as FT_start,
max(user_relationship) as user_relationship,
max(user_role) as user_role,
y,
min(screen_height) as screen_height,
min(screen_width) as screen_width,
sum(case when name = "LogIn" then 1 else 0 end) as LogIn,
sum(case when name = "OpenApp" then 1 else 0 end) as OpenApp,
sum(case when name = "IAFinishSkill" then 1 else 0 end) as IAFinishSkill,
sum(case when name = "ActivityDescription" then 1 else 0 end) as ActivityDescription,
sum(case when name = "S_SPHome" then 1 else 0 end) as S_SPHome,
sum(case when name = "LeanplumVarLoadAttempt" then 1 else 0 end) as LeanplumVarLoadAttempt,
sum(case when name = "S_OBBabyName" then 1 else 0 end) as S_OBBabyName,
sum(case when name = "S_OBUserRole" then 1 else 0 end) as S_OBUserRole,
sum(case when name = "S_IAHome" then 1 else 0 end) as S_IAHome,
sum(case when name = "AB_TEST" then 1 else 0 end) as AB_TEST,
sum(case when name = "MasterSkill" then 1 else 0 end) as MasterSkill,
sum(case when name = "IAStraightToActivities" then 1 else 0 end) as IAStraightToActivities,
sum(case when name = "S_DAPMaterials" then 1 else 0 end) as S_DAPMaterials,
sum(case when name = "$ae_session" then 1 else 0 end) as ae_session,
sum(case when name = "MilestonesUpdate" then 1 else 0 end) as MilestonesUpdate,
sum(case when name = "S_ProgressHome" then 1 else 0 end) as S_ProgressHome,
sum(case when name = "FirstActivityView" then 1 else 0 end) as FirstActivityView,
sum(case when name = "S_DAPHome" then 1 else 0 end) as S_DAPHome,
sum(case when name = "CreateBaby" then 1 else 0 end) as CreateBaby,
sum(case when name = "S_MenuFamilyProfile" then 1 else 0 end) as S_MenuFamilyProfile,
sum(case when name = "ActivityView" then 1 else 0 end) as ActivityView,
sum(case when name = "OBCreateUser" then 1 else 0 end) as OBCreateUser,
sum(case when name = "S_Paywall_Soft" then 1 else 0 end) as S_Paywall_Soft,
sum(case when name = "S_MenuFamilyHome" then 1 else 0 end) as S_MenuFamilyHome,
sum(case when name = "SignUp" then 1 else 0 end) as SignUp,
sum(case when name = "S_CATHome_F" then 1 else 0 end) as S_CATHome_F,
sum(case when name = "S_MilestonesHome" then 1 else 0 end) as S_MilestonesHome,
sum(case when name = "S_HealthInterest" then 1 else 0 end) as S_HealthInterest,
sum(case when name = "$ae_first_open" then 1 else 0 end) as ae_first_open,
sum(case when name = "S_IAIntro" then 1 else 0 end) as S_IAIntro,
sum(case when name = "S_PremiumProcess" then 1 else 0 end) as S_PremiumProcess,
sum(case when name = "S_SkillsMilestones" then 1 else 0 end) as S_SkillsMilestones,
sum(case when name = "S_CATHome_P" then 1 else 0 end) as S_CATHome_P,
sum(case when name = "TAPCallToAction" then 1 else 0 end) as TAPCallToAction,
sum(case when name = "IAStartAssessment" then 1 else 0 end) as IAStartAssessment,
sum(case when name = "IAFinishAssessment" then 1 else 0 end) as IAFinishAssessment,
sum(case when name = "S_IAReminderSet" then 1 else 0 end) as S_IAReminderSet,
sum(case when name = "IASkipAssessment" then 1 else 0 end) as IASkipAssessment,
sum(case when name = "SkillsViewAllSkills" then 1 else 0 end) as SkillsViewAllSkills,
sum(case when name = "FreeTrialStart" then 1 else 0 end) as FreeTrialStart,
sum(case when name = "S_SkillsHome" then 1 else 0 end) as S_SkillsHome,
sum(case when name = "S_SPLogin" then 1 else 0 end) as S_SPLogin,
sum(case when name = "PPPaymentStarted" then 1 else 0 end) as PPPaymentStarted,
sum(case when name = "S_DSVidas" then 1 else 0 end) as S_DSVidas,
sum(case when name = "Paywall_Dismiss" then 1 else 0 end) as Paywall_Dismiss,
sum(case when name = "S_SPSignUp" then 1 else 0 end) as S_SPSignUp,
sum(case when name = "ArticleView" then 1 else 0 end) as ArticleView,
sum(case when name = "LifeSpent" then 1 else 0 end) as LifeSpent,
sum(case when name = "PPPaymentProvider" then 1 else 0 end) as PPPaymentProvider,
sum(case when name = "ClickInDAP" then 1 else 0 end) as ClickInDAP,
sum(case when name = "$ae_crashed" then 1 else 0 end) as ae_crashed,
sum(case when name = "Share" then 1 else 0 end) as Share,
sum(case when name = "RestorePurchase" then 1 else 0 end) as RestorePurchase,
sum(case when name = "SkillsDetail" then 1 else 0 end) as SkillsDetail,
sum(case when name = "DAPActivityCompleted" then 1 else 0 end) as DAPActivityCompleted,
sum(case when name = "OBCreateTwins" then 1 else 0 end) as OBCreateTwins,
sum(case when name = "S_OBPremature" then 1 else 0 end) as S_OBPremature,
sum(case when name = "S_IAReminderContinue" then 1 else 0 end) as S_IAReminderContinue,
sum(case when name = "S_MenuEditBaby" then 1 else 0 end) as S_MenuEditBaby,
sum(case when name = "S_PromoCode" then 1 else 0 end) as S_PromoCode,
sum(case when name = "SkillsReviewIA" then 1 else 0 end) as SkillsReviewIA,
sum(case when name = "S_ProgressSkillHome" then 1 else 0 end) as S_ProgressSkillHome,
sum(case when name = "CATSkillDetail" then 1 else 0 end) as CATSkillDetail,
sum(case when name = "S_DAPNewSkills" then 1 else 0 end) as S_DAPNewSkills,
sum(case when name = "MenuMemberStats" then 1 else 0 end) as MenuMemberStats,
sum(case when name = "IAReminder" then 1 else 0 end) as IAReminder,
sum(case when name = "S_MenuUserProfile" then 1 else 0 end) as S_MenuUserProfile,
sum(case when name = "$ae_updated" then 1 else 0 end) as ae_updated,
sum(case when name = "OverTime" then 1 else 0 end) as OverTime,
sum(case when name = "S_CATViewSkill" then 1 else 0 end) as S_CATViewSkill,
sum(case when name = "MenuChangeBirthday" then 1 else 0 end) as MenuChangeBirthday,
sum(case when name = "Feedback_RateActivity" then 1 else 0 end) as Feedback_RateActivity,
sum(case when name = "SkillsCardNotPersonalize" then 1 else 0 end) as SkillsCardNotPersonalize,
sum(case when name = "MenuFamilyHome" then 1 else 0 end) as MenuFamilyHome,
sum(case when name = "MenuHelp" then 1 else 0 end) as MenuHelp,
sum(case when name = "MenuFamilyDefault" then 1 else 0 end) as MenuFamilyDefault,
sum(case when name = "S_NPSScore" then 1 else 0 end) as S_NPSScore,
sum(case when name = "S_UpdateMilestones" then 1 else 0 end) as S_UpdateMilestones,
sum(case when name = "S_InviteAccepted" then 1 else 0 end) as S_InviteAccepted,
sum(case when name = "RateActivityDAP" then 1 else 0 end) as RateActivityDAP,
sum(case when name = "DAPChangeActivity" then 1 else 0 end) as DAPChangeActivity,
sum(case when name = "S_Demo_CreateBaby" then 1 else 0 end) as S_Demo_CreateBaby,
sum(case when name = "NPSSubmit" then 1 else 0 end) as NPSSubmit,
sum(case when name = "MenuFamilyInvite" then 1 else 0 end) as MenuFamilyInvite,
sum(case when name = "OBNotification" then 1 else 0 end) as OBNotification,
sum(case when name = "SlideshowView" then 1 else 0 end) as SlideshowView,
sum(case when name = "PromoCodeRedeem" then 1 else 0 end) as PromoCodeRedeem,
sum(case when name = "ComparedAge" then 1 else 0 end) as ComparedAge,
sum(case when name = "HealthInterest" then 1 else 0 end) as HealthInterest,
sum(case when name = "S_Paywall" then 1 else 0 end) as S_Paywall,
sum(case when name = "PPPaymentFail" then 1 else 0 end) as PPPaymentFail,
sum(case when name = "Dismiss_Feedback_RateActivity" then 1 else 0 end) as Dismiss_Feedback_RateActivity,
sum(case when name = "SPForgotPass" then 1 else 0 end) as SPForgotPass,
sum(case when name = "ClickSurveyAnswer" then 1 else 0 end) as ClickSurveyAnswer,
sum(case when name = "GetStartedBTN" then 1 else 0 end) as GetStartedBTN,
sum(case when name = "CancelledAutorenew" then 1 else 0 end) as CancelledAutorenew
from(
SELECT u.id as user_id,ev.name,ev.screen_width,ev.screen_height,
u.mp_country,/*contains nulls 0.001*/
if(ued.premium_conversion_date is not null, 'Premium', 'Freemium') as y,
if(p.initial_assessment_completed is  null, 0, 1) as initial_assessment_completed,
p.ios_app_release,/*contains nulls 0.001*/
if(p.ios_device_model is null , 'NULL', ios_device_model) as ios_device_model,
p.timezone,
if(p.user_relationship is null, 'NULL', user_relationship) as user_relationship,
if(p.user_role is null, 'NULL', user_role) as user_role,
if(ued.trial_start is not null, 1, 0) as FT_start,
if(ued.trial_converted is not null , 1, 0) as FT_conversion
/*[relationship, user_role] .07 nulls/null as string to take it as a category*/
FROM `celtic-music-240111.mixpanel.event` as ev
left join mixpanel.people as p
on p.distinct_id = ev.distinct_id
inner join aws_kinedu_app.users as u
on u.email = p.user_email
inner join aws_kinedu_app.user_extra_data as ued
on u.id = ued.user_id

where timestamp_MILLIS(ev.mp_processing_time_ms) >= '2020-01-01' and  timestamp_diff(timestamp_MILLIS(ev.mp_processing_time_ms) , u.created_at, day) <= 1  /* Events days*/
and u.mp_os='iOS'
and ued.user_id>6224237
)

where mp_country not in ('BR','US')
group by user_id, y

'''



import datetime
start = datetime.datetime.now()
raw_data = query_BQ(query)
end = datetime.datetime.now()
print(end - start)




mariadb_connection = mariadb.connect(host = 'dbmaster.c6ji2pa9hmrh.us-west-2.rds.amazonaws.com', user='root',
                                     password= passKineduDB, database='kinedu_app', port=3306)
cursor = mariadb_connection.cursor()
# la consulta
string = '''
SELECT user_id, model_id,prediction, sended_at
FROM fbpredictives
WHERE
user_id>6415107
and ((model_id is null) or (model_id= 'Demographics') or (model_id= 'Behavior' and prediction='Freemium'))
;
'''


cursor.execute(string)
rw_data  = pd.DataFrame(cursor.fetchall())
rw_data.columns = cursor.column_names


l1= raw_data['user_id'].values.tolist()
l2=rw_data['user_id'].tolist()

def list_duplicates(seq):
  seen = set()
  seen_add = seen.add
  # adds all elements it doesn't know yet to seen and all other to seen_twice
  seen_twice = set( x for x in seq if x in seen or seen_add(x) )
  # turn the set into a list (as requested)
  return list( seen_twice )


# In[8]:


l= l1+l2
users=list_duplicates(l) # yi


# In[9]:


premium_before= rw_data[rw_data['prediction'] =='Premium']
premium_before = premium_before[premium_before['user_id'].isin(users)]
type(rw_data)
premium_before.reset_index(drop=True)
usbefPrem = premium_before['user_id'].tolist()
print("Usuarios premium Demographics" " " + str(len(usbefPrem)))


# In[10]:


raw_data=raw_data[raw_data['user_id'].isin(users)]

FT= raw_data[['user_id','y','FT_start']]

raw_data['FT_start']=(raw_data['FT_start']-0.0602957830923989)/0.238034827481322
raw_data['FreeTrialStart']=(raw_data['FreeTrialStart']-0.057184731197122)/0.233426463578131
raw_data['S_CATHome_P']=(raw_data['S_CATHome_P']-0.113829858104057)/1.20909509116576
raw_data['S_ProgressSkillHome']=(raw_data['S_ProgressSkillHome']-0.0261341682765971)/0.593408757697964
raw_data['MenuFamilyInvite']=(raw_data['MenuFamilyInvite']-0.00794084338152021)/0.293043502968124
raw_data['ComparedAge']=(raw_data['ComparedAge']-0.00259809473053094)/0.0720380034146825
raw_data['PPPaymentFail']=(raw_data['PPPaymentFail']-0.0124108986743055)/0.219857676984812
raw_data['S_MenuFamilyProfile']=(raw_data['S_MenuFamilyProfile']-0.0704816467923522)/0.721108519109464
raw_data['DAPActivityCompleted']=(raw_data['DAPActivityCompleted']-0.0380187862234361)/0.330596569159904
raw_data['S_CATViewSkill']=(raw_data['S_CATViewSkill']-0.00239157950836053)/0.0614128284962554
raw_data['MenuMemberStats']=(raw_data['MenuMemberStats']-0.00616214775831057)/0.178807439337853
raw_data['SignUp']=(raw_data['SignUp']-7.32795949636932E-05)/0.00930582423456936
raw_data['SkillsDetail']=(raw_data['SkillsDetail']-0.0611551528878822)/0.956301604466534
raw_data['FirstActivityView']=(raw_data['FirstActivityView']-0.000226500566251415)/0.016322430172931
raw_data['MilestonesUpdate']=(raw_data['MilestonesUpdate']-0.0932982479515021)/0.605036948949365
raw_data['S_DAPMaterials']=(raw_data['S_DAPMaterials']-0.153833855172873)/0.570766497541239
raw_data['S_SkillsMilestones']=(raw_data['S_SkillsMilestones']-0.0188395176870295)/0.404614031556799
raw_data['S_Paywall_Soft']=(raw_data['S_Paywall_Soft']-3.99706881620145E-05)/0.00816190294803041
raw_data['S_ProgressHome']=(raw_data['S_ProgressHome']-0.0889347811604822)/0.586703789805256
raw_data['DAPChangeActivity']=(raw_data['DAPChangeActivity']-0.0109453067750316)/0.162278493568635
raw_data['LogIn']=(raw_data['LogIn']-0.000386383318899473)/0.026820288716795
raw_data['Feedback_RateActivity']=(raw_data['Feedback_RateActivity']-0.0336553194324162)/0.362580048342971
raw_data['S_DSVidas']=(raw_data['S_DSVidas']-0.000306441942575444)/0.0425666852594841
raw_data['ae_session']=(raw_data['ae_session']-4.77220038638332)/4.76588124568175
raw_data['S_IAReminderContinue']=(raw_data['S_IAReminderContinue']-0.000026647125441343)/0.00632220249325732
raw_data['ActivityView']=(raw_data['ActivityView']-0.515601891945907)/4.86663702322537
raw_data['LeanplumVarLoadAttempt']=(raw_data['LeanplumVarLoadAttempt']-0.000326427286656451)/0.0395654204250102
raw_data['PPPaymentStarted']=(raw_data['PPPaymentStarted']-0.467403903803878)/1.37445769111879
raw_data['OpenApp']=(raw_data['OpenApp']-3.26180800746119)/8.00442012660354
raw_data['PromoCodeRedeem']=(raw_data['PromoCodeRedeem']-0.00191859303177669)/0.066681470968711
raw_data['ActivityDescription']=(raw_data['ActivityDescription']-9.32649390447005E-05)/0.0171205033264661
raw_data['Dismiss_Feedback_RateActivity']=(raw_data['Dismiss_Feedback_RateActivity']-0.00698154686563187)/0.10268184165271
raw_data['PPPaymentProvider']=(raw_data['PPPaymentProvider']-0.41987875557924)/2.29492746778102
raw_data['S_DAPHome']=(raw_data['S_DAPHome']-3.40341749383784)/10.4653100832785
raw_data['ArticleView']=(raw_data['ArticleView']-0.180107920858037)/1.21464888400815
raw_data['SkillsViewAllSkills']=(raw_data['SkillsViewAllSkills']-0.0243821197788288)/0.231587499226555
raw_data['S_IAReminderSet']=(raw_data['S_IAReminderSet']-0.000026647125441343)/0.005162033793557
raw_data['IAReminder']=(raw_data['IAReminder']-0.000026647125441343)/0.005162033793557
raw_data['S_Paywall']=(raw_data['S_Paywall']-0.73626673772567)/0.746392835500399
raw_data['S_DAPNewSkills']=(raw_data['S_DAPNewSkills']-0.00902671374325495)/0.134679661771843
raw_data['RateActivityDAP']=(raw_data['RateActivityDAP']-0.00358403837186063)/0.0967832900104278
raw_data['CancelledAutorenew']=(raw_data['CancelledAutorenew']-1.33235627206715E-05)/0.00365013341693402
raw_data['S_MenuFamilyHome']=(raw_data['S_MenuFamilyHome']-0.710872027180067)/2.67766694185345
raw_data['S_OBUserRole']=(raw_data['S_OBUserRole']-0.000666178136033575)/0.0307495603026977
raw_data['CATSkillDetail']=(raw_data['CATSkillDetail']-0.356132169742189)/1.75491849534367
raw_data['SkillsReviewIA']=(raw_data['SkillsReviewIA']-0.0392911864632602)/0.242402638145584
raw_data['GetStartedBTN']=(raw_data['GetStartedBTN']-0.654739857437879)/1.22994393239436
raw_data['S_HealthInterest']=(raw_data['S_HealthInterest']-0.00243821197788288)/0.0634911134030922
raw_data['S_OBBabyName']=(raw_data['S_OBBabyName']-1.19401105855705)/0.784795864522763
raw_data['AB_TEST']=(raw_data['AB_TEST']-3.14699886749716)/12.564469730138

#Filtrar Variables que se ocupan en el modelo
variables= ['user_id','FT_start', 'FreeTrialStart', 'S_CATHome_P', 'S_ProgressSkillHome', 'MenuFamilyInvite', 'ComparedAge', 'PPPaymentFail', 'S_MenuFamilyProfile', 'DAPActivityCompleted', 'S_CATViewSkill', 'MenuMemberStats', 'SignUp', 'SkillsDetail', 'FirstActivityView', 'MilestonesUpdate', 'S_DAPMaterials', 'S_SkillsMilestones', 'S_Paywall_Soft', 'S_ProgressHome', 'DAPChangeActivity', 'LogIn', 'Feedback_RateActivity', 'S_DSVidas', 'ae_session', 'S_IAReminderContinue', 'ActivityView', 'LeanplumVarLoadAttempt', 'PPPaymentStarted', 'OpenApp', 'PromoCodeRedeem', 'ActivityDescription', 'Dismiss_Feedback_RateActivity', 'PPPaymentProvider', 'S_DAPHome', 'ArticleView', 'SkillsViewAllSkills', 'S_IAReminderSet', 'IAReminder', 'S_Paywall', 'S_DAPNewSkills', 'RateActivityDAP', 'CancelledAutorenew', 'S_MenuFamilyHome', 'S_OBUserRole', 'CATSkillDetail', 'SkillsReviewIA', 'GetStartedBTN', 'S_HealthInterest', 'S_OBBabyName', 'AB_TEST']

Outputs= raw_data[variables]

# carga de modelo
h2o.init()
modelopath = '/home/fbfakeevents/Conversion/BehaviorMixpanel/Models/iOS/RW/Grid_DRF_py_3_sid_9122_model_python_1596337999357_44555_model_5'
modelo = h2o.load_model(modelopath)
Outputs = h2o.H2OFrame(Outputs)
Outputs['prediccion'] = modelo.predict(Outputs)['predict']
archivo = open('/home/fbfakeevents/Conversion/Demographics/Predictive/Tokens/tokensOnline.txt', 'r') #cuidar que la primer linea sea el token y la segunda el id de la app
lineas = archivo.read().splitlines()
token = lineas[1] # Kinedu APP  token
path = lineas[2] # Kinedu APP id
archivo.close()


# In[15]:


Outputs['prediccion'].table()

Outputs= Outputs.as_data_frame()
if 'FT_start' in list(Outputs.columns):
    del Outputs['FT_start']

Outputs= pd.merge(Outputs, FT, on='user_id')

Outputs=Outputs[(Outputs['prediccion']=="Premium")| (Outputs['y']=="Premium") | (Outputs['FT_start']==1)]
Outputs['prediccionf']= 'Premium'
Outputs=Outputs.reset_index()

count = 0


for i in range(Outputs.shape[0]):
    print(i)
    query ='' # inicializamos el query al string vacio
    query = "UPDATE fbpredictives SET  date_predicted = NOW(),  model_id = 'Behavior' , prediction = 'Premium' , sended = 0, processed= 1 WHERE user_id =" + str(int(Outputs.loc[i, 'user_id']))
    #print(query)
    cursor.execute(query) #insert en la DB de produccion
    mariadb_connection.commit()
    count +=1


print("Registros Premium Actualizados" " " + str(count))


# In[ ]:
