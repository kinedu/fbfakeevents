#!/usr/bin/env python
# coding: utf-8

# In[1]:


from h2o.estimators.random_forest import H2ORandomForestEstimator #checar que modelo es el mejor 
import h2o 
import urllib
import json 
import requests
import time 
import datetime
import pandas as pd
import numpy as np
import mysql.connector as mariadb
inicio = datetime.datetime.now()
archivo = open('/home/fbfakeevents/Conversion/BehaviorMixpanel/Predictive/Tokens/tokensOnline.txt', 'r') #cuidar que la primer linea lineas = archivo.read().splitlines()
lineas = archivo.read().splitlines()
passKineduDB = lineas[0]
archivo.close()


# In[2]:


from google.cloud import bigquery
from google.oauth2 import service_account

credentials = service_account.Credentials.from_service_account_file(
  '/home/fbfakeevents/Conversion/BehaviorMixpanel/Predictive/Tokens/Credentials/KineduDataWarehouse.json',
    scopes=["https://www.googleapis.com/auth/cloud-platform"],
)

client = bigquery.Client(
    credentials=credentials,
    project=credentials.project_id,
)


# In[3]:


def query_BQ(query_string):
    query_job = client.query(query_string)
    results = query_job.result().to_dataframe()  # Waits for job to complete and cast to pandas dataframe
    return(results)


# In[4]:


query = '''
SELECT distinct user_id,
#max(days_to_convert) as days_to_convert, /*Use only for analysis*/
max(days_to_take_FT) as days_to_take_FT,
#max(days_in_app) as days_in_app,
#max(device) as device,#
max(network) as adjust_network,
#max(screen_height) as screen_height,
max(screen_width) as screenwidth,
#max(android_app_version) as android_app_version,
#max(android_lib_version) as android_lib_version /*.002 null class*/,
max(initial_assessment_completed) as initial_assessment_completed,
max(timezone) as timezone,
max(user_relationship) as user_relationship,
#max(user_role) as user_role,
#max(FT_start) as FT_start,
#max(FT_conversion) as FT_conversion,
(case when country = 'US' then 1 else 2 end) as country,
#kinedu_language,
y,
sum(case when video = "Completed" then 1 else 0 end) as Video_Completed,
sum(case when name ="OpenApp" then 1 else 0 end) as OpenApp,
sum(case when name ="LogOut" then 1 else 0 end) as LogOut,
sum(case when name ="S_DAPNewSkills" then 1 else 0 end) as S_DAPNewSkills,
sum(case when name ="AB_TEST" then 1 else 0 end) as AB_TEST,
sum(case when name ="S_SPHome" then 1 else 0 end) as S_SPHome,
sum(case when name ="S_SPLogin" then 1 else 0 end) as S_SPLogin,
sum(case when name ="LogIn" then 1 else 0 end) as LogIn,
sum(case when name ="SPForgotPass" then 1 else 0 end) as SPForgotPass,
sum(case when name ="S_SPSignup" then 1 else 0 end) as S_SPSignup,
sum(case when name ="SignUp" then 1 else 0 end) as SignUp,
sum(case when name ="OBCreateUser" then 1 else 0 end) as OBCreateUser,
sum(case when name ="S_OBBabyName" then 1 else 0 end) as S_OBBabyName,
sum(case when name ="OBCreateTwins" then 1 else 0 end) as OBCreateTwins,
sum(case when name ="S_OBPremature" then 1 else 0 end) as S_OBPremature,
sum(case when name ="Paywall_Dismiss" then 1 else 0 end) as Paywall_Dismiss,
sum(case when name ="S_InviteReceived" then 1 else 0 end) as S_InviteReceived,
sum(case when name ="InviteReceived" then 1 else 0 end) as InviteReceived,
sum(case when name ="S_Paywall_Soft" then 1 else 0 end) as S_Paywall_Soft,
sum(case when name ="CreateBaby" then 1 else 0 end) as CreateBaby,
sum(case when name ="S_IAIntro" then 1 else 0 end) as S_IAIntro,
sum(case when name ="IAStraightToActivities" then 1 else 0 end) as IAStraightToActivities,
sum(case when name ="IAStartAssessment" then 1 else 0 end) as IAStartAssessment,
sum(case when name ="IASkipAssessment" then 1 else 0 end) as IASkipAssessment,
sum(case when name ="MasterSkill" then 1 else 0 end) as MasterSkill,
sum(case when name ="IAFinishSkill" then 1 else 0 end) as IAFinishSkill,
sum(case when name ="S_IAHome " then 1 else 0 end) as S_IAHome ,
sum(case when name ="S_HealthInterest" then 1 else 0 end) as S_HealthInterest,
sum(case when name ="IAFinishAssessment" then 1 else 0 end) as IAFinishAssessment,
sum(case when name ="S_IASetReminder" then 1 else 0 end) as S_IASetReminder,
sum(case when name ="S_IAReminderSet" then 1 else 0 end) as S_IAReminderSet,
sum(case when name ="S_IAReminderContinue" then 1 else 0 end) as S_IAReminderContinue,
sum(case when name ="IAReminder" then 1 else 0 end) as IAReminder,
sum(case when name ="S_IA_Answer_Skill" then 1 else 0 end) as S_IA_Answer_Skill,
sum(case when name ="S_DAPHome " then 1 else 0 end) as S_DAPHome ,
sum(case when name ="DAPChangeActivity" then 1 else 0 end) as DAPChangeActivity,
sum(case when name ="S_DAPPastPlansPremium" then 1 else 0 end) as S_DAPPastPlansPremium,
sum(case when name ="DAPActivityCompleted" then 1 else 0 end) as DAPActivityCompleted,
sum(case when name ="DAPFullScreenActivity" then 1 else 0 end) as DAPFullScreenActivity,
sum(case when name ="S_DAPMaterials" then 1 else 0 end) as S_DAPMaterials,
sum(case when name ="MilestonesUpdate" then 1 else 0 end) as MilestonesUpdate,
sum(case when name ="Share " then 1 else 0 end) as Share ,
sum(case when name ="ArticleView" then 1 else 0 end) as ArticleView,
sum(case when name ="SlideshowView" then 1 else 0 end) as SlideshowView,
sum(case when name ="ActivityView" then 1 else 0 end) as ActivityView,
sum(case when name ="ClickInDAP" then 1 else 0 end) as ClickInDAP,
sum(case when name ="S_DSVidas " then 1 else 0 end) as S_DSVidas ,
sum(case when name ="RateActivityDAP" then 1 else 0 end) as RateActivityDAP,
sum(case when name ="Feedback_RateActivity" then 1 else 0 end) as Feedback_RateActivity,
sum(case when name ="Dismiss_Feedback_RateActivity" then 1 else 0 end) as Dismiss_Feedback_RateActivity,
sum(case when name ="LifeSpent" then 1 else 0 end) as LifeSpent,
sum(case when name ="WhatchAd" then 1 else 0 end) as WhatchAd,
sum(case when name ="VideoPlayerActivity" then 1 else 0 end) as VideoPlayerActivity,
sum(case when name ="ActivityDescription " then 1 else 0 end) as ActivityDescription ,
sum(case when name ="FreeTrialStart" then 1 else 0 end) as FreeTrialStart,
sum(case when name ="NPSSubmit" then 1 else 0 end) as NPSSubmit,
sum(case when name ="S_NPSScore" then 1 else 0 end) as S_NPSScore,
sum(case when name ="S_RWDashboard" then 1 else 0 end) as S_RWDashboard,
sum(case when name ="SkillsDetail " then 1 else 0 end) as SkillsDetail ,
sum(case when name ="S_ProgressHome" then 1 else 0 end) as S_ProgressHome,
sum(case when name ="S_ProgressSkillHome  " then 1 else 0 end) as S_ProgressSkillHome  ,
sum(case when name ="OverTime" then 1 else 0 end) as OverTime,
sum(case when name ="ComparedAge" then 1 else 0 end) as ComparedAge,
sum(case when name ="S_MenuFamilyHome" then 1 else 0 end) as S_MenuFamilyHome,
sum(case when name ="S_MenuSetReminders" then 1 else 0 end) as S_MenuSetReminders,
sum(case when name ="MenuHelp" then 1 else 0 end) as MenuHelp,
sum(case when name ="MenuMemberStats" then 1 else 0 end) as MenuMemberStats,
sum(case when name ="MenuFamilyInvite" then 1 else 0 end) as MenuFamilyInvite,
sum(case when name ="MenuChangeBirthday" then 1 else 0 end) as MenuChangeBirthday,
sum(case when name ="S_MenuEditBaby" then 1 else 0 end) as S_MenuEditBaby,
sum(case when name ="S_MenuUserProfile" then 1 else 0 end) as S_MenuUserProfile,
sum(case when name ="MenuFamilyDefault" then 1 else 0 end) as MenuFamilyDefault,
sum(case when name ="DailyReminders" then 1 else 0 end) as DailyReminders,
sum(case when name ="S_MenuFamilyProfile" then 1 else 0 end) as S_MenuFamilyProfile,
sum(case when name ="TAPCallToAction" then 1 else 0 end) as TAPCallToAction,
sum(case when name ="S_PremiumProcess" then 1 else 0 end) as S_PremiumProcess,
sum(case when name ="RestorePurchase" then 1 else 0 end) as RestorePurchase,
sum(case when name ="CATSkillDetail " then 1 else 0 end) as CATSkillDetail ,
sum(case when name ="S_CATHome_P" then 1 else 0 end) as S_CATHome_P,
sum(case when name ="S_CATHome_F" then 1 else 0 end) as S_CATHome_F,
sum(case when name ="S_CATSkill" then 1 else 0 end) as S_CATSkill,
sum(case when name ="CATSearch" then 1 else 0 end) as CATSearch,
sum(case when name ="S_CATViewSkill" then 1 else 0 end) as S_CATViewSkill,
sum(case when name ="S_CollectionView" then 1 else 0 end) as S_CollectionView,
sum(case when name ="ViewAllCollections" then 1 else 0 end) as ViewAllCollections,
sum(case when name ="S_Searcher " then 1 else 0 end) as S_Searcher ,
sum(case when name ="S_SearchResults " then 1 else 0 end) as S_SearchResults ,
sum(case when name ="S_SearchEmptyState" then 1 else 0 end) as S_SearchEmptyState,
sum(case when name ="ResetSearch" then 1 else 0 end) as ResetSearch,
sum(case when name ="S_CatalogHome " then 1 else 0 end) as S_CatalogHome ,
sum(case when name ="S_MilestonesHome" then 1 else 0 end) as S_MilestonesHome,
sum(case when name ="SkillsCardNotPersonalize " then 1 else 0 end) as SkillsCardNotPersonalize ,
sum(case when name ="SkillsViewAllSkills" then 1 else 0 end) as SkillsViewAllSkills,
sum(case when name ="S_PromoCode" then 1 else 0 end) as S_PromoCode,
sum(case when name ="PromoCodeRedeem" then 1 else 0 end) as PromoCodeRedeem,
sum(case when name ="S_FeedHome" then 1 else 0 end) as S_FeedHome,
sum(case when name ="InviteClassrooms" then 1 else 0 end) as InviteClassrooms,
sum(case when name ="shown" then 1 else 0 end) as shown,
sum(case when name ="clicked" then 1 else 0 end) as clicked,
sum(case when name ="dismissed" then 1 else 0 end) as dismissed
from
(
SELECT u.user_id,

timestamp_diff(ued.premium_conversion_date,u.created_at, day) as days_to_convert,/*Use only for analysis*/
timestamp_diff(ued.trial_start ,u.created_at, day) as days_to_take_FT,/*Use only for analysis*/
timestamp_diff(timestamp(current_date('UTC')),u.created_at, day) as days_in_app,

ev.name, ev.screen_height,ev.screen_width,ev.properties,
u.country,
u.kinedu_language,
u.network,
IF(ev.name= "VideoPlayerActivity" and JSON_EXTRACT(ev.properties, "$.video_completed")="true", "Completed", "Not Completed") as Video,
if (ev.device is null, 'NULL', ev.device) as device,
/*0.01915nulls */
if (p.android_app_version is null, 'NULL', android_app_version) as android_app_version, /*all related to android will be categoric 0.001 nulls*/
if( p.android_app_version_code is null, 'NULL', cast( android_app_version_code as string) ) as android_app_version_code,
if( android_brand is null, 'NULL', android_brand) as android_brand,
if(android_lib_version is null, 'NULL', android_lib_version)  as android_lib_version,
if(android_manufacturer is null, 'NULL', android_manufacturer) as android_manufacturer,
if(android_os_version is null, 'NULL', android_os_version) as android_os_version,
if(ued.premium_conversion_date is not null, 'Premium', 'Freemium') as y,
if(p.initial_assessment_completed is  null or initial_assessment_completed="NO", 0, 1) as initial_assessment_completed,
p.timezone,
if(p.user_relationship is null, 'NULL', user_relationship) as user_relationship,
if(p.user_role is null, 'NULL', user_role) as user_role,
if(ued.trial_start is not null, 1, 0) as FT_start,
if(ued.trial_converted is not null , 1, 0) as FT_conversion
FROM `celtic-music-240111.mixpanel.event` as ev
left join mixpanel.people as p
on p.distinct_id = ev.distinct_id
inner join celtic-music-240111.dbt_prod_caf.caf_users as u
on u.email = p.user_email
inner join aws_kinedu_app.user_extra_data as ued
on u.user_id = ued.user_id
where DATE(ev.time)>= '2020-01-01' and  timestamp_diff(timestamp(DATE(ev.time)) , u.created_at, day) <1 /*only the first 2 day events*/
and u.os='Android'
#and u.network in ('Facebook', 'SMARTLY - Facebook', 'Facebook Installs', 'Instagram', 'SMARTLY - Instagram')
)
where kinedu_language in ('en') and country in('US','GB','CA','AU') and FT_start=1 and days_in_app<=4 
#and days_to_take_FT<1
group by user_id,y, country,kinedu_language

'''


# In[5]:


import datetime
start = datetime.datetime.now()
raw_data = query_BQ(query)
end = datetime.datetime.now()
print(end - start)


# In[7]:


len(raw_data)


# In[8]:


mariadb_connection = mariadb.connect(host = 'dbmaster.c6ji2pa9hmrh.us-west-2.rds.amazonaws.com', user='root',
                                     password= passKineduDB, database='kinedu_app', port=3306)
cursor = mariadb_connection.cursor()
# la consulta
string = '''
SELECT user_id, model_id,prediction, sended_at
FROM fbpredictives
WHERE 
user_id>6413364
and ((model_id is null) or (model_id= 'Demographics') or (model_id= 'Behavior' and prediction='Freemium'))
;
'''


cursor.execute(string)
rw_data  = pd.DataFrame(cursor.fetchall())
rw_data.columns = cursor.column_names


# In[9]:


l1= raw_data['user_id'].values.tolist()
l2=rw_data['user_id'].tolist()

def list_duplicates(seq):
  seen = set()
  seen_add = seen.add
  # adds all elements it doesn't know yet to seen and all other to seen_twice
  seen_twice = set( x for x in seq if x in seen or seen_add(x) )
  # turn the set into a list (as requested)
  return list( seen_twice )


# In[10]:


l= l1+l2
users=list_duplicates(l) # yi


# In[11]:


premium_before= rw_data[rw_data['prediction'] =='Premium']
premium_before = premium_before[premium_before['user_id'].isin(users)]
type(rw_data)
premium_before.reset_index(drop=True)
usbefPrem = premium_before['user_id'].tolist()
print("Usuarios premium Demographics" " " + str(len(usbefPrem)))


# In[12]:


raw_data=raw_data[raw_data['user_id'].isin(users)]


# In[13]:


len(raw_data)


# In[14]:


FT= raw_data[['user_id','y']]
#'days_in_app'


# In[15]:


try:
    raw_data['screenwidth']=(raw_data['screenwidth']-1069.19985141158)/181.820988813783
    raw_data['DAPActivityCompleted']=(raw_data['DAPActivityCompleted']-1.66084695393759)/3.15082013289035
    raw_data['DAPFullScreenActivity']=(raw_data['DAPFullScreenActivity']-4.81872213967311)/6.95880366848564
    raw_data['ActivityView']=(raw_data['ActivityView']-5.20988112927191)/8.96117429815524
    raw_data['shown']=(raw_data['shown']-5.24777117384844)/11.7482216627023
    raw_data['VideoPlayerActivity']=(raw_data['VideoPlayerActivity']-4.38410104011887)/15.556478652497
    raw_data['MilestonesUpdate']=(raw_data['MilestonesUpdate']-1.43016344725111)/2.76638194986909
    raw_data['OpenApp']=(raw_data['OpenApp']-4.96099554234769)/3.77443258448205
    raw_data['S_ProgressHome']=(raw_data['S_ProgressHome']-1.81277860326894)/4.0855602380314
    raw_data['ArticleView']=(raw_data['ArticleView']-0.78120356612184)/2.12365146407239
    raw_data['Feedback_RateActivity']=(raw_data['Feedback_RateActivity']-0.376671619613669)/0.988611287166622
    raw_data['S_MenuFamilyProfile']=(raw_data['S_MenuFamilyProfile']-0.536032689450223)/1.43760412566165
    raw_data['Video_Completed']=(raw_data['Video_Completed']-0.528231797919764)/1.71153152607712
    raw_data['MenuHelp']=(raw_data['MenuHelp']-0.101411589895988)/0.437602092333395
    raw_data['MenuFamilyInvite']=(raw_data['MenuFamilyInvite']-0.171248142644873)/0.488460145104894
    raw_data['S_MilestonesHome']=(raw_data['S_MilestonesHome']-1.30460624071322)/5.22593306466547
    raw_data['S_IA_Answer_Skill']=(raw_data['S_IA_Answer_Skill']-3.39041604754829)/5.26688937105967
    raw_data['S_NPSScore']=(raw_data['S_NPSScore']-0.0906389301634473)/0.415190965827579
    raw_data['S_DAPPastPlansPremium']=(raw_data['S_DAPPastPlansPremium']-0.283432392273402)/0.957813432664818
    raw_data['S_MenuUserProfile']=(raw_data['S_MenuUserProfile']-0.568350668647846)/1.25973333888044
    raw_data['MenuMemberStats']=(raw_data['MenuMemberStats']-0.187221396731055)/0.997887105640189
    raw_data['NPSSubmit']=(raw_data['NPSSubmit']-0.0345468053491827)/0.182662853330439
    raw_data['S_DAPMaterials']=(raw_data['S_DAPMaterials']-0.757429420505201)/1.10143337771317
    raw_data['S_PremiumProcess']=(raw_data['S_PremiumProcess']-0.101411589895988)/1.0129805310485
    raw_data['SkillsViewAllSkills']=(raw_data['SkillsViewAllSkills']-0.317607726597325)/1.53461792365198
    raw_data['S_DAPNewSkills']=(raw_data['S_DAPNewSkills']-0.0564635958395245)/0.249427090433234
    raw_data['AB_TEST']=(raw_data['AB_TEST']-0.299034175334324)/0.778825471556529
    raw_data['S_CATHome_P']=(raw_data['S_CATHome_P']-1.01708766716196)/2.72546751176337
    raw_data['RateActivityDAP']=(raw_data['RateActivityDAP']-0.0334323922734026)/0.250596527029248
    raw_data['DAPChangeActivity']=(raw_data['DAPChangeActivity']-0.199851411589896)/0.912459676331368
    raw_data['S_CATHome_F']=(raw_data['S_CATHome_F']-0.0219167904903417)/0.303397649202877
    raw_data['dismissed']=(raw_data['dismissed']-0.12927191679049)/0.721031243851832
    raw_data['S_MenuFamilyHome']=(raw_data['S_MenuFamilyHome']-1.88075780089153)/3.01690537523779
    raw_data['Dismiss_Feedback_RateActivity']=(raw_data['Dismiss_Feedback_RateActivity']-0.0516344725111441)/0.27656034028985
    raw_data['S_IASetReminder']=(raw_data['S_IASetReminder']-0.0794947994056463)/0.304181262414387
    raw_data['S_CATViewSkill']=(raw_data['S_CATViewSkill']-0.0716939078751857)/1.03688265103882
    raw_data['ViewAllCollections']=(raw_data['ViewAllCollections']-0.0278603268945022)/0.203015611083743
    raw_data['ClickInDAP']=(raw_data['ClickInDAP']-0.0111441307578008)/0.141218454286317
    raw_data['MenuFamilyDefault']=(raw_data['MenuFamilyDefault']-0.0178306092124814)/0.304276548627262
    raw_data['S_SPLogin']=(raw_data['S_SPLogin']-0.105497771173848)/0.46266746061356
    raw_data['S_SearchEmptyState']=(raw_data['S_SearchEmptyState']-0.00445765230312035)/0.090307925629885
    raw_data['S_PromoCode']=(raw_data['S_PromoCode']-0.00817236255572065)/0.146582787436198
    raw_data['country']=(raw_data['country']-1.37592867756315)/0.484451740403179
    raw_data['S_IAIntro']=(raw_data['S_IAIntro']-1.64227340267459)/0.898098508817688
    raw_data['IAFinishSkill']=(raw_data['IAFinishSkill']-7.92533432392272)/5.36978873571266
    raw_data['clicked']=(raw_data['clicked']-0.101040118870728)/0.399980066238331
    raw_data['S_SPSignup']=(raw_data['S_SPSignup']-0.000371471025260029)/0.0192735836122924
    raw_data['OverTime']=(raw_data['OverTime']-0.000371471025260029)/0.0192735836122924
    raw_data['ResetSearch']=(raw_data['ResetSearch']-0.000371471025260029)/0.0192735836122924
    raw_data['SlideshowView']=(raw_data['SlideshowView']-0.00222882615156017)/0.054478463873776
    raw_data['S_OBPremature']=(raw_data['S_OBPremature']-0.113670133729569)/0.334566803156471
    raw_data['IAFinishAssessment']=(raw_data['IAFinishAssessment']-0.946136701337296)/0.509730593037368
    raw_data['LogOut']=(raw_data['LogOut']-0.000371471025260029)/0.0192735836122924
    raw_data['S_RWDashboard']=(raw_data['S_RWDashboard']-0.000371471025260029)/0.0192735836122924
    raw_data['InviteClassrooms']=(raw_data['InviteClassrooms']-0.000371471025260029)/0.0192735836122924
    raw_data['Paywall_Dismiss']=(raw_data['Paywall_Dismiss']-0.0185735512630014)/0.140434271561746
    raw_data['SPForgotPass']=(raw_data['SPForgotPass']-0.00148588410104011)/0.0609416278137779
    raw_data['PromoCodeRedeem']=(raw_data['PromoCodeRedeem']-0.013372956909361)/0.148720010765547
    raw_data['S_FeedHome']=(raw_data['S_FeedHome']-0.365527488855868)/0.865182021168485
    raw_data['LogIn']=(raw_data['LogIn']-0.854011887072808)/0.409668849427026
    raw_data['IASkipAssessment']=(raw_data['IASkipAssessment']-0.302748885586924)/0.725973931167839
    raw_data['IAStraightToActivities']=(raw_data['IAStraightToActivities']-0.0794947994056463)/0.285268239357579
    raw_data['OBCreateTwins']=(raw_data['OBCreateTwins']-0.013001485884101)/0.116535119329676
    raw_data['LifeSpent']=(raw_data['LifeSpent']-0.0516344725111441)/0.588450341705142
    raw_data['S_HealthInterest']=(raw_data['S_HealthInterest']-0.856612184249628)/0.433908503936389
    raw_data['IAReminder']=(raw_data['IAReminder']-0.0234026745913818)/0.17405649623003
    raw_data['CATSearch']=(raw_data['CATSearch']-0.0282317979197622)/0.302261009860336
    raw_data['S_MenuEditBaby']=(raw_data['S_MenuEditBaby']-0.208023774145616)/0.594633090447455
    raw_data['TAPCallToAction']=(raw_data['TAPCallToAction']-1.24368499257058)/0.661670982711352
    raw_data['S_SPHome']=(raw_data['S_SPHome']-1.90230312035661)/0.769471801116944
    raw_data['S_IAReminderContinue']=(raw_data['S_IAReminderContinue']-0.0185735512630014)/0.16021089134406
    raw_data['S_Paywall_Soft']=(raw_data['S_Paywall_Soft']-2.27154531946508)/0.921379971673091
    raw_data['MasterSkill']=(raw_data['MasterSkill']-0.0898959881129272)/0.711493258808971
    raw_data['RestorePurchase']=(raw_data['RestorePurchase']-0.00185735512630014)/0.0430650007114352
    raw_data['MenuChangeBirthday']=(raw_data['MenuChangeBirthday']-0.0234026745913818)/0.184422760397102
except:
    pass


# In[16]:


#Filtrar Variables que se ocupan en el modelo 
variables=['user_id','screenwidth', 'DAPActivityCompleted', 'DAPFullScreenActivity', 'ActivityView', 'shown', 'VideoPlayerActivity', 'MilestonesUpdate', 'OpenApp', 'S_ProgressHome', 'ArticleView', 'Feedback_RateActivity', 'S_MenuFamilyProfile', 'Video_Completed', 'MenuHelp', 'MenuFamilyInvite', 'S_MilestonesHome', 'S_IA_Answer_Skill', 'S_NPSScore', 'S_DAPPastPlansPremium', 'S_MenuUserProfile', 'MenuMemberStats', 'NPSSubmit', 'S_DAPMaterials', 'S_PremiumProcess', 'SkillsViewAllSkills', 'S_DAPNewSkills', 'AB_TEST', 'S_CATHome_P', 'RateActivityDAP', 'DAPChangeActivity', 'S_CATHome_F', 'dismissed', 'S_MenuFamilyHome', 'Dismiss_Feedback_RateActivity', 'S_IASetReminder', 'S_CATViewSkill', 'ViewAllCollections', 'ClickInDAP', 'MenuFamilyDefault', 'S_SPLogin', 'S_SearchEmptyState', 'S_PromoCode', 'country', 'S_IAIntro', 'IAFinishSkill', 'clicked', 'S_SPSignup', 'OverTime', 'ResetSearch', 'SlideshowView', 'S_OBPremature', 'IAFinishAssessment', 'LogOut', 'S_RWDashboard', 'InviteClassrooms', 'Paywall_Dismiss', 'SPForgotPass', 'PromoCodeRedeem', 'S_FeedHome', 'LogIn', 'IASkipAssessment', 'IAStraightToActivities', 'OBCreateTwins', 'LifeSpent', 'S_HealthInterest', 'IAReminder', 'CATSearch', 'S_MenuEditBaby', 'TAPCallToAction', 'S_SPHome', 'S_IAReminderContinue', 'S_Paywall_Soft', 'MasterSkill', 'RestorePurchase', 'MenuChangeBirthday', 'adjust_network', 'timezone', 'user_relationship']
Outputs= raw_data[variables]


# In[17]:


# carga de modelo 
h2o.init()
modelopath = '/home/fbfakeevents/Conversion/BehaviorMixpanel/Models/aOS/EN/FT/Grid_XGBoost_py_3_sid_b68e_model_python_1607122237353_2314_model_3' # 10 ó 3
modelo = h2o.load_model(modelopath)
Outputs = h2o.H2OFrame(Outputs)
Outputs['prediccion'] = modelo.predict(Outputs)['predict']
Outputs['p_premium']= modelo.predict(Outputs)['Premium']
archivo = open('/home/fbfakeevents/Conversion/BehaviorMixpanel/Predictive/Tokens/tokensOnline.txt', 'r') #cuidar que la primer linea sea el token y la segunda el id de la app 
lineas = archivo.read().splitlines()
token = lineas[1] # Kinedu APP  token
path = lineas[2] # Kinedu APP id
archivo.close()


# In[18]:


Outputs


# In[19]:


Outputs['prediccion'].table()


# In[20]:


Outputs= Outputs.as_data_frame()
Outputs=Outputs.sort_values(by=['p_premium'],ascending=False).reset_index(drop= True)
Outputs= Outputs.iloc[:35]


# In[21]:


Outputs=Outputs[Outputs['prediccion']=="Premium"]
Outputs=Outputs.reset_index(drop=True)


# In[22]:


Outputs.shape


# In[23]:


count = 0


# In[24]:


for i in range(Outputs.shape[0]):
    print(i)
    query ='' # inicializamos el query al string vacio
#if Outputs[i, 'prediccionf'] == 'Premium':
    # actualizamos en la base de kinedu 
    query = "UPDATE fbpredictives SET  date_predicted = NOW(),  model_id = 'Behavior' , prediction = 'Premium' , sended = 0, processed= 1 WHERE user_id =" + str(int(Outputs.loc[i, 'user_id'])) 
    #print(query)
    cursor.execute(query) #insert en la DB de produccion 
    mariadb_connection.commit()
    count +=1
 #print(query)
        


# In[25]:


print("Registros Premium Actualizados" " " + str(count))


# In[ ]:




