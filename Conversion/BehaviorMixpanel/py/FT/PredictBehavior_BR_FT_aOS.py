#!/usr/bin/env python
# coding: utf-8

# In[40]:


from h2o.estimators.random_forest import H2ORandomForestEstimator #checar que modelo es el mejor
import h2o
import urllib
import json
import requests
import time
import datetime
import pandas as pd
import numpy as np
import mysql.connector as mariadb
inicio = datetime.datetime.now()
archivo = open('/home/fbfakeevents/Conversion/BehaviorMixpanel/Predictive/Tokens/tokensOnline.txt', 'r') #cuidar que la primer linea lineas = archivo.read().splitlines()
lineas = archivo.read().splitlines()
passKineduDB = lineas[0]
archivo.close()


from google.cloud import bigquery
from google.oauth2 import service_account

credentials = service_account.Credentials.from_service_account_file(
  '/home/fbfakeevents/Conversion/BehaviorMixpanel/Predictive/Tokens/Credentials/KineduDataWarehouse.json',
    scopes=["https://www.googleapis.com/auth/cloud-platform"],
)

client = bigquery.Client(
    credentials=credentials,
    project=credentials.project_id,
)



def query_BQ(query_string):
    query_job = client.query(query_string)
    results = query_job.result().to_dataframe()  # Waits for job to complete and cast to pandas dataframe
    return(results)


query = '''
SELECT distinct user_id,
max(baby_age) as baby_age,
max(days_to_take_FT) as days_to_take_FT,
max(days_in_app) as days_in_app,
max(device) as device,#
max(adjust_network) as adjust_network,
max(android_app_version) as android_app_version,
max(android_lib_version) as android_lib_version /*.002 null class*/,
max(screen_width) as screenwidth,
max(timezone) as timezone,
max(user_relationship) as user_relationship,
max(user_role) as user_role,
y,
sum(case when video = "Completed" then 1 else 0 end) as Video_Completed,
sum(case when name ="OpenApp" then 1 else 0 end) as OpenApp,
sum(case when name ="LogOut" then 1 else 0 end) as LogOut,
sum(case when name ="S_DAPNewSkills" then 1 else 0 end) as S_DAPNewSkills,
sum(case when name ="AB_TEST" then 1 else 0 end) as AB_TEST,
sum(case when name ="S_SPHome" then 1 else 0 end) as S_SPHome,
sum(case when name ="S_SPLogin" then 1 else 0 end) as S_SPLogin,
sum(case when name ="LogIn" then 1 else 0 end) as LogIn,
sum(case when name ="SPForgotPass" then 1 else 0 end) as SPForgotPass,
sum(case when name ="S_SPSignup" then 1 else 0 end) as S_SPSignup,
sum(case when name ="SignUp" then 1 else 0 end) as SignUp,
sum(case when name ="OBCreateUser" then 1 else 0 end) as OBCreateUser,
sum(case when name ="S_OBBabyName" then 1 else 0 end) as S_OBBabyName,
sum(case when name ="OBCreateTwins" then 1 else 0 end) as OBCreateTwins,
sum(case when name ="S_OBPremature" then 1 else 0 end) as S_OBPremature,
sum(case when name ="Paywall_Dismiss" then 1 else 0 end) as Paywall_Dismiss,
sum(case when name ="S_Paywall_Soft" then 1 else 0 end) as S_Paywall_Soft,
sum(case when name ="S_IAIntro" then 1 else 0 end) as S_IAIntro,
sum(case when name ="IAStraightToActivities" then 1 else 0 end) as IAStraightToActivities,
sum(case when name ="IAStartAssessment" then 1 else 0 end) as IAStartAssessment,
sum(case when name ="IASkipAssessment" then 1 else 0 end) as IASkipAssessment,
sum(case when name ="MasterSkill" then 1 else 0 end) as MasterSkill,
sum(case when name ="IAFinishSkill" then 1 else 0 end) as IAFinishSkill,
sum(case when name ="S_HealthInterest" then 1 else 0 end) as S_HealthInterest,
sum(case when name ="S_IASetReminder" then 1 else 0 end) as S_IASetReminder,
sum(case when name ="IAReminder" then 1 else 0 end) as IAReminder,
sum(case when name ="S_IA_Answer_Skill" then 1 else 0 end) as S_IA_Answer_Skill,
sum(case when name ="DAPChangeActivity" then 1 else 0 end) as DAPChangeActivity,
sum(case when name ="S_DAPPastPlansPremium" then 1 else 0 end) as S_DAPPastPlansPremium,
sum(case when name ="DAPActivityCompleted" then 1 else 0 end) as DAPActivityCompleted,
sum(case when name ="S_DAPMaterials" then 1 else 0 end) as S_DAPMaterials,
sum(case when name ="MilestonesUpdate" then 1 else 0 end) as MilestonesUpdate,
sum(case when name ="ArticleView" then 1 else 0 end) as ArticleView,
sum(case when name ="SlideshowView" then 1 else 0 end) as SlideshowView,
sum(case when name ="ActivityView" then 1 else 0 end) as ActivityView,
sum(case when name ="ClickInDAP" then 1 else 0 end) as ClickInDAP,
sum(case when name ="RateActivityDAP" then 1 else 0 end) as RateActivityDAP,
sum(case when name ="Feedback_RateActivity" then 1 else 0 end) as Feedback_RateActivity,
sum(case when name ="Dismiss_Feedback_RateActivity" then 1 else 0 end) as Dismiss_Feedback_RateActivity,
sum(case when name ="LifeSpent" then 1 else 0 end) as LifeSpent,
sum(case when name ="WhatchAd" then 1 else 0 end) as WhatchAd,
sum(case when name ="VideoPlayerActivity" then 1 else 0 end) as VideoPlayerActivity,
sum(case when name ="NPSSubmit" then 1 else 0 end) as NPSSubmit,
sum(case when name ="S_NPSScore" then 1 else 0 end) as S_NPSScore,
sum(case when name ="S_ProgressHome" then 1 else 0 end) as S_ProgressHome,
sum(case when name ="S_MenuFamilyHome" then 1 else 0 end) as S_MenuFamilyHome,
sum(case when name ="MenuHelp" then 1 else 0 end) as MenuHelp,
sum(case when name ="MenuMemberStats" then 1 else 0 end) as MenuMemberStats,
sum(case when name ="MenuFamilyInvite" then 1 else 0 end) as MenuFamilyInvite,
sum(case when name ="MenuChangeBirthday" then 1 else 0 end) as MenuChangeBirthday,
sum(case when name ="S_MenuEditBaby" then 1 else 0 end) as S_MenuEditBaby,
sum(case when name ="S_MenuUserProfile" then 1 else 0 end) as S_MenuUserProfile,
sum(case when name ="MenuFamilyDefault" then 1 else 0 end) as MenuFamilyDefault,
sum(case when name ="S_MenuFamilyProfile" then 1 else 0 end) as S_MenuFamilyProfile,
sum(case when name ="TAPCallToAction" then 1 else 0 end) as TAPCallToAction,
sum(case when name ="S_PremiumProcess" then 1 else 0 end) as S_PremiumProcess,
sum(case when name ="RestorePurchase" then 1 else 0 end) as RestorePurchase,
sum(case when name ="S_CATHome_P" then 1 else 0 end) as S_CATHome_P,
sum(case when name ="S_CATHome_F" then 1 else 0 end) as S_CATHome_F,
sum(case when name ="CATSearch" then 1 else 0 end) as CATSearch,
sum(case when name ="S_CATViewSkill" then 1 else 0 end) as S_CATViewSkill,
sum(case when name ="S_CollectionView" then 1 else 0 end) as S_CollectionView,
sum(case when name ="ViewAllCollections" then 1 else 0 end) as ViewAllCollections,
sum(case when name ="S_SearchEmptyState" then 1 else 0 end) as S_SearchEmptyState,
sum(case when name ="ResetSearch" then 1 else 0 end) as ResetSearch,
sum(case when name ="S_MilestonesHome" then 1 else 0 end) as S_MilestonesHome,
sum(case when name ="SkillsViewAllSkills" then 1 else 0 end) as SkillsViewAllSkills,
sum(case when name ="S_PromoCode" then 1 else 0 end) as S_PromoCode,
sum(case when name ="PromoCodeRedeem" then 1 else 0 end) as PromoCodeRedeem,
sum(case when name ="S_FeedHome" then 1 else 0 end) as S_FeedHome,
sum(case when name ="InviteClassrooms" then 1 else 0 end) as InviteClassrooms,
sum(case when name ="shown" then 1 else 0 end) as shown,
sum(case when name ="clicked" then 1 else 0 end) as clicked,
sum(case when name ="dismissed" then 1 else 0 end) as dismissed
from
(
SELECT u.id as user_id,
date_diff(current_date('UTC'),date(b.birthday), month) as baby_age,
#timestamp_diff(timestamp(current_date('UTC')),b.birthday, day) as baby_ageD,
timestamp_diff(ued.trial_start ,u.created_at, day) as days_to_take_FT,/*Use only for analysis*/
timestamp_diff(timestamp(current_date('UTC')) ,u.created_at, day) as days_in_app,/*Use only for analysis*/

#timestamp_diff(ued.premium_conversion_date,u.created_at, day) as days_to_convert,/*Use only for analysis*/
ev.name, ev.screen_height,ev.screen_width,ev.properties,
u.mp_country,
u.adjust_network,
IF(ev.name= "VideoPlayerActivity" and JSON_EXTRACT(ev.properties, "$.video_completed")="true", "Completed", "Not Completed") as Video,
if (ev.device is null, 'NULL', ev.device) as device,
/*0.01915nulls */
if (p.android_app_version is null, 'NULL', android_app_version) as android_app_version, /*all related to android will be categoric 0.001 nulls*/
if( p.android_app_version_code is null, 'NULL', cast( android_app_version_code as string) ) as android_app_version_code,
if( android_brand is null, 'NULL', android_brand) as android_brand,
if(android_lib_version is null, 'NULL', android_lib_version)  as android_lib_version,
if(android_manufacturer is null, 'NULL', android_manufacturer) as android_manufacturer,
if(android_os_version is null, 'NULL', android_os_version) as android_os_version,
if(ued.premium_conversion_date is not null, 'Premium', 'Freemium') as y,
if(p.initial_assessment_completed is  null or initial_assessment_completed="NO", 0, 1) as initial_assessment_completed,
p.timezone,
if(p.user_relationship is null, 'NULL', user_relationship) as user_relationship,
if(p.user_role is null, 'NULL', user_role) as user_role,
if(ued.trial_start is not null, 1, 0) as FT_start,
if(ued.trial_converted is not null , 1, 0) as FT_conversion
FROM `celtic-music-240111.mixpanel.event` as ev
left join mixpanel.people as p
on p.distinct_id = ev.distinct_id
inner join aws_kinedu_app.users as u
on u.email = p.user_email
inner join aws_kinedu_app.user_extra_data as ued
on u.id = ued.user_id
left join `celtic-music-240111.aws_kinedu_app.babies` as b
on  u.id =b.author_id
where DATE(ev.time) >= '2020-01-01' and  timestamp_diff(timestamp(DATE(ev.time)) , ued.trial_start, day) <= 3 /*only the first day events*/
and u.mp_os='Android'
#and u.adjust_network in ('Facebook', 'SMARTLY - Facebook', 'Facebook Installs', 'Instagram', 'SMARTLY - Instagram')
and (u.created_at >='2020-01-01')
)
where mp_country ='BR'
and user_id > 6413364
and days_to_take_FT>=0 and days_to_take_FT<=3 #
group by user_id,y, mp_country

'''




import datetime
start = datetime.datetime.now()
raw_data = query_BQ(query)
end = datetime.datetime.now()
print(end - start)


mariadb_connection = mariadb.connect(host = 'dbmaster.c6ji2pa9hmrh.us-west-2.rds.amazonaws.com', user='root',
                                     password= passKineduDB, database='kinedu_app', port=3306)
cursor = mariadb_connection.cursor()
# la consulta
string = '''
SELECT user_id, model_id,prediction, sended_at
FROM fbpredictives
WHERE
user_id>6413364
and ((model_id is null) or (model_id= 'Demographics') or (model_id= 'Behavior' and prediction='Freemium'))
;
'''

cursor.execute(string)
rw_data  = pd.DataFrame(cursor.fetchall())
rw_data.columns = cursor.column_names


l1= raw_data['user_id'].values.tolist()
l2=rw_data['user_id'].tolist()

def list_duplicates(seq):
  seen = set()
  seen_add = seen.add
  seen_twice = set( x for x in seq if x in seen or seen_add(x) )
  return list( seen_twice )


l= l1+l2
users=list_duplicates(l) # yi

premium_before= rw_data[rw_data['prediction'] =='Premium']
premium_before = premium_before[premium_before['user_id'].isin(users)]
type(rw_data)
premium_before.reset_index(drop=True)
usbefPrem = premium_before['user_id'].tolist()
print("Usuarios premium Demographics" " " + str(len(usbefPrem)))

raw_data=raw_data[raw_data['user_id'].isin(users)]

#FT= raw_data[['user_id','y','days_in_app']]

try:
    raw_data['MenuHelp']=(raw_data['MenuHelp']-0.132159904534606)/0.648311896096211
    raw_data['S_PromoCode']=(raw_data['S_PromoCode']-0.0137231503579952)/0.188737449525357
    raw_data['days_to_take_FT']=(raw_data['days_to_take_FT']-0.0498210023866348)/0.306441290589511
    raw_data['S_CATHome_F']=(raw_data['S_CATHome_F']-0.0865155131264916)/0.772341659288833
    raw_data['NPSSubmit']=(raw_data['NPSSubmit']-0.0268496420047732)/0.19664716650127
    raw_data['S_PremiumProcess']=(raw_data['S_PremiumProcess']-0.677207637231503)/4.3606696467714
    raw_data['ClickInDAP']=(raw_data['ClickInDAP']-0.0134248210023866)/0.181505264011513
    raw_data['S_MenuFamilyProfile']=(raw_data['S_MenuFamilyProfile']-0.536694510739858)/1.50414592419054
    raw_data['MilestonesUpdate']=(raw_data['MilestonesUpdate']-1.17750596658711)/3.23418881719147
    raw_data['DAPActivityCompleted']=(raw_data['DAPActivityCompleted']-1.17929594272076)/2.83281508092727
    raw_data['S_MilestonesHome']=(raw_data['S_MilestonesHome']-1.12768496420048)/4.9543228680284
    raw_data['MenuFamilyInvite']=(raw_data['MenuFamilyInvite']-0.159904534606205)/0.667775376814508
    raw_data['S_ProgressHome']=(raw_data['S_ProgressHome']-1.54027446300715)/4.32417240742086
    raw_data['Paywall_Dismiss']=(raw_data['Paywall_Dismiss']-0.0426610978520286)/0.263631335490622
    raw_data['S_CATViewSkill']=(raw_data['S_CATViewSkill']-0.043854415274463)/0.830368357272824
    raw_data['S_NPSScore']=(raw_data['S_NPSScore']-0.0480310262529833)/0.304774992597135
    raw_data['OpenApp']=(raw_data['OpenApp']-5.54982100238662)/5.14167180848347
    raw_data['Feedback_RateActivity']=(raw_data['Feedback_RateActivity']-0.377684964200476)/1.26541239420255
    raw_data['S_DAPNewSkills']=(raw_data['S_DAPNewSkills']-0.04236276849642)/0.234316779149934
    raw_data['S_Paywall_Soft']=(raw_data['S_Paywall_Soft']-2.56205250596659)/1.58487075262442
    raw_data['LogIn']=(raw_data['LogIn']-1.0035799522673)/0.521102588995746
    raw_data['MenuMemberStats']=(raw_data['MenuMemberStats']-0.144689737470167)/1.07393434057599
    raw_data['S_DAPMaterials']=(raw_data['S_DAPMaterials']-1.00745823389021)/1.43149690578501
    raw_data['SkillsViewAllSkills']=(raw_data['SkillsViewAllSkills']-0.255369928400954)/1.37910866743296
    raw_data['S_MenuFamilyHome']=(raw_data['S_MenuFamilyHome']-2.31682577565633)/5.31326979154305
except:
    pass

#Filtrar Variables que se ocupan en el modelo
variables=['user_id','MenuHelp', 'S_PromoCode', 'days_to_take_FT', 'S_CATHome_F', 'NPSSubmit', 'S_PremiumProcess', 'ClickInDAP', 'S_MenuFamilyProfile', 'MilestonesUpdate', 'DAPActivityCompleted', 'S_MilestonesHome', 'MenuFamilyInvite', 'S_ProgressHome', 'Paywall_Dismiss', 'S_CATViewSkill', 'S_NPSScore', 'OpenApp', 'Feedback_RateActivity', 'S_DAPNewSkills', 'S_Paywall_Soft', 'LogIn', 'MenuMemberStats', 'S_DAPMaterials', 'SkillsViewAllSkills', 'S_MenuFamilyHome', 'adjust_network']
Outputs= raw_data[variables]

h2o.init()
modelopath = '/home/fbfakeevents/Conversion/BehaviorMixpanel/Models/aOS/BR/FT/Grid_DRF_py_3_sid_a7f2_model_python_1599758583169_2814_model_8' # 10 ó 3
modelo = h2o.load_model(modelopath)
Outputs = h2o.H2OFrame(Outputs)
Outputs['prediccion'] = modelo.predict(Outputs)['predict']

archivo = open('/home/fbfakeevents/Conversion/BehaviorMixpanel/Predictive/Tokens/tokensOnline.txt', 'r') #cuidar que la primer linea sea el token y la segunda el id de la app
lineas = archivo.read().splitlines()
token = lineas[1] # Kinedu APP  token
path = lineas[2] # Kinedu APP id
archivo.close()


Outputs= Outputs.as_data_frame()
Outputs=Outputs[Outputs['prediccion']=="Premium"]
Outputs=Outputs.reset_index(drop=True)

count = 0

for i in range(Outputs.shape[0]):
    print(i)
    query ='' # inicializamos el query al string vacio
#if Outputs[i, 'prediccionf'] == 'Premium':
    # actualizamos en la base de kinedu
    query = "UPDATE fbpredictives SET  date_predicted = NOW(),  model_id = 'Behavior' , prediction = 'Premium' , sended = 0, processed= 1 WHERE user_id =" + str(int(Outputs.loc[i, 'user_id']))
    #print(query)
    cursor.execute(query) #insert en la DB de produccion
    mariadb_connection.commit()
    count +=1

print("Registros Premium Actualizados" " " + str(count))


# In[ ]:
