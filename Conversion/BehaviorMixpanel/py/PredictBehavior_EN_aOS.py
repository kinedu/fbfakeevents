#!/usr/bin/env python
# coding: utf-8

# In[1]:


from h2o.estimators.random_forest import H2ORandomForestEstimator #checar que modelo es el mejor 
import h2o 
import urllib
import json 
import requests
import time 
import datetime
import pandas as pd
import numpy as np
import mysql.connector as mariadb
inicio = datetime.datetime.now()
archivo = open('/home/fbfakeevents/Conversion/BehaviorMixpanel/Predictive/Tokens/tokensOnline.txt', 'r') #cuidar que la primer linea lineas = archivo.read().splitlines()
lineas = archivo.read().splitlines()
passKineduDB = lineas[0]
archivo.close()


# In[2]:


from google.cloud import bigquery
from google.oauth2 import service_account

credentials = service_account.Credentials.from_service_account_file(
  '/home/fbfakeevents/Conversion/BehaviorMixpanel/Predictive/Tokens/Credentials/KineduDataWarehouse.json',
    scopes=["https://www.googleapis.com/auth/cloud-platform"],
)

client = bigquery.Client(
    credentials=credentials,
    project=credentials.project_id,
)


# In[3]:


def query_BQ(query_string):
    query_job = client.query(query_string)
    results = query_job.result().to_dataframe()  # Waits for job to complete and cast to pandas dataframe
    return(results)


# In[4]:


query = '''
SELECT 
distinct user_id,
max(days_in_app) as days_in_app,
(case when max(country) = 'US' then 1 else 2 end) as country,
max(FT_start) as FT_start,
y,
sum(case when name ="MenuFamilyInvite" then 1 else 0 end) as MenuFamilyInvite,
sum(case when name ="S_CATHome_P" then 1 else 0 end) as S_CATHome_P,
sum(case when name ="clicked" then 1 else 0 end) as clicked,
sum(case when name ="PromoCodeRedeem" then 1 else 0 end) as PromoCodeRedeem,
sum(case when name ="S_PromoCode" then 1 else 0 end) as S_PromoCode,
sum(case when name ="MenuMemberStats" then 1 else 0 end) as MenuMemberStats,
sum(case when name ="Feedback_RateActivity" then 1 else 0 end) as Feedback_RateActivity,
sum(case when name ="ViewAllCollections" then 1 else 0 end) as ViewAllCollections,
sum(case when name ="DAPChangeActivity" then 1 else 0 end) as DAPChangeActivity,
sum(case when name ="S_CollectionView" then 1 else 0 end) as S_CollectionView,
sum(case when name ="S_CATViewSkill" then 1 else 0 end) as S_CATViewSkill,
sum(case when video = "Completed" then 1 else 0 end) as Video_Completed,
sum(case when name ="DAPActivityCompleted" then 1 else 0 end) as DAPActivityCompleted,
sum(case when name ="S_MenuUserProfile" then 1 else 0 end) as S_MenuUserProfile,
sum(case when name ="CATSearch" then 1 else 0 end) as CATSearch,
sum(case when name ="SkillsViewAllSkills" then 1 else 0 end) as SkillsViewAllSkills,
sum(case when name ="S_MenuFamilyProfile" then 1 else 0 end) as S_MenuFamilyProfile,
sum(case when name ="S_ProgressHome" then 1 else 0 end) as S_ProgressHome,
sum(case when name ="Dismiss_Feedback_RateActivity" then 1 else 0 end) as Dismiss_Feedback_RateActivity,
sum(case when name ="S_FeedHome" then 1 else 0 end) as S_FeedHome,
sum(case when name ="S_MilestonesHome" then 1 else 0 end) as S_MilestonesHome,
sum(case when name ="shown" then 1 else 0 end) as shown,
sum(case when name ="ArticleView" then 1 else 0 end) as ArticleView,
sum(case when name ="S_MenuFamilyHome" then 1 else 0 end) as S_MenuFamilyHome,
sum(case when name ="ActivityView" then 1 else 0 end) as ActivityView,
sum(case when name ="ClickInDAP" then 1 else 0 end) as ClickInDAP,
sum(case when name ="MenuHelp" then 1 else 0 end) as MenuHelp,
sum(case when name ="RateActivityDAP" then 1 else 0 end) as RateActivityDAP,
sum(case when name ="MilestonesUpdate" then 1 else 0 end) as MilestonesUpdate,
sum(case when name ="S_DAPMaterials" then 1 else 0 end) as S_DAPMaterials,
sum(case when name ="FreeTrialStart" then 1 else 0 end) as FreeTrialStart,
sum(case when name ="S_DAPNewSkills" then 1 else 0 end) as S_DAPNewSkills,
from
(
SELECT u.user_id,
timestamp_diff(timestamp(current_date('UTC')),u.created_at, day) as days_in_app,
timestamp_diff(ued.premium_conversion_date,u.created_at, day) as days_to_convert,/*Use only for analysis*/
timestamp_diff(ued.trial_start ,u.created_at, day) as days_to_take_FT,/*Use only for analysis*/
ev.name, ev.screen_height,ev.screen_width,ev.properties,
u.country,
u.kinedu_language,
u.network,
IF(ev.name= "VideoPlayerActivity" and JSON_EXTRACT(ev.properties, "$.video_completed")="true", "Completed", "Not Completed") as Video,
if (ev.device is null, 'NULL', ev.device) as device,
/*0.01915nulls */
if (p.android_app_version is null, 'NULL', android_app_version) as android_app_version, /*all related to android will be categoric 0.001 nulls*/
if( p.android_app_version_code is null, 'NULL', cast( android_app_version_code as string) ) as android_app_version_code,
if( android_brand is null, 'NULL', android_brand) as android_brand,
if(android_lib_version is null, 'NULL', android_lib_version)  as android_lib_version,
if(android_manufacturer is null, 'NULL', android_manufacturer) as android_manufacturer,
if(android_os_version is null, 'NULL', android_os_version) as android_os_version,
if(ued.premium_conversion_date is not null, 'Premium', 'Freemium') as y,
if(p.initial_assessment_completed is  null or initial_assessment_completed="NO", 0, 1) as initial_assessment_completed,
p.timezone,
if(p.user_relationship is null, 'NULL', user_relationship) as user_relationship,
if(p.user_role is null, 'NULL', user_role) as user_role,
if(ued.trial_start is not null, 1, 0) as FT_start,
if(ued.trial_converted is not null , 1, 0) as FT_conversion
FROM `celtic-music-240111.mixpanel.event` as ev
left join mixpanel.people as p
on p.distinct_id = ev.distinct_id
inner join celtic-music-240111.dbt_prod_caf.caf_users as u
on u.email = p.user_email
inner join aws_kinedu_app.user_extra_data as ued
on u.user_id = ued.user_id
where DATE(ev.time)>= '2020-01-01' and  timestamp_diff(timestamp(DATE(ev.time)) , u.created_at, day) <=1 /*only the first 2 day events*/
and u.os='Android'
and u.network in ('Facebook', 'SMARTLY - Facebook', 'Facebook Installs', 'Instagram', 'SMARTLY - Instagram')
)
where days_in_app<=4
and kinedu_language in ('en') and country in('US','GB','CA','AU') 
group by user_id,y
'''


# In[5]:


import datetime
start = datetime.datetime.now()
raw_data = query_BQ(query)
end = datetime.datetime.now()
print(end - start)


# In[6]:


mariadb_connection = mariadb.connect(host = 'dbmaster.c6ji2pa9hmrh.us-west-2.rds.amazonaws.com', user='root',
                                     password= passKineduDB, database='kinedu_app', port=3306)
cursor = mariadb_connection.cursor()
# la consulta
string = '''
SELECT user_id, model_id,prediction, sended_at
FROM fbpredictives
WHERE 
user_id>6416030
and ((model_id is null) or (model_id= 'Demographics') or (model_id= 'Behavior' and prediction='Freemium'))
;
'''


cursor.execute(string)
rw_data  = pd.DataFrame(cursor.fetchall())
rw_data.columns = cursor.column_names


# In[7]:


l1= raw_data['user_id'].values.tolist()
l2=rw_data['user_id'].tolist()

def list_duplicates(seq):
  seen = set()
  seen_add = seen.add
  # adds all elements it doesn't know yet to seen and all other to seen_twice
  seen_twice = set( x for x in seq if x in seen or seen_add(x) )
  # turn the set into a list (as requested)
  return list( seen_twice )


# In[8]:


l= l1+l2
users=list_duplicates(l) # yi


# In[9]:


premium_before= rw_data[rw_data['prediction'] =='Premium']
premium_before = premium_before[premium_before['user_id'].isin(users)]
type(rw_data)
premium_before.reset_index(drop=True)
usbefPrem = premium_before['user_id'].tolist()
print("Usuarios premium Demographics" " " + str(len(usbefPrem)))


# In[10]:


raw_data=raw_data[raw_data['user_id'].isin(users)]


# In[11]:


#raw_data=raw_data.loc[raw_data['days_in_app']<=14]


# In[12]:



#raw_data['days_to_take_FT']


# In[13]:


raw_data['days_in_app'].value_counts()


# In[14]:


raw_data['FT_start'].value_counts()


# In[15]:


len(raw_data)


# In[16]:


FTStart= raw_data.loc[raw_data['FT_start']==1]
FTStart= pd.DataFrame(FTStart['user_id'])


# In[17]:


raw_data= raw_data.loc[raw_data['FT_start']==0]


# In[18]:


P=raw_data.loc[raw_data['y']=='Premium']
P= pd.DataFrame(P['user_id'])


# In[19]:


raw_data= raw_data.loc[raw_data['y']=='Freemium']


# In[20]:


#FT= raw_data[['user_id','y','days_in_app']]


# In[21]:


try:
    raw_data['MenuFamilyInvite']=(raw_data['MenuFamilyInvite']-0.154023569060617)/0.401454782286394
    raw_data['S_CATHome_P']=(raw_data['S_CATHome_P']-0.310595105046235)/1.13144655348658
    raw_data['clicked']=(raw_data['clicked']-0.228343423727009)/0.532600196017197
    raw_data['PromoCodeRedeem']=(raw_data['PromoCodeRedeem']-0.0335749707357502)/0.206080220867157
    raw_data['S_PromoCode']=(raw_data['S_PromoCode']-0.0633239933533805)/0.34587514061135
    raw_data['MenuMemberStats']=(raw_data['MenuMemberStats']-0.113088115775033)/0.593563715718446
    raw_data['Feedback_RateActivity']=(raw_data['Feedback_RateActivity']-0.1682735491251552)/0.624514304578696
    raw_data['ViewAllCollections']=(raw_data['ViewAllCollections']-0.0280449267074083)/0.165045455553976
    raw_data['DAPChangeActivity']=(raw_data['DAPChangeActivity']-0.042496065042885)/0.248625167495779
    raw_data['S_CollectionView']=(raw_data['S_CollectionView']-0.413377137728384)/1.14505827110422
    raw_data['S_CATViewSkill']=(raw_data['S_CATViewSkill']-0.0232320540502314)/0.18310853120887
    raw_data['Video_Completed']=(raw_data['Video_Completed']-0.380883076489711)/0.99057943316339
    raw_data['DAPActivityCompleted']=(raw_data['DAPActivityCompleted']-0.5154641041942079)/1.11005279541836
    raw_data['S_MenuUserProfile']=(raw_data['S_MenuUserProfile']-0.392317062478736)/1.11354824461824
    raw_data['CATSearch']=(raw_data['CATSearch']-0.0188912260585661)/0.15317391045801
    raw_data['SkillsViewAllSkills']=(raw_data['SkillsViewAllSkills']-0.46411626234926706)/1.53392571748829
    raw_data['S_MenuFamilyProfile']=(raw_data['S_MenuFamilyProfile']-0.45762436627493613)/1.11907672342669
    raw_data['S_ProgressHome']=(raw_data['S_ProgressHome']-0.890579754364821)/2.4804988786274
    raw_data['Dismiss_Feedback_RateActivity']=(raw_data['Dismiss_Feedback_RateActivity']-0.026517466328349455)/0.168085202237774
    raw_data['S_FeedHome']=(raw_data['S_FeedHome']-0.361436044818406)/0.798680947020138
    raw_data['S_MilestonesHome']=(raw_data['S_MilestonesHome']-1.75637618863657)/5.00930911671683
    raw_data['shown']=(raw_data['shown']-4.94648339147445)/7.4126899518777
    raw_data['ArticleView']=(raw_data['ArticleView']-0.323319836080781)/1.37088746782852
    raw_data['S_MenuFamilyHome']=(raw_data['S_MenuFamilyHome']-1.73236448023565)/2.9799422319061
    raw_data['ActivityView']=(raw_data['ActivityView']-2.65150219024079)/4.31788065332079
    raw_data['ClickInDAP']=(raw_data['ClickInDAP']-0.0181924520957424)/0.15979803372931
    raw_data['MenuHelp']=(raw_data['MenuHelp']-0.0097838756842974)/0.122516361931042
    raw_data['RateActivityDAP']=(raw_data['RateActivityDAP']-0.00949100758256008)/0.0859051552863977
    raw_data['MilestonesUpdate']=(raw_data['MilestonesUpdate']-0.903222916772224)/2.25606805487669
    raw_data['S_DAPMaterials']=(raw_data['S_DAPMaterials']-0.531571758817522)/0.861477193282392
    raw_data['FreeTrialStart']=(raw_data['FreeTrialStart']-0.00126487366560659)/0.0292669944389155
    raw_data['S_DAPNewSkills']=(raw_data['S_DAPNewSkills']-0.00690740827871254)/0.072753361101349
    raw_data['country']=(raw_data['country']-1.3800181194414145)/0.44982774354501
except:
    pass


# In[22]:


#Filtrar Variables que se ocupan en el modelo 
variables=['user_id','MenuFamilyInvite', 'S_CATHome_P', 'clicked', 'PromoCodeRedeem', 'S_PromoCode', 'MenuMemberStats', 'Feedback_RateActivity', 'ViewAllCollections', 'DAPChangeActivity', 'S_CollectionView', 'S_CATViewSkill', 'Video_Completed', 'DAPActivityCompleted', 'S_MenuUserProfile', 'CATSearch', 'SkillsViewAllSkills', 'S_MenuFamilyProfile', 'S_ProgressHome', 'Dismiss_Feedback_RateActivity', 'S_FeedHome', 'S_MilestonesHome', 'shown', 'ArticleView', 'S_MenuFamilyHome', 'ActivityView', 'ClickInDAP', 'MenuHelp', 'RateActivityDAP', 'MilestonesUpdate', 'S_DAPMaterials', 'FreeTrialStart', 'S_DAPNewSkills', 'country']
Outputs= raw_data[variables]


# In[24]:


# carga de modelo 
h2o.init()
modelopath = '/home/fbfakeevents/Conversion/BehaviorMixpanel/Models/aOS/EN/Grid_XGBoost_py_3_sid_aac6_model_python_1606954809514_1528_model_7' # 10 ó 3
modelo = h2o.load_model(modelopath)
Outputs = h2o.H2OFrame(Outputs)
Outputs['prediccion'] = modelo.predict(Outputs)['predict']
Outputs['p_premium']= modelo.predict(Outputs)['Premium']
archivo = open('/home/fbfakeevents/Conversion/BehaviorMixpanel/Predictive/Tokens/tokensOnline.txt', 'r') #cuidar que la primer linea sea el token y la segunda el id de la app 
lineas = archivo.read().splitlines()
token = lineas[1] # Kinedu APP  token
path = lineas[2] # Kinedu APP id
archivo.close()



# In[26]:


Outputs= Outputs.as_data_frame()
Outputs=Outputs.sort_values(by=['p_premium'],ascending=False).reset_index(drop= True)
Outputs= Outputs.iloc[:35]


# In[27]:


Outputs= pd.DataFrame(Outputs['user_id'])
Outputs=pd.concat([P, FTStart,Outputs])
Outputs=Outputs.reset_index(drop=True)


# In[28]:


count = 0


# In[29]:


Outputs.shape


# In[31]:


for i in range(Outputs.shape[0]):
    print(i)
    query ='' # inicializamos el query al string vacio
#if Outputs[i, 'prediccionf'] == 'Premium':
    # actualizamos en la base de kinedu 
    query = "UPDATE fbpredictives SET  date_predicted = NOW(),  model_id = 'Behavior' , prediction = 'Premium' , sended = 0, processed= 1 WHERE user_id =" + str(int(Outputs.loc[i, 'user_id'])) 
    #print(query)
    cursor.execute(query) #insert en la DB de produccion 
    mariadb_connection.commit()
    count +=1
 #print(query)
        


# In[32]:


print("Registros Premium Actualizados" " " + str(count))


# In[ ]:




