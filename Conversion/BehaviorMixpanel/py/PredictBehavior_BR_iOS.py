
from h2o.estimators.random_forest import H2ORandomForestEstimator #checar que modelo es el mejor
import h2o
import urllib
import json
import requests
import time
import datetime
import pandas as pd
import numpy as np
import mysql.connector as mariadb
inicio = datetime.datetime.now()
archivo = open('/home/fbfakeevents/Conversion/Demographics/Predictive/Tokens/tokensOnline.txt', 'r') #cuidar que la primer linea sea el token y la segunda el id de la app
lineas = archivo.read().splitlines()
passKineduDB = lineas[0]
archivo.close()


# In[25]:


from google.cloud import bigquery
from google.oauth2 import service_account

credentials = service_account.Credentials.from_service_account_file(
  '/home/fbfakeevents/Conversion/BehaviorMixpanel/Credentials/KineduDataWarehouse.json',
    scopes=["https://www.googleapis.com/auth/cloud-platform"],
)

client = bigquery.Client(
    credentials=credentials,
    project=credentials.project_id,
)



def query_BQ(query_string):
    query_job = client.query(query_string)
    results = query_job.result().to_dataframe()  # Waits for job to complete and cast to pandas dataframe
    return(results)



# In[27]:
query = '''
SELECT
user_id,
max(mp_country) as mp_country,
max(initial_assessment_completed) as initial_assessment_completed,
max(ios_app_release) as ios_app_release,
max(ios_device_model) as ios_device_model,
max(timezone) as timezone,
max(FT_start) as FT_start,
max(user_relationship) as user_relationship,
max(user_role) as user_role,
y,
min(screen_height) as screen_height,
min(screen_width) as screen_width,
sum(case when name = "LogIn" then 1 else 0 end) as LogIn,
sum(case when name = "OpenApp" then 1 else 0 end) as OpenApp,
sum(case when name = "IAFinishSkill" then 1 else 0 end) as IAFinishSkill,
sum(case when name = "ActivityDescription" then 1 else 0 end) as ActivityDescription,
sum(case when name = "S_SPHome" then 1 else 0 end) as S_SPHome,
sum(case when name = "LeanplumVarLoadAttempt" then 1 else 0 end) as LeanplumVarLoadAttempt,
sum(case when name = "S_OBBabyName" then 1 else 0 end) as S_OBBabyName,
sum(case when name = "S_OBUserRole" then 1 else 0 end) as S_OBUserRole,
sum(case when name = "S_IAHome" then 1 else 0 end) as S_IAHome,
sum(case when name = "AB_TEST" then 1 else 0 end) as AB_TEST,
sum(case when name = "MasterSkill" then 1 else 0 end) as MasterSkill,
sum(case when name = "IAStraightToActivities" then 1 else 0 end) as IAStraightToActivities,
sum(case when name = "S_DAPMaterials" then 1 else 0 end) as S_DAPMaterials,
sum(case when name = "$ae_session" then 1 else 0 end) as ae_session,
sum(case when name = "MilestonesUpdate" then 1 else 0 end) as MilestonesUpdate,
sum(case when name = "S_ProgressHome" then 1 else 0 end) as S_ProgressHome,
sum(case when name = "FirstActivityView" then 1 else 0 end) as FirstActivityView,
sum(case when name = "S_DAPHome" then 1 else 0 end) as S_DAPHome,
sum(case when name = "CreateBaby" then 1 else 0 end) as CreateBaby,
sum(case when name = "S_MenuFamilyProfile" then 1 else 0 end) as S_MenuFamilyProfile,
sum(case when name = "ActivityView" then 1 else 0 end) as ActivityView,
sum(case when name = "OBCreateUser" then 1 else 0 end) as OBCreateUser,
sum(case when name = "S_Paywall_Soft" then 1 else 0 end) as S_Paywall_Soft,
sum(case when name = "S_MenuFamilyHome" then 1 else 0 end) as S_MenuFamilyHome,
sum(case when name = "SignUp" then 1 else 0 end) as SignUp,
sum(case when name = "S_CATHome_F" then 1 else 0 end) as S_CATHome_F,
sum(case when name = "S_MilestonesHome" then 1 else 0 end) as S_MilestonesHome,
sum(case when name = "S_HealthInterest" then 1 else 0 end) as S_HealthInterest,
sum(case when name = "$ae_first_open" then 1 else 0 end) as ae_first_open,
sum(case when name = "S_IAIntro" then 1 else 0 end) as S_IAIntro,
sum(case when name = "S_PremiumProcess" then 1 else 0 end) as S_PremiumProcess,
sum(case when name = "S_SkillsMilestones" then 1 else 0 end) as S_SkillsMilestones,
sum(case when name = "S_CATHome_P" then 1 else 0 end) as S_CATHome_P,
sum(case when name = "TAPCallToAction" then 1 else 0 end) as TAPCallToAction,
sum(case when name = "IAStartAssessment" then 1 else 0 end) as IAStartAssessment,
sum(case when name = "IAFinishAssessment" then 1 else 0 end) as IAFinishAssessment,
sum(case when name = "S_IAReminderSet" then 1 else 0 end) as S_IAReminderSet,
sum(case when name = "IASkipAssessment" then 1 else 0 end) as IASkipAssessment,
sum(case when name = "SkillsViewAllSkills" then 1 else 0 end) as SkillsViewAllSkills,
sum(case when name = "FreeTrialStart" then 1 else 0 end) as FreeTrialStart,
sum(case when name = "S_SkillsHome" then 1 else 0 end) as S_SkillsHome,
sum(case when name = "S_SPLogin" then 1 else 0 end) as S_SPLogin,
sum(case when name = "PPPaymentStarted" then 1 else 0 end) as PPPaymentStarted,
sum(case when name = "S_DSVidas" then 1 else 0 end) as S_DSVidas,
sum(case when name = "Paywall_Dismiss" then 1 else 0 end) as Paywall_Dismiss,
sum(case when name = "S_SPSignUp" then 1 else 0 end) as S_SPSignUp,
sum(case when name = "ArticleView" then 1 else 0 end) as ArticleView,
sum(case when name = "LifeSpent" then 1 else 0 end) as LifeSpent,
sum(case when name = "PPPaymentProvider" then 1 else 0 end) as PPPaymentProvider,
sum(case when name = "ClickInDAP" then 1 else 0 end) as ClickInDAP,
sum(case when name = "$ae_crashed" then 1 else 0 end) as ae_crashed,
sum(case when name = "Share" then 1 else 0 end) as Share,
sum(case when name = "RestorePurchase" then 1 else 0 end) as RestorePurchase,
sum(case when name = "SkillsDetail" then 1 else 0 end) as SkillsDetail,
sum(case when name = "DAPActivityCompleted" then 1 else 0 end) as DAPActivityCompleted,
sum(case when name = "OBCreateTwins" then 1 else 0 end) as OBCreateTwins,
sum(case when name = "S_OBPremature" then 1 else 0 end) as S_OBPremature,
sum(case when name = "S_IAReminderContinue" then 1 else 0 end) as S_IAReminderContinue,
sum(case when name = "S_MenuEditBaby" then 1 else 0 end) as S_MenuEditBaby,
sum(case when name = "S_PromoCode" then 1 else 0 end) as S_PromoCode,
sum(case when name = "SkillsReviewIA" then 1 else 0 end) as SkillsReviewIA,
sum(case when name = "S_ProgressSkillHome" then 1 else 0 end) as S_ProgressSkillHome,
sum(case when name = "CATSkillDetail" then 1 else 0 end) as CATSkillDetail,
sum(case when name = "S_DAPNewSkills" then 1 else 0 end) as S_DAPNewSkills,
sum(case when name = "MenuMemberStats" then 1 else 0 end) as MenuMemberStats,
sum(case when name = "IAReminder" then 1 else 0 end) as IAReminder,
sum(case when name = "S_MenuUserProfile" then 1 else 0 end) as S_MenuUserProfile,
sum(case when name = "$ae_updated" then 1 else 0 end) as ae_updated,
sum(case when name = "OverTime" then 1 else 0 end) as OverTime,
sum(case when name = "S_CATViewSkill" then 1 else 0 end) as S_CATViewSkill,
sum(case when name = "MenuChangeBirthday" then 1 else 0 end) as MenuChangeBirthday,
sum(case when name = "Feedback_RateActivity" then 1 else 0 end) as Feedback_RateActivity,
sum(case when name = "SkillsCardNotPersonalize" then 1 else 0 end) as SkillsCardNotPersonalize,
sum(case when name = "MenuFamilyHome" then 1 else 0 end) as MenuFamilyHome,
sum(case when name = "MenuHelp" then 1 else 0 end) as MenuHelp,
sum(case when name = "MenuFamilyDefault" then 1 else 0 end) as MenuFamilyDefault,
sum(case when name = "S_NPSScore" then 1 else 0 end) as S_NPSScore,
sum(case when name = "S_UpdateMilestones" then 1 else 0 end) as S_UpdateMilestones,
sum(case when name = "S_InviteAccepted" then 1 else 0 end) as S_InviteAccepted,
sum(case when name = "RateActivityDAP" then 1 else 0 end) as RateActivityDAP,
sum(case when name = "DAPChangeActivity" then 1 else 0 end) as DAPChangeActivity,
sum(case when name = "S_Demo_CreateBaby" then 1 else 0 end) as S_Demo_CreateBaby,
sum(case when name = "NPSSubmit" then 1 else 0 end) as NPSSubmit,
sum(case when name = "MenuFamilyInvite" then 1 else 0 end) as MenuFamilyInvite,
sum(case when name = "OBNotification" then 1 else 0 end) as OBNotification,
sum(case when name = "SlideshowView" then 1 else 0 end) as SlideshowView,
sum(case when name = "PromoCodeRedeem" then 1 else 0 end) as PromoCodeRedeem,
sum(case when name = "ComparedAge" then 1 else 0 end) as ComparedAge,
sum(case when name = "HealthInterest" then 1 else 0 end) as HealthInterest,
sum(case when name = "S_Paywall" then 1 else 0 end) as S_Paywall,
sum(case when name = "PPPaymentFail" then 1 else 0 end) as PPPaymentFail,
sum(case when name = "Dismiss_Feedback_RateActivity" then 1 else 0 end) as Dismiss_Feedback_RateActivity,
sum(case when name = "SPForgotPass" then 1 else 0 end) as SPForgotPass,
sum(case when name = "ClickSurveyAnswer" then 1 else 0 end) as ClickSurveyAnswer,
sum(case when name = "GetStartedBTN" then 1 else 0 end) as GetStartedBTN,
sum(case when name = "CancelledAutorenew" then 1 else 0 end) as CancelledAutorenew
from(
SELECT u.id as user_id,ev.name,ev.screen_width,ev.screen_height,
u.mp_country,/*contains nulls 0.001*/
if(ued.premium_conversion_date is not null, 'Premium', 'Freemium') as y,
if(p.initial_assessment_completed is  null, 0, 1) as initial_assessment_completed,
p.ios_app_release,/*contains nulls 0.001*/
if(p.ios_device_model is null , 'NULL', ios_device_model) as ios_device_model,
p.timezone,
if(p.user_relationship is null, 'NULL', user_relationship) as user_relationship,
if(p.user_role is null, 'NULL', user_role) as user_role,
if(ued.trial_start is not null, 1, 0) as FT_start,
if(ued.trial_converted is not null , 1, 0) as FT_conversion
/*[relationship, user_role] .07 nulls/null as string to take it as a category*/
FROM `celtic-music-240111.mixpanel.event` as ev
left join mixpanel.people as p
on p.distinct_id = ev.distinct_id
inner join aws_kinedu_app.users as u
on u.email = p.user_email
inner join aws_kinedu_app.user_extra_data as ued
on u.id = ued.user_id

where timestamp_MILLIS(ev.mp_processing_time_ms) >= '2020-01-01' and  timestamp_diff(timestamp_MILLIS(ev.mp_processing_time_ms) , u.created_at, day) <= 2  /* Events days*/
and u.mp_os='iOS'
and ued.user_id>6223707)

where mp_country = 'BR'
group by user_id, y

'''


#sum(case when name = "PPPaymentSuccess" then contador else 0 end) as PPPaymentSuccess,


# In[28]:


import datetime
start = datetime.datetime.now()
raw_data = query_BQ(query)
end = datetime.datetime.now()
print(end - start)


# In[29]:


mariadb_connection = mariadb.connect(host = 'dbmaster.c6ji2pa9hmrh.us-west-2.rds.amazonaws.com', user='root',
                                     password= passKineduDB, database='kinedu_app', port=3306)
cursor = mariadb_connection.cursor()
# la consulta
string = '''
SELECT user_id, model_id,prediction, sended_at
FROM fbpredictives
WHERE
user_id>6415107
and ((model_id is null) or (model_id= 'Demographics') or (model_id= 'Behavior' and prediction='Freemium'))
;
'''

cursor.execute(string)
rw_data  = pd.DataFrame(cursor.fetchall())
rw_data.columns = cursor.column_names


l1= raw_data['user_id'].values.tolist()
l2=rw_data['user_id'].tolist()

def list_duplicates(seq):
  seen = set()
  seen_add = seen.add
  # adds all elements it doesn't know yet to seen and all other to seen_twice
  seen_twice = set( x for x in seq if x in seen or seen_add(x) )
  # turn the set into a list (as requested)
  return list( seen_twice )


# In[31]:


l= l1+l2
users=list_duplicates(l) # yi


# In[32]:


premium_before= rw_data[rw_data['prediction'] =='Premium']
premium_before = premium_before[premium_before['user_id'].isin(users)]
type(rw_data)
premium_before.reset_index(drop=True)
usbefPrem = premium_before['user_id'].tolist()
print("Usuarios premium Demographics" " " + str(len(usbefPrem)))


# In[33]:


raw_data=raw_data[raw_data['user_id'].isin(users)]

FT= raw_data[['user_id','y','FT_start']]
# In[34]:

raw_data['MenuFamilyInvite']=(raw_data['MenuFamilyInvite']-0.00674394099051633)/0.1465821722116
raw_data['S_CATHome_P']=(raw_data['S_CATHome_P']-0.106304882332279)/1.37940748284455
raw_data['ComparedAge']=(raw_data['ComparedAge']-0.0014401123990165)/0.0470193634685475
raw_data['S_ProgressSkillHome']=(raw_data['S_ProgressSkillHome']-0.016754478398314)/0.407261582593069
raw_data['S_CATViewSkill']=(raw_data['S_CATViewSkill']-0.00231822971548998)/0.0586251315708258
raw_data['MenuMemberStats']=(raw_data['MenuMemberStats']-0.00686687741482261)/0.195054462559798
raw_data['ActivityView']=(raw_data['ActivityView']-0.46882683526519)/2.04977518625978
raw_data['MilestonesUpdate']=(raw_data['MilestonesUpdate']-0.106252195293291)/0.575941768234288
raw_data['DAPActivityCompleted']=(raw_data['DAPActivityCompleted']-0.0463294696171408)/0.37403247764755
raw_data['PPPaymentFail']=(raw_data['PPPaymentFail']-0.0108359676852827)/0.21833589575183
raw_data['ActivityDescription']=(raw_data['ActivityDescription']-0.000140498770635756)/0.0221750894774706
raw_data['SkillsDetail']=(raw_data['SkillsDetail']-0.0427643133122585)/0.714511377809975
raw_data['S_SkillsMilestones']=(raw_data['S_SkillsMilestones']-0.0159992975061468)/0.40645022890911
raw_data['S_DAPMaterials']=(raw_data['S_DAPMaterials']-0.161661397962767)/0.581853641164841
raw_data['ae_session']=(raw_data['ae_session']-5.01849315068491)/4.09366599072914
raw_data['S_DAPHome']=(raw_data['S_DAPHome']-3.90521601685986)/5.13511814480501
raw_data['OpenApp']=(raw_data['OpenApp']-3.68563400070249)/6.09487395406999
raw_data['LogIn']=(raw_data['LogIn']-0.000403933965577801)/0.0217721958768529
raw_data['PPPaymentProvider']=(raw_data['PPPaymentProvider']-0.50235335440815)/1.45457383605311
raw_data['S_MenuFamilyProfile']=(raw_data['S_MenuFamilyProfile']-0.15230066736916)/12.0686821669396
raw_data['Feedback_RateActivity']=(raw_data['Feedback_RateActivity']-0.0368106779065683)/0.320501548149973
raw_data['S_OBUserRole']=(raw_data['S_OBUserRole']-0.000280997541271513)/0.0187396407226222
raw_data['ArticleView']=(raw_data['ArticleView']-0.221250439058658)/1.16794839828231
raw_data['SkillsViewAllSkills']=(raw_data['SkillsViewAllSkills']-0.0173515981735159)/0.188205623075128
raw_data['PPPaymentStarted']=(raw_data['PPPaymentStarted']-0.578222690551456)/1.58409677210745
raw_data['DAPChangeActivity']=(raw_data['DAPChangeActivity']-0.0114857744994731)/0.168597008646963
raw_data['PromoCodeRedeem']=(raw_data['PromoCodeRedeem']-0.000684931506849315)/0.0425262777820248
raw_data['AB_TEST']=(raw_data['AB_TEST']-3.50644538110292)/4.24931799161908
raw_data['FirstActivityView']=(raw_data['FirstActivityView']-0.000228310502283105)/0.0151083548217772
raw_data['LeanplumVarLoadAttempt']=(raw_data['LeanplumVarLoadAttempt']-0.000386371619248331)/0.0492290267294973
raw_data['S_Paywall']=(raw_data['S_Paywall']-0.722778363189322)/0.636820424416431
raw_data['GetStartedBTN']=(raw_data['GetStartedBTN']-0.757727432384971)/1.41705985156162
raw_data['CATSkillDetail']=(raw_data['CATSkillDetail']-0.327853881278539)/1.57502964363844
raw_data['S_ProgressHome']=(raw_data['S_ProgressHome']-0.0984018264840182)/0.60034551257914
raw_data['IAFinishSkill']=(raw_data['IAFinishSkill']-0.003178784685634)/0.201524975215326
raw_data['S_MenuFamilyHome']=(raw_data['S_MenuFamilyHome']-0.93066385669125)/2.14914920043097
raw_data['Dismiss_Feedback_RateActivity']=(raw_data['Dismiss_Feedback_RateActivity']-0.00677906568317527)/0.0960579261716137
raw_data['S_HealthInterest']=(raw_data['S_HealthInterest']-0.00277485072005619)/0.0695671818102444
raw_data['S_MenuUserProfile']=(raw_data['S_MenuUserProfile']-0.394380049174569)/1.20761298328633
raw_data['S_MilestonesHome']=(raw_data['S_MilestonesHome']-0.0276431331225851)/0.226165981689459
raw_data['Share']=(raw_data['Share']-0.00660344221988057)/0.118645557105274
raw_data['SkillsReviewIA']=(raw_data['SkillsReviewIA']-0.0494906919564453)/0.263604367062737
raw_data['RateActivityDAP']=(raw_data['RateActivityDAP']-0.00419740077274324)/0.0797347176527642
raw_data['SignUp']=(raw_data['SignUp']-5.26870389884088E-05)/0.00725845633312314
raw_data['S_DSVidas']=(raw_data['S_DSVidas']-0.000105374077976817)/0.0145169126662462
raw_data['S_OBBabyName']=(raw_data['S_OBBabyName']-1.24589041095888)/0.563785411609263
raw_data['MenuHelp']=(raw_data['MenuHelp']-0.0225500526870389)/0.19652246951092
raw_data['OverTime']=(raw_data['OverTime']-0.000175623463294696)/0.0205298044403479
# In[35]:
variables=['user_id','MenuFamilyInvite', 'S_CATHome_P', 'ComparedAge', 'S_ProgressSkillHome', 'S_CATViewSkill', 'MenuMemberStats', 'ActivityView', 'MilestonesUpdate', 'DAPActivityCompleted', 'PPPaymentFail', 'ActivityDescription', 'SkillsDetail', 'S_SkillsMilestones', 'S_DAPMaterials', 'ae_session', 'S_DAPHome', 'OpenApp', 'LogIn', 'PPPaymentProvider', 'S_MenuFamilyProfile', 'Feedback_RateActivity', 'S_OBUserRole', 'ArticleView', 'SkillsViewAllSkills', 'PPPaymentStarted', 'DAPChangeActivity', 'PromoCodeRedeem', 'AB_TEST', 'FirstActivityView', 'LeanplumVarLoadAttempt', 'S_Paywall', 'GetStartedBTN', 'CATSkillDetail', 'S_ProgressHome', 'IAFinishSkill', 'S_MenuFamilyHome', 'Dismiss_Feedback_RateActivity', 'S_HealthInterest', 'S_MenuUserProfile', 'S_MilestonesHome', 'Share', 'SkillsReviewIA', 'RateActivityDAP', 'SignUp', 'S_DSVidas', 'S_OBBabyName', 'MenuHelp', 'OverTime']
#Filtrar Variables que se ocupan en el modelo

Outputs= raw_data[variables]


# carga de modelo
h2o.init()
modelopath = '/home/fbfakeevents/Conversion/BehaviorMixpanel/Models/iOS/BR/Grid_DRF_py_3_sid_afb7_model_python_1596731982453_30108_model_1' # 10 ó 3

modelo = h2o.load_model(modelopath)
Outputs = h2o.H2OFrame(Outputs)
Outputs['prediccion'] = modelo.predict(Outputs)['predict']
archivo = open('/home/fbfakeevents/Conversion/Demographics/Predictive/Tokens/tokensOnline.txt', 'r') #cuidar que la primer linea sea el token y la segunda el id de la app
lineas = archivo.read().splitlines()
token = lineas[1] # Kinedu APP  token
path = lineas[2] # Kinedu APP id
archivo.close()


# In[37]:


Outputs['prediccion'].table()

Outputs= Outputs.as_data_frame()
if 'FT_start' in list(Outputs.columns):
    del Outputs['FT_start']

Outputs= pd.merge(Outputs, FT, on='user_id')
Outputs=Outputs[(Outputs['prediccion']=="Premium")| (Outputs['y']=="Premium") | (Outputs['FT_start']==1)]
Outputs['prediccionf']= 'Premium'
Outputs=Outputs.reset_index()


count = 0


# In[ ]:


for i in range(Outputs.shape[0]):
    print(i)
    query ='' # inicializamos el query al string vacio
#if Outputs[i, 'prediccion'] == 'Premium':
        # actualizamos en la base de kinedu
    query = "UPDATE fbpredictives SET  date_predicted = NOW(),  model_id = 'Behavior' , prediction = 'Premium' , sended = 0, processed= 1 WHERE user_id =" + str(int(Outputs.loc[i, 'user_id']))
    #print(query)
    cursor.execute(query) #insert en la DB de produccion
    mariadb_connection.commit()
    count +=1
 #print(query)



# In[ ]:


print("Registros Premium Actualizados" " " + str(count))


# In[ ]:
