#!/usr/bin/env python
# coding: utf-8

from h2o.estimators.random_forest import H2ORandomForestEstimator #checar que modelo es el mejor
import h2o
import urllib
import json
import requests
import time
import datetime
import pandas as pd
import numpy as np
import mysql.connector as mariadb
inicio = datetime.datetime.now()
archivo = open('/home/fbfakeevents/Conversion/BehaviorMixpanel/Predictive/Tokens/tokensOnline.txt', 'r') #cuidar que la primer linea lineas = archivo.read().splitlines()
lineas = archivo.read().splitlines()
passKineduDB = lineas[0]
archivo.close()

from google.cloud import bigquery
from google.oauth2 import service_account

credentials = service_account.Credentials.from_service_account_file(
  '/home/fbfakeevents/Conversion/BehaviorMixpanel/Predictive/Tokens/Credentials/KineduDataWarehouse.json',
    scopes=["https://www.googleapis.com/auth/cloud-platform"],
)

client = bigquery.Client(
    credentials=credentials,
    project=credentials.project_id,
)

# In[3]:


def query_BQ(query_string):
    query_job = client.query(query_string)
    results = query_job.result().to_dataframe()  # Waits for job to complete and cast to pandas dataframe
    return(results)


# In[43]:

query = '''
SELECT distinct user_id,
max(baby_age) as baby_age,
#max(days_to_convert) as days_to_convert, /*Use only for analysis*/
#max(days_to_take_FT) as days_to_take_FT,
max(days_in_app) as days_in_app,
max(device) as device,#
max(adjust_network) as adjust_network,
max(android_app_version) as android_app_version,
max(android_lib_version) as android_lib_version /*.002 null class*/,
max(screen_width) as screenwidth,
max(timezone) as timezone,
max(user_relationship) as user_relationship,
max(user_role) as user_role,
#max(initial_assessment_completed) as initial_assessment_completed,
#max(FT_start) as FT_start,
#max(FT_conversion) as FT_conversion,
#mp_country,
y,
sum(case when video = "Completed" then 1 else 0 end) as Video_Completed,
sum(case when name ="OpenApp" then 1 else 0 end) as OpenApp,
sum(case when name ="LogOut" then 1 else 0 end) as LogOut,
sum(case when name ="S_DAPNewSkills" then 1 else 0 end) as S_DAPNewSkills,
sum(case when name ="AB_TEST" then 1 else 0 end) as AB_TEST,
sum(case when name ="S_SPHome" then 1 else 0 end) as S_SPHome,
sum(case when name ="S_SPLogin" then 1 else 0 end) as S_SPLogin,
sum(case when name ="LogIn" then 1 else 0 end) as LogIn,
sum(case when name ="SPForgotPass" then 1 else 0 end) as SPForgotPass,
sum(case when name ="S_SPSignup" then 1 else 0 end) as S_SPSignup,
sum(case when name ="SignUp" then 1 else 0 end) as SignUp,
sum(case when name ="OBCreateUser" then 1 else 0 end) as OBCreateUser,
sum(case when name ="S_OBBabyName" then 1 else 0 end) as S_OBBabyName,
sum(case when name ="OBCreateTwins" then 1 else 0 end) as OBCreateTwins,
sum(case when name ="S_OBPremature" then 1 else 0 end) as S_OBPremature,
sum(case when name ="Paywall_Dismiss" then 1 else 0 end) as Paywall_Dismiss,
sum(case when name ="S_Paywall_Soft" then 1 else 0 end) as S_Paywall_Soft,
sum(case when name ="S_IAIntro" then 1 else 0 end) as S_IAIntro,
sum(case when name ="IAStraightToActivities" then 1 else 0 end) as IAStraightToActivities,
sum(case when name ="IAStartAssessment" then 1 else 0 end) as IAStartAssessment,
sum(case when name ="IASkipAssessment" then 1 else 0 end) as IASkipAssessment,
sum(case when name ="MasterSkill" then 1 else 0 end) as MasterSkill,
sum(case when name ="IAFinishSkill" then 1 else 0 end) as IAFinishSkill,
sum(case when name ="S_HealthInterest" then 1 else 0 end) as S_HealthInterest,
sum(case when name ="S_IASetReminder" then 1 else 0 end) as S_IASetReminder,
sum(case when name ="IAReminder" then 1 else 0 end) as IAReminder,
sum(case when name ="S_IA_Answer_Skill" then 1 else 0 end) as S_IA_Answer_Skill,
sum(case when name ="DAPChangeActivity" then 1 else 0 end) as DAPChangeActivity,
sum(case when name ="S_DAPPastPlansPremium" then 1 else 0 end) as S_DAPPastPlansPremium,
sum(case when name ="DAPActivityCompleted" then 1 else 0 end) as DAPActivityCompleted,
sum(case when name ="S_DAPMaterials" then 1 else 0 end) as S_DAPMaterials,
sum(case when name ="MilestonesUpdate" then 1 else 0 end) as MilestonesUpdate,
sum(case when name ="ArticleView" then 1 else 0 end) as ArticleView,
sum(case when name ="SlideshowView" then 1 else 0 end) as SlideshowView,
sum(case when name ="ActivityView" then 1 else 0 end) as ActivityView,
sum(case when name ="ClickInDAP" then 1 else 0 end) as ClickInDAP,
sum(case when name ="RateActivityDAP" then 1 else 0 end) as RateActivityDAP,
sum(case when name ="Feedback_RateActivity" then 1 else 0 end) as Feedback_RateActivity,
sum(case when name ="Dismiss_Feedback_RateActivity" then 1 else 0 end) as Dismiss_Feedback_RateActivity,
sum(case when name ="LifeSpent" then 1 else 0 end) as LifeSpent,
sum(case when name ="WhatchAd" then 1 else 0 end) as WhatchAd,
sum(case when name ="VideoPlayerActivity" then 1 else 0 end) as VideoPlayerActivity,
sum(case when name ="NPSSubmit" then 1 else 0 end) as NPSSubmit,
sum(case when name ="S_NPSScore" then 1 else 0 end) as S_NPSScore,
sum(case when name ="S_ProgressHome" then 1 else 0 end) as S_ProgressHome,
sum(case when name ="S_MenuFamilyHome" then 1 else 0 end) as S_MenuFamilyHome,
sum(case when name ="MenuHelp" then 1 else 0 end) as MenuHelp,
sum(case when name ="MenuMemberStats" then 1 else 0 end) as MenuMemberStats,
sum(case when name ="MenuFamilyInvite" then 1 else 0 end) as MenuFamilyInvite,
sum(case when name ="MenuChangeBirthday" then 1 else 0 end) as MenuChangeBirthday,
sum(case when name ="S_MenuEditBaby" then 1 else 0 end) as S_MenuEditBaby,
sum(case when name ="S_MenuUserProfile" then 1 else 0 end) as S_MenuUserProfile,
sum(case when name ="MenuFamilyDefault" then 1 else 0 end) as MenuFamilyDefault,
sum(case when name ="S_MenuFamilyProfile" then 1 else 0 end) as S_MenuFamilyProfile,
sum(case when name ="TAPCallToAction" then 1 else 0 end) as TAPCallToAction,
sum(case when name ="S_PremiumProcess" then 1 else 0 end) as S_PremiumProcess,
sum(case when name ="RestorePurchase" then 1 else 0 end) as RestorePurchase,
sum(case when name ="S_CATHome_P" then 1 else 0 end) as S_CATHome_P,
sum(case when name ="S_CATHome_F" then 1 else 0 end) as S_CATHome_F,
sum(case when name ="CATSearch" then 1 else 0 end) as CATSearch,
sum(case when name ="S_CATViewSkill" then 1 else 0 end) as S_CATViewSkill,
sum(case when name ="S_CollectionView" then 1 else 0 end) as S_CollectionView,
sum(case when name ="ViewAllCollections" then 1 else 0 end) as ViewAllCollections,
sum(case when name ="S_SearchEmptyState" then 1 else 0 end) as S_SearchEmptyState,
sum(case when name ="ResetSearch" then 1 else 0 end) as ResetSearch,
sum(case when name ="S_MilestonesHome" then 1 else 0 end) as S_MilestonesHome,
sum(case when name ="SkillsViewAllSkills" then 1 else 0 end) as SkillsViewAllSkills,
sum(case when name ="S_PromoCode" then 1 else 0 end) as S_PromoCode,
sum(case when name ="PromoCodeRedeem" then 1 else 0 end) as PromoCodeRedeem,
sum(case when name ="S_FeedHome" then 1 else 0 end) as S_FeedHome,
sum(case when name ="InviteClassrooms" then 1 else 0 end) as InviteClassrooms,
sum(case when name ="shown" then 1 else 0 end) as shown,
sum(case when name ="clicked" then 1 else 0 end) as clicked,
sum(case when name ="dismissed" then 1 else 0 end) as dismissed
from
(
SELECT u.id as user_id,
date_diff(current_date('UTC'),date(b.birthday), month) as baby_age,
#timestamp_diff(timestamp(current_date('UTC')),b.birthday, day) as baby_ageD,
timestamp_diff(ued.trial_start ,u.created_at, day) as days_to_take_FT,/*Use only for analysis*/
timestamp_diff(timestamp(current_date('UTC')) ,u.created_at, day) as days_in_app,/*Use only for analysis*/

timestamp_diff(ued.premium_conversion_date,u.created_at, day) as days_to_convert,/*Use only for analysis*/
ev.name, ev.screen_height,ev.screen_width,ev.properties,
u.mp_country,
u.adjust_network,
IF(ev.name= "VideoPlayerActivity" and JSON_EXTRACT(ev.properties, "$.video_completed")="true", "Completed", "Not Completed") as Video,
if (ev.device is null, 'NULL', ev.device) as device,
/*0.01915nulls */
if (p.android_app_version is null, 'NULL', android_app_version) as android_app_version, /*all related to android will be categoric 0.001 nulls*/
if( p.android_app_version_code is null, 'NULL', cast( android_app_version_code as string) ) as android_app_version_code,
if( android_brand is null, 'NULL', android_brand) as android_brand,
if(android_lib_version is null, 'NULL', android_lib_version)  as android_lib_version,
if(android_manufacturer is null, 'NULL', android_manufacturer) as android_manufacturer,
if(android_os_version is null, 'NULL', android_os_version) as android_os_version,
if(ued.premium_conversion_date is not null, 'Premium', 'Freemium') as y,
if(p.initial_assessment_completed is  null or initial_assessment_completed="NO", 0, 1) as initial_assessment_completed,
p.timezone,
if(p.user_relationship is null, 'NULL', user_relationship) as user_relationship,
if(p.user_role is null, 'NULL', user_role) as user_role,
if(ued.trial_start is not null, 1, 0) as FT_start,
if(ued.trial_converted is not null , 1, 0) as FT_conversion
FROM `celtic-music-240111.mixpanel.event` as ev
left join mixpanel.people as p
on p.distinct_id = ev.distinct_id
inner join aws_kinedu_app.users as u
on u.email = p.user_email
inner join aws_kinedu_app.user_extra_data as ued
on u.id = ued.user_id
left join `celtic-music-240111.aws_kinedu_app.babies` as b
on  u.id =b.author_id
where timestamp_MILLIS(ev.mp_processing_time_ms) >= '2020-01-01' and  timestamp_diff(timestamp_MILLIS(ev.mp_processing_time_ms) , u.created_at, day) <= 3 /*only the first day events*/
and u.mp_os='Android'
)
where mp_country ='US'
and user_id>6415107
and days_to_take_FT is null
group by user_id,y, mp_country

'''

# In[44]:


import datetime
start = datetime.datetime.now()
raw_data = query_BQ(query)
end = datetime.datetime.now()
print(end - start)


# In[45]:


mariadb_connection = mariadb.connect(host = 'dbmaster.c6ji2pa9hmrh.us-west-2.rds.amazonaws.com', user='root',
                                     password= passKineduDB, database='kinedu_app', port=3306)
cursor = mariadb_connection.cursor()
# la consulta
string = '''
SELECT user_id, model_id,prediction, sended_at
FROM fbpredictives
WHERE
user_id>6415107
and ((model_id is null) or (model_id= 'Demographics') or (model_id= 'Behavior' and prediction='Freemium'))
;
'''

cursor.execute(string)
rw_data  = pd.DataFrame(cursor.fetchall())
rw_data.columns = cursor.column_names
# In[46]:


l1= raw_data['user_id'].values.tolist()
l2=rw_data['user_id'].tolist()

def list_duplicates(seq):
  seen = set()
  seen_add = seen.add
  seen_twice = set( x for x in seq if x in seen or seen_add(x) )
  return list( seen_twice )


l= l1+l2
users=list_duplicates(l) # yi

premium_before= rw_data[rw_data['prediction'] =='Premium']
premium_before = premium_before[premium_before['user_id'].isin(users)]
premium_before.reset_index(drop=True)
usbefPrem = premium_before['user_id'].tolist()
print("Usuarios premium Demographics" " " + str(len(usbefPrem)))

raw_data=raw_data[raw_data['user_id'].isin(users)]

#FT= raw_data[['user_id','y','days_in_app']]

try:
    raw_data['S_NPSScore']=(raw_data['S_NPSScore']-0.0435037566838274)/0.204191494544074
    raw_data['MenuFamilyInvite']=(raw_data['MenuFamilyInvite']-0.166486268740131)/0.4843919169663039
    raw_data['Video_Completed']=(raw_data['Video_Completed']-0.561540684375904)/1.609024966114448
    raw_data['S_DAPPastPlansPremium']=(raw_data['S_DAPPastPlansPremium']-0.22577561317715503)/0.9400495924103891
    raw_data['VideoPlayerActivity']=(raw_data['VideoPlayerActivity']-4.610172438121991)/13.370857563476891
    raw_data['clicked']=(raw_data['clicked']-0.230456018029138)/0.5828996204613913
    raw_data['Dismiss_Feedback_RateActivity']=(raw_data['Dismiss_Feedback_RateActivity']-0.05044620199620133)/0.2525789056954336
    raw_data['NPSSubmit']=(raw_data['NPSSubmit']-0.0135029521917842)/0.09874345396912403
    raw_data['S_CATHome_P']=(raw_data['S_CATHome_P']-0.490470739544319)/2.33441628253275
    raw_data['ViewAllCollections']=(raw_data['ViewAllCollections']-0.0329620262072876)/0.18963664822559972
    raw_data['Feedback_RateActivity']=(raw_data['Feedback_RateActivity']-0.279255985840022)/0.8687172366954798
    raw_data['PromoCodeRedeem']=(raw_data['PromoCodeRedeem']-0.0608825004270863)/0.3845860203326064
    raw_data['DAPChangeActivity']=(raw_data['DAPChangeActivity']-0.0996426998397184)/0.5903130161569025
    raw_data['S_CollectionView']=(raw_data['S_CollectionView']-0.476886970706736)/1.47295120885508
    raw_data['S_PromoCode']=(raw_data['S_PromoCode']-0.124578372987249)/0.6723215601625535
    raw_data['MenuMemberStats']=(raw_data['MenuMemberStats']-0.209260104139846)/1.0523155241358089
    raw_data['MenuHelp']=(raw_data['MenuHelp']-0.0231085371200223)/0.1578695728951455
    raw_data['S_ProgressHome']=(raw_data['S_ProgressHome']-1.23685280896221)/3.0482598744275
    raw_data['DAPActivityCompleted']=(raw_data['DAPActivityCompleted']-1.1397768805402568)/2.376140848421322
    raw_data['WhatchAd']=(raw_data['WhatchAd']-0.0467801092079264)/0.2658661497197959
    raw_data['dismissed']=(raw_data['dismissed']-0.185141419519744)/0.6211620330590609
    raw_data['ArticleView']=(raw_data['ArticleView']-0.598116984405364)/1.6329249205923
    raw_data['S_SearchEmptyState']=(raw_data['S_SearchEmptyState']-0.0074919649652132)/0.08803709632391316
    raw_data['S_FeedHome']=(raw_data['S_FeedHome']-0.46022378286427)/1.32606699365185
    raw_data['S_MenuUserProfile']=(raw_data['S_MenuUserProfile']-0.4849446151757148)/1.1710474951047307
    raw_data['S_DAPNewSkills']=(raw_data['S_DAPNewSkills']-0.038931141096084)/0.2207362497252302
    raw_data['AB_TEST']=(raw_data['AB_TEST']-1.017077168126837)/2.0599661017342
    raw_data['SignUp']=(raw_data['SignUp']-1.070108120203227)/0.28469628381990
    raw_data['RestorePurchase']=(raw_data['RestorePurchase']-0.0483094814195166)/0.3629518886348152
    raw_data['S_CATViewSkill']=(raw_data['S_CATViewSkill']-0.06306704131027)/0.7524350420973434
    raw_data['S_MenuFamilyProfile']=(raw_data['S_MenuFamilyProfile']-0.6299423975780094)/1.548748436595098
    raw_data['ActivityView']=(raw_data['ActivityView']-3.7716266051917)/6.3644943491363
    raw_data['OBCreateUser']=(raw_data['OBCreateUser']-1.07028459649833)/0.282786747597333
    raw_data['SkillsViewAllSkills']=(raw_data['SkillsViewAllSkills']-0.5388211305818525)/1.663601767041557
    raw_data['CATSearch']=(raw_data['CATSearch']-0.036759834834188)/0.523015185367369
    raw_data['RateActivityDAP']=(raw_data['RateActivityDAP']-0.0248687197864952)/0.2318086458807239
    raw_data['shown']=(raw_data['shown']-6.577440668020707)/12.6792837160008
    raw_data['S_MilestonesHome']=(raw_data['S_MilestonesHome']-2.08149540483749)/5.520842782012455
    raw_data['S_SPSignup']=(raw_data['S_SPSignup']-0.00444041659560972)/0.09456734969724795
    raw_data['MenuFamilyDefault']=(raw_data['MenuFamilyDefault']-0.0360574266259123)/0.7942682722208694
    raw_data['S_DAPMaterials']=(raw_data['S_DAPMaterials']-0.710180519877539)/1.2916397650631
    raw_data['S_MenuEditBaby']=(raw_data['S_MenuEditBaby']-0.274779250722079)/0.873393692310213
    raw_data['S_MenuFamilyHome']=(raw_data['S_MenuFamilyHome']-2.247997619126913)/4.407486320622006
    raw_data['S_IA_Answer_Skill']=(raw_data['S_IA_Answer_Skill']-3.0319385295237375)/11.433956565726568
    raw_data['ResetSearch']=(raw_data['ResetSearch']-0.0012911862732093)/0.02946337017917536
    raw_data['MilestonesUpdate']=(raw_data['MilestonesUpdate']-1.49701161506676)/3.5190595122673
    raw_data['InviteClassrooms']=(raw_data['InviteClassrooms']-0.0116375126271105)/0.30021550362464616
    raw_data['LogIn']=(raw_data['LogIn']-0.9110438602506178)/0.47289579194352
    raw_data['LogOut']=(raw_data['LogOut']-0.00252483233664542)/0.07246577695345732
    raw_data['S_IASetReminder']=(raw_data['S_IASetReminder']-0.107397747098481)/0.4085671278892994
except:
    pass


#Filtrar Variables que se ocupan en el modelo
variables=['user_id','S_NPSScore', 'MenuFamilyInvite', 'Video_Completed', 'S_DAPPastPlansPremium', 'VideoPlayerActivity', 'clicked', 'Dismiss_Feedback_RateActivity', 'NPSSubmit', 'S_CATHome_P', 'ViewAllCollections', 'Feedback_RateActivity', 'PromoCodeRedeem', 'DAPChangeActivity', 'S_CollectionView', 'S_PromoCode', 'MenuMemberStats', 'MenuHelp', 'S_ProgressHome', 'DAPActivityCompleted', 'WhatchAd', 'dismissed', 'ArticleView', 'S_SearchEmptyState', 'S_FeedHome', 'S_MenuUserProfile', 'S_DAPNewSkills', 'AB_TEST', 'SignUp', 'RestorePurchase', 'S_CATViewSkill', 'S_MenuFamilyProfile', 'ActivityView', 'OBCreateUser', 'SkillsViewAllSkills', 'CATSearch', 'RateActivityDAP', 'shown', 'S_MilestonesHome', 'S_SPSignup', 'MenuFamilyDefault', 'S_DAPMaterials', 'S_MenuEditBaby', 'S_MenuFamilyHome', 'S_IA_Answer_Skill', 'ResetSearch', 'MilestonesUpdate', 'InviteClassrooms', 'LogIn', 'LogOut', 'S_IASetReminder', 'adjust_network']
Outputs= raw_data[variables]
# carga de modelo
h2o.init()
modelopath = '/home/fbfakeevents/Conversion/BehaviorMixpanel/Models/aOS/US/Grid_XGBoost_py_3_sid_9940_model_python_1599782079367_1288_model_7' # 10 ó 3
modelo = h2o.load_model(modelopath)
Outputs = h2o.H2OFrame(Outputs)
Outputs['prediccion'] = modelo.predict(Outputs)['predict']
Outputs['p_premium']= modelo.predict(Outputs)['Premium']
archivo = open('/home/fbfakeevents/Conversion/BehaviorMixpanel/Predictive/Tokens/tokensOnline.txt', 'r') #cuidar que la primer linea sea el token y la segunda el id de la app
lineas = archivo.read().splitlines()
token = lineas[1] # Kinedu APP  token
path = lineas[2] # Kinedu APP id
archivo.close()
# In[62]:
Outputs= Outputs.as_data_frame()
Outputs=Outputs.sort_values(by=['p_premium'],ascending=False).reset_index(drop= True)
Outputs= Outputs.iloc[:70]

Outputs=Outputs[Outputs['prediccion']=="Premium"]
Outputs=Outputs.reset_index(drop=True)


count = 0

for i in range(Outputs.shape[0]):
    print(i)
    query ='' # inicializamos el query al string vacio
#if Outputs[i, 'prediccionf'] == 'Premium':
    # actualizamos en la base de kinedu
    query = "UPDATE fbpredictives SET  date_predicted = NOW(),  model_id = 'Behavior' , prediction = 'Premium' , sended = 0, processed= 1 WHERE user_id =" + str(int(Outputs.loc[i, 'user_id']))
    #print(query)
    cursor.execute(query) #insert en la DB de produccion
    mariadb_connection.commit()
    count +=1

print("Registros Premium Actualizados" " " + str(count))


# In[ ]:
